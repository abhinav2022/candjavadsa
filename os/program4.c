
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(){

	int pid = fork();

	if(pid>1){
	
		for(int i=1;i<=5;i++){
		
			printf("In parent\n");
			printf("parent pid = %d\n",getpid());
			printf("parent ppid = %d\n",getppid());
		}
	}else if(pid==0){
	
		for(int i=1;i<=10;i++){
		
			printf("In child\n");
			sleep(2);
			printf("child pid = %d\n",getpid());
			printf("child ppid = %d \n",getppid());

		}
	}else
		return -1;
}
