
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

void * run(){

	printf("Start run\n");
	printf("in run %ld\n",pthread_self());
	printf("End run\n");
}

void *gun(){

	printf("Start gun\n");
	printf("in gun %ld\n",pthread_self());
	long int tid;
	pthread_create(&tid,NULL,run,NULL);
	printf("End gun\n");
}

void * fun(){

	printf("Start fun\n");
	long int tid;
	pthread_create(&tid,NULL,gun,NULL);
	printf("in fun tid %ld\n",pthread_self());
	pthread_exit(NULL);
	
	printf("End fun\n");
}

void main(){

	printf("Start main\n");
	printf("main tid %ld\n",pthread_self());
	long int tid;
	//sleep(2);
	pthread_create(&tid,NULL,fun,NULL);
	printf("fun tid = %ld\n",tid);
	pthread_join(tid,NULL);
	printf("ZZZZZZZZZ\n");
	/*for(int i=0;i<15;i++){
	printf("End main\n");
	}*/
}
