
#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(){

	int pid = fork();

	if(pid>1){
	
		printf("%d\n",getpid());
		printf("%d\n",getppid());
		printf("%d\n",pid);

	}else if(pid==0){
	
		printf("%d\n",getpid());
		printf("%d\n",getppid());
		printf("%d\n",pid);

	}else {
	
		return -1;
	}
}
