
class ThreadDemo extends Thread{

	public void run(){
	
		System.out.println("Child thread");
	}

	public static void main(String a[] ){
	
		System.out.println("Hello");

		ThreadDemo obj = new ThreadDemo();

		obj.start();

		System.out.println("Main Thread");
	}
}
