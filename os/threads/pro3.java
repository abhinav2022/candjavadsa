
class ThreadDemo implements Runnable{

	public void run(){
	
		System.out.println("Child thread");
		System.out.println(Thread.currentThread().getName());
	}

	public static void main(String a[] ){
	
		System.out.println("Hello");

		ThreadDemo obj = new ThreadDemo();
	
		Thread t1 = new Thread(obj);

		t1.start();

		System.out.println("Main Thread");
		System.out.println(Thread.currentThread().getName());
	}
}
