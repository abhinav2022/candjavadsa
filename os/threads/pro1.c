
#include<stdio.h>
#include<unistd.h>

void main(){

	int pid = fork();

	if(pid == 0){
	
		execlp("ps","s",NULL);
	}else{
	
		printf("%d\n",pid);
	}
}
