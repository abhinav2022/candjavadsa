
#include<stdio.h>
#include<pthread.h>

void fun(){

	printf("Start fun\n");
	printf("%ld\n",pthread_self());
	printf("End fun\n");
}

void main(){

	printf("Start main\n");
	printf("%ld\n",pthread_self());	
	fun();
	printf("End main\n");
}
