
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>

int main(){

	int pid = fork();

	if(pid>1){
	
		printf("a.out pid %d\n",getpid());
		printf("a.out ppid %d\n",getppid());
	}else if(pid==0){
	
		for(int i=1;i<=10;i++){
		
			printf("In child pid %d\n",getpid());
			printf("In child ppid %d\n",getppid());
			sleep(2);
		}
		execlp("./pro2","\n",NULL);
	}
	else
		return -1;
}
