
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	struct Node*prev;
	char str[20];
	struct Node*next;
}n;

void addAtPos(n**,int);
int countNode(n*head){

	int c=0;

	while(head!=NULL){
	
		c++;
		head=head->next;
	}
	return c;
}

n* createNode(){

	n* newNode = (n*)malloc(sizeof(n));

	int i=0;
	char ch;
	printf("Enter name\n");
	while((ch=getchar())!='\n'){
	
		newNode->str[i]=ch;
		i++;
	}
	newNode->next=NULL;
	newNode->prev = NULL;

	return newNode;
}

void addNode(n**head){

	n* newNode = createNode();
	
	if(*head==NULL){
	
		*head=newNode;

	}else{
	
		n * temp = *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void addFirst(n**head){

	n* temp = createNode();
	if(*head==NULL){

		*head = temp;
	}else{
	
		temp->next=*head;
		(*head)->prev=temp;
		*head = temp;
	}
}

void addLast(n**head){

	addNode(head);
}

void addAtPos(n**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("No node present\n");
	}
	else{
	
		if(pos==1){
		
			addFirst(head);
		}else if(pos==countNode(*head)+1){
		
			addLast(head);
		}else{
		
			n*newNode = createNode();
			n*temp =*head;
			while(pos-2){
			
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
	}
}

void delFirst(n**head){

	if(*head==NULL){
	
		printf("No nodes\n");
	}else if((*head)->next==NULL){
	
		free(*head);
		(*head)=NULL;
	}else{
	
		*head=(*head)->next;

		free((*head)->prev);
		(*head)->prev=NULL;
	}
}

void delLast(n**head){

	if(*head==NULL){
	
		printf("No node present\n");
	}else{
	
		if((*head)->next==NULL){
		
			free(*head);
			*head=NULL;

		}else{
			
			n*temp = *head;
		
			while(temp->next->next!=NULL){
			
				temp=temp->next;
			}

			free(temp->next);
			temp->next=NULL;
		}
	}	
}

void printLL(n*temp){

        if(temp==NULL)
                printf("No nodes present\n");

        else{
        
	while(temp->next!=NULL){
	
		printf("|%s|->",temp->str);
		temp=temp->next;
	}
	printf("|%s|\n",temp->str);
}
}

void delAtPos(n**head,int pos){

	int count=countNode(*head);
	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("Invalid node position %d of total nodes %d\n",pos,count);

	}else if(*head==NULL){
	
		printf("No node present\n");
	}else if(pos==1){
	
		delFirst(head);
	}else if(pos==count){
	
		delLast(head);
	}else{
	
		n*temp = *head;
		while((pos--)-2){
		
			temp=temp->next;
		}
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next=temp;
	}
}

void mystrrev(char*str){

        char *temp =str;

        while(*temp!='\0')
                temp++;
temp--;
        char ch;
        while(str<temp){

                ch = *str;
                *str = *temp;
                *temp = ch;

                str++;
                temp--;
        }

}

void rev(n*head){

        if(head==NULL){

                printf("No Node present\n");
        }else{

                while(head!=NULL){

                        mystrrev(head->str);
                        head=head->next;
                }
        }
}
void main(){

	n*head = NULL;
	char choice;

	do{
	
		int n;
		printf("1.AddNode\n");
		printf("2.AddFirst\n");
		printf("3.AddLast\n");
		printf("4.AddAtPos\n");
		printf("5.delFirst\n");
		printf("6.delLast\n");
		printf("7.delAtPos\n");
		printf("8.printLL\n");
		printf("9.countNode\n");
		printf("10.rev\n");
		printf("11.Exit\n");
		printf("Your choice:-");
		scanf("%d",&n);
		getchar();

		switch(n){
		
			case 1:
				addNode(&head);
				break;

			case 2:
				addFirst(&head);
				break;

			case 3:
				addLast(&head);
				break;

			case 4:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);
					getchar();
					addAtPos(&head,pos);
				}
				break;

			case 5:
				delFirst(&head);
				break;

			case 6:
				delLast(&head);
				break;

			case 7:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);
					getchar();
					delAtPos(&head,pos);
				}
				break;

			case 8:
				printLL(head);
				break;

			case 9:
				printf("Total number of Nodes are %d\n",countNode(head));
				break;

			case 10:
				rev(head);
				break;

			case 11:
				printf("Exit\n");
				exit(0);
				break;

			default:
				printf("Wrong choice\n");
				break;

		}
		printf("Do you want to continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
