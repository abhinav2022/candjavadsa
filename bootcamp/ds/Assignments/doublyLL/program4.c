
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	struct Node * prev;
	int d;
	struct Node* next;
}n;

n* createNode(){

	n* newNode = (n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	newNode->prev=NULL;

	return newNode;
}

int printLL(n*temp){

	if(temp==NULL){
	
		printf("No node present\n");
		return -1;
	}

	while(temp->next!=NULL){
	
		printf("|%d|->",temp->d);
		temp=temp->next;
	}
	printf("|%d|\n",temp->d);

	return 0;
}

void addNode(n**head){

	n*newNode = createNode();

	if(*head==NULL){
	
		*head=newNode;
	}else {
	
		n*temp= *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

int countNode(n*temp){

	int c=0;

	while(temp!=NULL){
	
		c++;
		temp=temp->next;
	}
	return c;
}

void addFirst(n**head){

	n*newNode= createNode();
	if(*head==NULL){
	
		*head =  newNode;
	}else{
	
		newNode->next=*head;
		(*head)->prev = newNode;
		(*head)=newNode;
	}
}

void addLast(n**head){

	addNode(head);
}

void addAtPos(n**head,int pos){
	int p=countNode(*head);

	if(pos<=0 || pos>p+1){
	
		printf("Wrong node position %d total nodes are %d",pos,p);

	}else if(pos==1){
	
		addFirst(head);
	}else if(pos==p+1){
	
		addLast(head);
	}else{
	
		n*newNode = createNode();
		n*temp = *head;

		while((pos--)-2){
		
			temp=temp->next;
		}
		newNode->next=temp->next;
		newNode->prev = temp;
		temp->next->prev=newNode;
		temp->next=newNode;
		
	}
}

void delFirst(n**head){

	if(*head==NULL){
	
		printf("No nodes present\n");

	}else if((*head)->next==NULL){
	
		free(*head);
		*head=NULL;
	}else{
	
		*head=(*head)->next;

		free((*head)->prev);

		(*head)->prev=NULL;

	}
}

void delLast(n**head){

	if(*head==NULL){
	
		printf("No nodes present\n");

	}else if((*head)->next==NULL){
	
		free(*head);
		*head=NULL;
	}else{
	
		n*temp=*head;

		while(temp->next->next!=NULL){
		
			temp=temp->next;
		}
		free(temp->next);

		temp->next=NULL;
	}
}

void delAtPos(n**head,int pos){

	int count = countNode(*head);
	if(pos<=0 || pos>count){
	
		printf("Wrong choice\n");

	}else if(pos==1){
	
		delFirst(head);
	}else if(pos==count){
	
		delLast(head);
	}else{
	
		n*temp=*head;

		while((pos--)-2){
		
			temp=temp->next;
		}
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next->prev=NULL;
	}
}
void addIt(n*head){

        if(head==NULL){

                printf("No Node present\n");
        }else{

                while(head!=NULL){

                        int p=head->d,c=0;

                        while(p>0){

                                c+=p%10;
                                p/=10;

                        }
                        head->d = c;
                        head=head->next;
                }

        }
}
void main(){

	char choice;
	n* head = NULL;

	do{
		 int n;
                printf("1.AddNode\n");
                printf("2.AddFirst\n");
                printf("3.AddLast\n");
                printf("4.AddAtPos\n");
                printf("5.delFirst\n");
                printf("6.delLast\n");
                printf("7.delAtPos\n");
                printf("8.printLL\n");
                printf("9.countNode\n");
                printf("10.addIt\n");
                printf("11.Exit\n");
                printf("Your choice:-");
                scanf("%d",&n);

                switch(n){

                        case 1:
                                addNode(&head);
                                break;

                        case 2:
                                addFirst(&head);
                                break;

                        case 3:
                                addLast(&head);
                                break;

                        case 4:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);
                                        addAtPos(&head,pos);
                                }
                                break;

                        case 5:
                                delFirst(&head);
                                break;

                        case 6:	
				delLast(&head);
                                break;

                        case 7:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);
                                        delAtPos(&head,pos);
                                }
                                break;

                        case 8:
                                printLL(head);
                                break;

                        case 9:
                                printf("Total number of Nodes are %d\n",countNode(head));
                                break;

                        case 10:
                                addIt(head);
                                break;

                        case 11:
                                printf("Exit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice\n");
                                break;

                }
		getchar();
		printf("Do you continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
