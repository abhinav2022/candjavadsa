//Question 11: Write program that accept two sll whose sum of digits are even ;

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	struct Node* next;
}n;

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));

	if(newNode==NULL){
	
		printf("Memory full\n");
		exit(-1);
	}
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	return newNode;
}

int addNode(n**head){

	n*newNode=createNode();

	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		n*temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}

		temp->next=newNode;
	}
	return 0;
}

int countNode(n*head){

	int c=0;

	while(head!=NULL){
		c++;
		head=head->next;
	}
	return c;
}

int addFirst(n**head){

	n*newNode=createNode();
	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		newNode->next=*head;
		*head=newNode;
	}
}

int addLast(n**head){

	addNode(head);
}

int addAtPos(int pos,n**head){

	int c=countNode(*head);
	if(pos<=0 || pos>c+1){
	
		return -1;
	}else{
	
		if(pos==1)
			addFirst(head);
		else if(pos==c+1)
			addLast(head);
		else{
			n*newNode=createNode();
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;

			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	}
}

int deleteFirst(n**head){

	if(*head==NULL){
	
		return -1;
	} else {
	
		if((*head)->next==NULL){
		
			free(*head);
			*head=NULL;
		}else{
		
			n*temp=*head;
			*head=(*head)->next;
			free(temp);
		}
	}
}

int deleteLast(n**head){

	if(*head==NULL){

                return -1;
        } else {

                if((*head)->next==NULL){

                        free(*head);
                        *head=NULL;
                }else{

                        n*temp=*head;
			while(temp->next->next!=NULL)
                        	temp=temp->next;

                        free(temp->next);
			temp->next=NULL;
                }
        }
}

int delAtPos(int pos,n**head){

	int c= countNode(*head);
	if(pos<=0 || pos>c || *head == NULL){
	
		return -1;
	} else {
	
		if(pos==1){

			deleteFirst(head);

		}else if(pos==c){
		
			deleteLast(head);
		}else{
		
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;
			}
			n*temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);
		}
	}
}

int printLL(n*head){

	if(head==NULL){
	
		return -1;
	} else {
	
		while(head->next!=NULL){
		
			printf("|%d|->",head->d);
			head=head->next;
		}
			printf("|%d|\n",head->d);
			return 0;
	}
}

int concatprime(n**temp1,n**temp2){
	
	n*temp3=*temp1,*temp4=*temp2;
	int p = countNode(temp3);
	if(*temp1==NULL ){
			
		return -1;

	} else {
	
		while(temp4!=NULL && temp4->next!=NULL){
		
			temp4=temp4->next;
		}

		while(temp3!=NULL){
		
			int p=temp3->d;
			int m=0,j=0;
			 while(p>0){

                                m+= p %10;
                                p/=10;
                        }
			for(int i=1;i<=m;i++){
			
				if(m%i==0)
					j++;
			}
			if(j<=2){
				n* newNode= (n*)malloc(sizeof(n));
				newNode->d = temp3->d;
				if(*temp2==NULL){
				
					*temp2=newNode;
					temp4=newNode;
				} else {
				
					temp4->next=newNode;
					temp4=temp4->next;
				}

				newNode->next=NULL;
			}
				temp3=temp3->next;
		}
}
}
void main(){

	n*head1= NULL;
	n*head2=NULL;
        char choice;
        do{
                printf("1.addNode1\n");
                printf("2.addfirst\n");
                printf("3.addAtposition\n");
                printf("4.addLast\n");
                printf("5.deletefirst\n");
                printf("6.deletelast\n");
                printf("7.deleteAtPos\n");
                printf("8.countNodes\n");
                printf("9.concat prime number nodes\n");
                printf("10.printll\n");
                printf("11.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:{
				       int c;
				       printf("Which List want to add\n");
				       scanf("%d",&c);
				       if(c==1)
                                		addNode(&head1);
				       else if(c==2)
					       addNode(&head2);
				        else
                                               printf("Invalid list\n");
			       }
                                break;

                        case 2:
				{
                                	int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
				       if(c==1)
					addFirst(&head1);
				       else if(c==2)
					       addFirst(&head2);
				       else
					       printf("Invalid list\n");
				}
                                break;

                        case 3:
                                {
					int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
				       if(c==1){
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(pos,&head1);
				       }
				       else if(c==2){
				       
					       int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(pos,&head2);

				       } else
                                               printf("Invalid list\n");
                                }
                                break;

                        case 4:
				{
				 int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
                                       if(c==1)
                                        addLast(&head1);
                                       else if(c==2)
                                               addLast(&head2);
                                       else
                                               printf("Invalid list\n");
                                
				}
                                break;

                        case 5:{
				       int c;
                                       printf("Which List want to deletFirst\n");
                                       scanf("%d",&c);
                                       if(c==1){
				       
					       int t=deleteFirst(&head1);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
				       }
                                       else if(c==2){
				      	int t=deleteFirst(&head2);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n"); 
				       }
                                       else
                                               printf("Invalid list\n");
			       }
                                break;

                        case 6:
				{
                                	int c;
                                       printf("Which List want to deleteLast\n");
                                       scanf("%d",&c);
                                       if(c==1){

                                               int t=deleteLast(&head1);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
                                       }
                                       else if(c==2){
                                        int t=deleteLast(&head2);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
                                       }
                                       else
                                               printf("Invalid list\n");
				}
                                break;
                        case 7:
                                {
					int c;
					printf("Enter List\n");
					scanf("%d",&c);
					if(c==1){
                                        int pos;
                                        printf("Enter pos\n");
                                        scanf("%d",&pos);
                                        int y=delAtPos(pos,&head1);
					if(y==-1){
					
						printf("Invalid operation\n");
					}
					}
					else if(c==2){
					 int pos;
                                        printf("Enter pos\n");
                                        scanf("%d",&pos);
                                        int y=delAtPos(pos,&head2);
                                        if(y==-1){

                                                printf("Invalid operation\n");
                                        }
					} else{
					
						printf("Invalid List\n");
					}
                                }
                                break;

                        case 8:{
				       int c;
                                        printf("Enter List\n");
                                        scanf("%d",&c);
                                	if(c==1)
				       printf("Total number of nodes are %d\n",countNode(head1));
					else if(c==2)
						 printf("Total number of nodes are %d\n",countNode(head2));
					else
						 printf("Invalid List\n");
			       }
                                break;
                        case 9:
                                {	
                                      int y=concatprime(&head1,&head2);
					if(y==-1)
						printf("Invalid operation\n");
 				}
                                break;

                        case 10:
				{
					int c;
					printf("choose list\n");
					scanf("%d",&c);
					if(c==1){
                                	int p=printLL(head1);
					if(p==-1)
						printf("No node\n");
					}
					else if(c==2){
				
                                        int p=printLL(head2);
                                        if(p==-1)
                                                printf("No node\n");
                                        }
					else
						printf("Invalid operation\n");
					
				}
                                break;

                        case 11:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }
		 getchar();
                printf("Do you want to continue \n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
