//Question 5: Write program that accept two sll by taking range from user and concat nodes ;

#include<stdio.h>
#include<stdlib.h>

typedef struct Node n;
struct Node{

	int d;
	struct Node* next;
};

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));

	if(newNode==NULL){
	
		printf("Memory full\n");
		exit(-1);
	}
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	return newNode;
}

int addNode(n**head){

	n*newNode=createNode();

	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		n*temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}

		temp->next=newNode;
	}
	return 0;
}

int countNode(n*head){

	int c=0;

	while(head!=NULL){
		c++;
		head=head->next;
	}
	return c;
}

int addFirst(n**head){

	n*newNode=createNode();
	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		newNode->next=*head;
		*head=newNode;
	}
}

int addLast(n**head){

	addNode(head);
}

int addAtPos(int pos,n**head){

	int c=countNode(*head);
	if(pos<=0 || pos>c+1){
	
		return -1;
	}else{
	
		if(pos==1)
			addFirst(head);
		else if(pos==c+1)
			addLast(head);
		else{
			n*newNode=createNode();
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;

			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	}
}

int deleteFirst(n**head){

	if(*head==NULL){
	
		return -1;
	} else {
	
		if((*head)->next==NULL){
		
			free(*head);
			*head=NULL;
		}else{
		
			n*temp=*head;
			*head=(*head)->next;
			free(temp);
		}
	}
}

int deleteLast(n**head){

	if(*head==NULL){

                return -1;
        } else {

                if((*head)->next==NULL){

                        free(*head);
                        *head=NULL;
                }else{

                        n*temp=*head;
			while(temp->next->next!=NULL)
                        	temp=temp->next;

                        free(temp->next);
			temp->next=NULL;
                }
        }
}

int delAtPos(int pos,n**head){

	int c= countNode(*head);
	if(pos<=0 || pos>c || *head == NULL){
	
		return -1;
	} else {
	
		if(pos==1){

			deleteFirst(head);

		}else if(pos==c){
		
			deleteLast(head);
		}else{
		
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;
			}
			n*temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);
		}
	}
}

int printLL(n*head){

	if(head==NULL){
	
		return -1;
	} else {
	
		while(head->next!=NULL){
		
			printf("|%d|->",head->d);
			head=head->next;
		}
			printf("|%d|\n",head->d);
			return 0;
	}
}

int concatR(n*temp1,n*temp2,int s,int e){

	int p = countNode(temp1);

	if(temp1==NULL || temp2==NULL){
	
		return -1;
	} else {
	
		if(s<=0||s>p || e<=0 ||e>p || s>e){
		
			return -1;
		}
		else{
			int q=s-1;	
			while(temp2->next!=NULL){
				temp2=temp2->next;
			}
			while(q-- ){
			
				temp1=temp1->next;
			}
			for(int i=s;i<=e;i++){
				n*newNode=(n*)malloc(sizeof(n));
				newNode->d=temp1->d;
				temp2->next=newNode;
				temp2=temp2->next;
				temp1=temp1->next;
				newNode->next=NULL;
			}
			return 0;
		}
	}
}

void main(){

	n*head1= NULL;
	n*head2=NULL;
        char choice;
        do{
                printf("1.addNode1\n");
                printf("2.addfirst\n");
                printf("3.addAtposition\n");
                printf("4.addLast\n");
                printf("5.deletefirst\n");
                printf("6.deletelast\n");
                printf("7.deleteAtPos\n");
                printf("8.countNodes\n");
                printf("9.concat range of nodes\n");
                printf("10.printll\n");
                printf("11.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:{
				       int c;
				       printf("Which List want to add\n");
				       scanf("%d",&c);
				       if(c==1)
                                		addNode(&head1);
				       else if(c==2)
					       addNode(&head2);
				        else
                                               printf("Invalid list\n");
			       }
                                break;

                        case 2:
				{
                                	int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
				       if(c==1)
					addFirst(&head1);
				       else if(c==2)
					       addFirst(&head2);
				       else
					       printf("Invalid list\n");
				}
                                break;

                        case 3:
                                {
					int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
				       if(c==1){
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(pos,&head1);
				       }
				       else if(c==2){
				       
					       int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(pos,&head2);

				       } else
                                               printf("Invalid list\n");
                                }
                                break;

                        case 4:
				{
				 int c;
                                       printf("Which List want to add\n");
                                       scanf("%d",&c);
                                       if(c==1)
                                        addLast(&head1);
                                       else if(c==2)
                                               addLast(&head2);
                                       else
                                               printf("Invalid list\n");
                                
				}
                                break;

                        case 5:{
				       int c;
                                       printf("Which List want to deletFirst\n");
                                       scanf("%d",&c);
                                       if(c==1){
				       
					       int t=deleteFirst(&head1);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
				       }
                                       else if(c==2){
				      	int t=deleteFirst(&head2);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n"); 
				       }
                                       else
                                               printf("Invalid list\n");
			       }
                                break;

                        case 6:
				{
                                	int c;
                                       printf("Which List want to deleteLast\n");
                                       scanf("%d",&c);
                                       if(c==1){

                                               int t=deleteLast(&head1);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
                                       }
                                       else if(c==2){
                                        int t=deleteLast(&head2);

                                        if(t==-1)
                                                printf("Delete operation cannot be done\n");
                                       }
                                       else
                                               printf("Invalid list\n");
				}
                                break;
                        case 7:
                                {
					int c;
					printf("Enter List\n");
					scanf("%d",&c);
					if(c==1){
                                        int pos;
                                        printf("Enter pos\n");
                                        scanf("%d",&pos);
                                        int y=delAtPos(pos,&head1);
					if(y==-1){
					
						printf("Invalid operation\n");
					}
					}
					else if(c==2){
					 int pos;
                                        printf("Enter pos\n");
                                        scanf("%d",&pos);
                                        int y=delAtPos(pos,&head2);
                                        if(y==-1){

                                                printf("Invalid operation\n");
                                        }
					} else{
					
						printf("Invalid List\n");
					}
                                }
                                break;

                        case 8:{
				       int c;
                                        printf("Enter List\n");
                                        scanf("%d",&c);
                                	if(c==1)
				       printf("Total number of nodes are %d\n",countNode(head1));
					else if(c==2)
						 printf("Total number of nodes are %d\n",countNode(head2));
					else
						 printf("Invalid List\n");
			       }
                                break;
                        case 9:
                                {	
					int c,l;
					printf("Enter lower limit\n");
					scanf("%d",&c);
					printf("Enter upper limit\n");
					scanf("%d",&l);
                                      int y=concatR(head1,head2,c,l);
					if(y==-1)
						printf("Invalid operation\n");
 				}
                                break;

                        case 10:
				{
					int c;
					printf("choose list\n");
					scanf("%d",&c);
					if(c==1){
                                	int p=printLL(head1);
					if(p==-1)
						printf("No node\n");
					}
					else if(c==2){
				
                                        int p=printLL(head2);
                                        if(p==-1)
                                                printf("No node\n");
                                        }
					else
						printf("Invalid operation\n");
					
				}
                                break;

                        case 11:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }
		 getchar();
                printf("Do you want to continue \n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
