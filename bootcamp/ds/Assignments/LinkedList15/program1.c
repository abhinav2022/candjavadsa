//Question 1: Write program that searches all occurence of a particular element in SLLL;

#include<stdio.h>
#include<stdlib.h>      

typedef struct Node n;		//Alias for struct Node
struct Node{

	int d;
	struct Node* next;
};

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));

	if(newNode==NULL){
	
		printf("Memory full\n");		//for condtion of memory full
		exit(-1);
	}
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	return newNode;
}

int addNode(n**head){

	n*newNode=createNode();

	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		n*temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}

		temp->next=newNode;		//adding node at last
	}
	return 0;
}

int countNode(n*head){

	int c=0;

	while(head!=NULL){
		c++;
		head=head->next;
	}				
	return c;		//returning count in main
}

int addFirst(n**head){

	n*newNode=createNode();
	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		newNode->next=*head;
		*head=newNode;		//adding node at first position 
	}
}

int addLast(n**head){

	addNode(head);		//adding node at last
}

int addAtPos(int pos,n**head){		//funtion to add node at required position

	int c=countNode(*head);
	if(pos<=0 || pos>c+1){
	
		return -1;
	}else{
	
		if(pos==1)
			addFirst(head);
		else if(pos==c+1)
			addLast(head);
		else{
			n*newNode=createNode();
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;

			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	}
}

int deleteFirst(n**head){

	if(*head==NULL){
	
		return -1;
	} else {
	
		if((*head)->next==NULL){
		
			free(*head);
			*head=NULL;
		}else{
		
			n*temp=*head;
			*head=(*head)->next;
			free(temp);
		}
	}
}

int deleteLast(n**head){

	if(*head==NULL){

                return -1;
        } else {

                if((*head)->next==NULL){

                        free(*head);
                        *head=NULL;
                }else{

                        n*temp=*head;
			while(temp->next->next!=NULL)
                        	temp=temp->next;

                        free(temp->next);
			temp->next=NULL;
                }
        }
}

int delAtPos(int pos,n**head){

	int c= countNode(*head);
	if(pos<=0 || pos>c || *head == NULL){
	
		return -1;
	} else {
	
		if(pos==1){

			deleteFirst(head);

		}else if(pos==c){
		
			deleteLast(head);
		}else{
		
			n*temp=*head;

			while((pos--)-2){
			
				temp=temp->next;
			}
			n*temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);
		}
	}
}

int printLL(n*head){

	if(head==NULL){
	
		return -1;
	} else {
	
		while(head->next!=NULL){
		
			printf("|%d|->",head->d);
			head=head->next;
		}
			printf("|%d|\n",head->d);
			return 0;
	}
}

int count(int num,n*temp){

	int c=0;

	if(temp==NULL){
	
		return -1;
	} else {
	
		while(temp!=NULL){	//traversing through whole linkedllist for getting count of number
		
			if(temp->d==num)
				c++;

			temp=temp->next;
		}
		return c;
	}
}

void main(){

	n*head= NULL;

        char choice;
        do{
                printf("1.addNode\n");
                printf("2.addfirst\n");
                printf("3.addAtposition\n");
                printf("4.addLast\n");
                printf("5.deletefirst\n");
                printf("6.deletelast\n");
                printf("7.deleteAtPos\n");
                printf("8.countNodes\n");
                printf("9.countOfNodes\n");
                printf("10.printll\n");
                printf("11.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                addNode(&head);
                                break;

                        case 2:
                                addFirst(&head);
                                break;

                        case 3:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(pos,&head);
                                }
                                break;

                        case 4:
                                addLast(&head);
                                break;

                        case 5:{
				int c=deleteFirst(&head);
					
					if(c==-1)
						printf("Delete operation cannot be done\n");

			       }
                                break;

                        case 6:
				{
                                	int c=deleteLast(&head);
					if(c==-1)
						printf("No node to delete\n");
				}
                                break;
                        case 7:
                                {
                                        int pos;
                                        printf("Enter pos\n");
                                        scanf("%d",&pos);
                                        int y=delAtPos(pos,&head);
					if(y==-1){
					
						printf("Invalid operation\n");
					}
                                }
                                break;

                        case 8:
                                printf("Total number of nodes are %d\n",countNode(head));
                                break;
                        case 9:
                                {
                                        int num;
                                        printf("Enter number\n");
                                        scanf("%d",&num);
                                        int y=count(num,head);
					if(y==-1)
						printf("No node present\n");
					else
						printf("count is %d for %d\n",y,num);
                                }
                                break;

                        case 10:
				{
                                	int p=printLL(head);
					if(p==-1)
						printf("No node\n");
				}
                                break;

                        case 11:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }
		 getchar();
                printf("Do you want to continue \n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
