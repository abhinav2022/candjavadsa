
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	struct Node*next;
}n;

n* createNode(){

	n*newNode = (n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);

	newNode->next=NULL;

	return newNode;
}

void addNode(n**head){

	n*newNode=createNode();

	if(*head==NULL){
	
		*head=newNode;
	}else{
	
		n*temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		
		temp->next=newNode;
	}
}

int countNode(n*temp){

	int c=0;

	while(temp!=NULL){
	
		c++;
		temp=temp->next;
	}
	return c;
}

int addNpos(n**head1,n**head2,int num){

	int c = countNode(*head2);
	n*temp1=*head1;
	n*temp2=*head2;
	if(num>c || c<0){
	
		printf("Invalid operation\n");
		return -1;
	}
	while(temp1->next!=NULL){
	
		temp1=temp1->next;
	}	

	int d = c-num;
	while(d--){
	
		temp2=temp2->next;
	}
	temp1->next=temp2;

	return 0;
}
void printLL(n*temp){

	while(temp->next!=NULL){
	
		printf("|%d|->",temp->d);
		temp=temp->next;
	}
		printf("|%d|\n",temp->d);
}

void main(){

	n*head1=NULL;
	n*head2=NULL;

	int a;
	printf("Enter number of nodes\n");
	scanf("%d",&a);

	for(int i=0;i<a;i++){
	
		addNode(&head1);
	}
	printf("Enter number of nodes\n");
	scanf("%d",&a);

	for(int i=0;i<a;i++){
	
		addNode(&head2);
	}

	printf("Enter nodes\n");
	scanf("%d",&a);
	printLL(head1);
	printLL(head2);

	addNpos(&head1,&head2,a);
	printLL(head1);
}
