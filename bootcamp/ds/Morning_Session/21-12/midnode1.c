
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	struct Node*next;
}n;

n* createNode(){

	n*newNode=(n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	return newNode;
}

int addNode(n**head){

	n*newNode=createNode();

	if(newNode==NULL){
	
		printf("Memory full\n");
		exit(-1);
	}else{
	
		if(*head==NULL){
		
			*head=newNode;

		} else {
		
			n*temp=*head;

			while(temp->next!=NULL){
			
				temp=temp->next;
			}
			temp->next=newNode;
		}
	}
}

int printLL(n*head){

	if(head==NULL){
	
		printf("Empty\n");
	}
	else{
	
		while(head->next!=NULL){
		
			printf("%d->",head->d);
			head=head->next;
		}
		printf("%d\n",head->d);
	}
}
int countNode(n*head){

	int c=0;

	while(head!=NULL){
	
		c++;
		head=head->next;
	}
	return c;
}

int midNode(n*head){

	n*fast=head;
	while(fast!=NULL && fast->next!=NULL){
	
		fast=fast->next->next;
		head=head->next;
	}
	return head->d;
}

int pal(n*head){

	int p=1,cnt=countNode(head);

	n*temp=head;

	int arr[cnt];

	for(int i=0;i<cnt;i++){
	
		*(arr+i)=temp->d;
		temp=temp->next;
	}

	int start=0,end=cnt-1;
	while(start<end){
	
		if(arr[start]==arr[end])
			p=1;
		else{
			p=0;
			break;
		}
		start++;
		end--;
	}
	return p;
}

void main(){

	n*head=NULL;
	int a;
	printf("Enter number\n");
	scanf("%d",&a);

	for(int i=0;i<a;i++){
	
		addNode(&head);
	}
	printLL(head);

	printf("\n%d\n",midNode(head));

	if(pal(head)){
	
		printf("palindrome\n");
	}else{
	
		printf("Not palindrome\n");
	}
}
