
#include<stdio.h>
#include<stdlib.h>

typedef struct processor{

	char name[40];
	int core;
	int thread;
	float freq;
	int gen;
	int cache;
	struct processor* next;
}p;

p* createNode(){

	p* newNode=(p*)malloc(sizeof(p));

	int i=0;
	char ch;
	getchar();
	printf("Enter name\n");
	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	printf("Enter cores\n");
	scanf("%d",&newNode->core);
	printf("Enter thread\n");
	scanf("%d",&newNode->thread);
	printf("Enter freq\n");
	scanf("%f",&newNode->freq);
	printf("Enter gen\n");
	scanf("%d",&newNode->gen);
	printf("Enter cache\n");
	scanf("%d",&newNode->cache);

	newNode->next=NULL;
}

p* addNode(p*head){

	p* newNode = createNode();

	if(head==NULL){
	
		return head=newNode;
	}else{
	
		p* temp= head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		return temp->next=newNode;
	}
}

void printll(p*newNode){

	while(newNode!=NULL){
	
		printf("|%s -> ",newNode->name);
		printf("%d -> ",newNode->core);
		printf("%.2f -> ",newNode->freq);
		printf("%d -> ",newNode->thread);
		printf("%d MB| ",newNode->cache);
		newNode=newNode->next;
	}
	printf("\n");
}

void main(){

	p* head = NULL;

	int n;

	printf("Enter numbers\n");
	scanf("%d",&n);

	for(int i=0;i<n;i++){
	
		if(i==0)
			head = addNode(head);
		else
			addNode(head);
	}
	printll(head);
}
