
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	struct Node*next;
}n;

int countNode(n*temp){

	int c=0;
	n*temp2=temp;
	if(temp==NULL){
	
		return 0;
	}else{
	while(temp->next!=temp2){
		c++;
		temp=temp->next;
	}
	return ++c;
	}
}

n* createNode(){

	n* newNode = (n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);

	newNode->next=NULL;

	return newNode;
}

void addNode(n**head){

	n*newNode = createNode();
	
	if(*head==NULL){
		*head=newNode;
		newNode->next=*head;
	}else{
	
		n* temp=*head;

		while(temp->next!=*head){
		
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=*head;
	}
}

void addFirst(n**head){

	n* newNode= createNode();

	if(*head==NULL){
	
		*head=newNode;
		newNode->next=*head;
	}else{
	
		n*temp=*head;

		while(temp->next!=*head)
			temp=temp->next;

		newNode->next=*head;
		temp->next=newNode;
		*head=newNode;
	}
}

void addLast(n**head){

	addNode(head);
}

int addAtPos(n**head,int pos){
	int count=countNode(*head);
	if(pos<=0 || pos>count+1){
	
		printf("Wrong position \n");
		return -1;
	}else if(pos==1){
		addFirst(head);
	}else if(pos==count+1){
	
		addLast(head);
	}else{
	
		n*newNode= createNode();
		n*temp=*head;

		while((pos--)-2){
		
			temp=temp->next;

		}
		newNode->next=temp->next;
		temp->next=newNode;		
	}
}

void delFirst(n**head){

	if(*head==NULL){
	
		printf("NO nodes\n");
	}else if((*head)->next==*head){
	
		free(*head);
		*head=NULL;
	}else{
	
		n*temp=*head;
		
		while(temp->next!=*head){
		
			temp=temp->next;
		}
		temp->next=temp->next->next;

		free(*head);
		*head=temp->next;
	}
}

void delLast(n**head){

	if(*head==NULL){
	
		printf("No nodes\n");
	}else if((*head)->next==*head){
	
		free(*head);
		*head=NULL;
	}else{
	
		n*temp= *head;

		while(temp->next->next!=*head)
			temp=temp->next;
	
		free(temp->next);
		temp->next=*head;
	}
}

void delAtPos(n**head,int pos){
	int c = countNode(*head);
	if(pos<=0 ||pos>c+1){
		
		printf("Invalid node %d total node %d\n",pos,c);
	}else if(pos==1){
		delFirst(head);
	}else if(pos==c){
		delLast(head);
	}else{
	
		n*temp1= *head;
		n*temp2;

		while((pos)-2){
			temp1=temp1->next;
			pos--;
		}
		temp2=temp1->next;
		temp1->next=temp1->next->next;
		free(temp2);
	}
}

void printLL(n*temp){

	n*temp2=temp;
	if(temp==NULL){
	
		printf("NO nodes\n");
	}else{

		while(temp->next!=temp2){
	
			printf("|%d|->",temp->d);
			temp=temp->next;
		}
		
		printf("|%d|\n",temp->d);
	}
}

void main(){

	n*head=NULL;

	char choice;

        do{

                int n;
                printf("1.AddNode\n");
                printf("2.AddFirst\n");
                printf("3.AddLast\n");
                printf("4.AddAtPos\n");
                printf("5.delFirst\n");
                printf("6.delLast\n");
                printf("7.delAtPos\n");
                printf("8.printLL\n");
                printf("9.countNode\n");
                //printf("10.focc\n");
                printf("11.Exit\n");
                printf("Your choice:-");
                scanf("%d",&n);

                switch(n){

                        case 1:
                                addNode(&head);
                                break;

                        case 2:
                                addFirst(&head);
                                break;

                        case 3:
                                addLast(&head);
                                break;

                        case 4:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);
                                        addAtPos(&head,pos);
                                }
                                break;
			case 5:
                                delFirst(&head);
                                break;

                        case 6:
                                delLast(&head);
                                break;

                        case 7:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);
                                        delAtPos(&head,pos);
                                }
                                break;

                        case 8:
                                printLL(head);
                                break;

                        case 9:
                                printf("Total number of Nodes are %d\n",countNode(head));
                                break;

                        /*case 10:
                                {
                                int pl;
                                printf("Enter number\n");
                                scanf("%d",&pl);
                                c(head,pl);
                                }
                                break;*/

                        case 11:
                                printf("Exit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice\n");
                                break;
                }
		getchar();
                printf("Do you want to continue\n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
