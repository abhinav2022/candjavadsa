
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	struct Node*next;
}n;

void addAtPos(n**,int);
int countNode(n*head){

	int c=0;

	while(head!=NULL){
	
		c++;
		head=head->next;
	}
	return c;
}

n* createNode(){

	n* newNode = (n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;

	return newNode;
}

void addNode(n**head){

	n* newNode = createNode();
	
	if(*head==NULL){
	
		*head=newNode;

	}else{
	
		n * temp = *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void addFirst(n**head){

	n* temp = createNode();
	if(*head==NULL){
	
		*head = temp;

	}else{
	
		temp->next=*head;
		*head = temp;
	}
}

void addLast(n**head){

	addNode(head);
}

void addAtPos(n**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("No node present\n");
	}
	else{
	
		if(pos==1){
		
			addFirst(head);
		}else if(pos==countNode(*head)+1){
		
			addLast(head);
		}else{
		
			n*newNode = createNode();
			n*temp =*head;
			while(pos-2){
			
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
			
		}
	}
}

void delFirst(n**head){

	if(*head==NULL){
	
		printf("No nodes\n");
	}else if((*head)->next==NULL){
	
		free(*head);
		(*head)=NULL;
	}else{
	
		n*temp=*head;

		*head=temp->next;

		free(temp);
	}
}

void delLast(n**head){

	if(*head==NULL){
	
		printf("No node present\n");
	}else{
	
		if((*head)->next==NULL){
		
			free(*head);
			*head=NULL;

		}else{
			
			n*temp = *head;
		
			while(temp->next->next!=NULL){
			
				temp=temp->next;
			}

			free(temp->next);
			temp->next=NULL;
		}
	}	
}

void printLL(n*temp){

	if(temp==NULL)
		printf("No nodes present\n");

	else{
	while(temp->next!=NULL){
	
		printf("|%d|->",temp->d);
		temp=temp->next;
	}
	printf("|%d|\n",temp->d);
	}
}

void delAtPos(n**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("Invalid node position %d of total nodes %d\n",pos,countNode(*head));

	}else if(*head==NULL){
	
		printf("No node present\n");
	}else if(pos==1){
	
		delFirst(head);
	}else if(pos==countNode(*head)){
	
		delLast(head);
	}else{
	
		n*temp = *head;
		n* pl = NULL;
		while((pos--)-2){
		
			temp=temp->next;
		}
		pl = temp->next;

		temp->next=temp->next->next;
		free(pl);
	}
}

void focc(n*head){

	int c = countNode(head);

	if(head==NULL){
	
		printf("No nodes present\n");
	}else if(head->next==NULL){
	
		printf("Only one node\n");
	}
	else{
	int d=c,a=c,cnt=c/2;

	n*temp1=head;
	n*temp2=head;
	while(cnt){
	
		while(a-1>0){
		
			temp1=temp1->next;
			a--;
		}
		int ite=temp2->d;
		temp2->d=temp1->d;
		temp1->d=ite;

		temp1=head;
		temp2=temp2->next;
		a=--d;
		cnt--;
	}
	}
}

void main(){

	n*head = NULL;
	char choice;

	do{
	
		int n;
		printf("1.AddNode\n");
		printf("2.AddFirst\n");
		printf("3.AddLast\n");
		printf("4.AddAtPos\n");
		printf("5.delFirst\n");
		printf("6.delLast\n");
		printf("7.delAtPos\n");
		printf("8.printLL\n");
		printf("9.countNode\n");
		printf("10.focc\n");
		printf("11.Exit\n");
		printf("Your choice:-");
		scanf("%d",&n);

		switch(n){
		
			case 1:
				addNode(&head);
				break;

			case 2:
				addFirst(&head);
				break;

			case 3:
				addLast(&head);
				break;

			case 4:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);
					addAtPos(&head,pos);
				}
				break;

			case 5:
				delFirst(&head);
				break;

			case 6:
				delLast(&head);
				break;

			case 7:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);
					delAtPos(&head,pos);
				}
				break;

			case 8:
				printLL(head);
				break;

			case 9:
				printf("Total number of Nodes are %d\n",countNode(head));
				break;

			case 10:
				focc(head);
				break;

			case 11:
				printf("Exit\n");
				exit(0);
				break;

			default:
				printf("Wrong choice\n");
				break;

		}

		getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
