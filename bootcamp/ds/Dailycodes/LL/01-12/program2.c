
#include<stdio.h>
#include<stdlib.h>

typedef struct Movie mv;

struct Movie {

	char name[20];
	float imdb;
	struct Movie *next;
};

mv * head = NULL;

void ch(char *str){

        while(*str!='\0'){

                if(*str=='\n'){

                        *str='\0';
			break;
                }
                str++;
        }
};
void addNode(){

	mv* newNode = (mv*)malloc(sizeof(mv));

	printf("Enter name\n");
	fgets(newNode->name,18,stdin);
	ch(newNode->name);
	printf("Enter rating\n");
	scanf("%f",&newNode->imdb);
	getchar();

	if(head==NULL)
		head = newNode;

	else{
	
		mv * temp = head;

		while(temp->next!=NULL){
			temp= temp->next;
		}
		temp->next = newNode;
	}
}

void printll(){

	mv * temp = head;

	while(temp!=NULL){
	
		printf("|%s ->",temp->name);
		printf(" %.2f| ",temp->imdb);

		temp= temp->next;
	}
	printf("\n");
}

void main(){

	addNode();
	addNode();
	printll();
}
