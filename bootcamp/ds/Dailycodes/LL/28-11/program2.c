//Self referencial structure
#include<stdio.h>
#include<string.h>
typedef struct Employee{

	int EmpId;
	char empName[20];
	float sal;
	struct Employee* next;
}Emp;
void main(){

	Emp obj1,obj2,obj3;	
	obj1.EmpId = 1;
	strcpy(obj1.empName,"Kanha");
	obj1.sal=50.00;
	obj1.next = &obj2;

	obj2.EmpId = 2;
	strcpy(obj2.empName,"Rahul");
	obj2.sal=60.00;
	obj2.next = &obj2;

	obj3.EmpId = 3;
	strcpy(obj3.empName,"Ashish");
	obj3.sal=70.00;
	obj3.next = NULL;

	printf("%d\n",obj1.EmpId);
	printf("%s\n",obj1.empName);
	printf("%f\n",obj1.sal);
	
	printf("%d\n",obj2.EmpId);
	printf("%s\n",obj2.empName);
	printf("%f\n",obj2.sal);
	
	printf("%d\n",obj3.EmpId);
	printf("%s\n",obj3.empName);
	printf("%f\n",obj3.sal);
}
