
#include<stdio.h>
#include<string.h>

typedef struct Movie{

	float imdb;
	char name[20];
	struct Movie* next;
}mv;

void main(){

	mv o1,o2;

	mv*head = &o1;
	
	head->imdb=3;
	strcpy(head->name,"Brahmastra");
	head->next= &o2;

	head->next->imdb=8.8;
	strcpy(head->next->name,"Drishyam 2");
	head->next->next = NULL;

	mv*temp = head;

	while(temp!=NULL){
	
		printf("%f\n",temp->imdb);
		printf("%s\n",temp->name);
		printf("%p\n",temp->next);
		temp=temp->next;
	}
};
