//Self referencial structure
#include<stdio.h>
#include<string.h>
typedef struct Employee{

	int EmpId;
	char empName[20];
	float sal;
	struct Employee* next;
}Emp;
void main(){

	Emp obj1,obj2,obj3;
	struct Employee *head = &obj1;
	obj1.EmpId = 1;
	strcpy(obj1.empName,"Kanha");
	obj1.sal=50.00;
	obj1.next = &obj2;

	obj2.EmpId = 2;
	strcpy(obj2.empName,"Rahul");
	obj2.sal=60.00;
	obj2.next = &obj3;

	obj3.EmpId = 3;
	strcpy(obj3.empName,"Ashish");
	obj3.sal=70.00;
	obj3.next = NULL;

	printf("%d\n",head->EmpId);
	printf("%s\n",head->empName);
	printf("%f\n",head->sal);
	
	printf("%d\n",head->next->EmpId);
	printf("%s\n",head->next->empName);
	printf("%f\n",head->next->sal);
	
	printf("%d\n",(*(*head).next).EmpId);
	printf("%s\n",(*((*head).next)).empName);
	printf("%f\n",head->next->sal);
	
	printf("%d\n",(*((*((*head).next)).next)).EmpId);
	printf("%s\n",head->next->next->empName);
	printf("%f\n",head->next->next->sal);
}
