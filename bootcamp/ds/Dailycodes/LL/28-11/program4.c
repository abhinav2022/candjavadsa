//Self referencial structure
#include<stdio.h>
#include<string.h>
typedef struct Batsman{

	int jerNo;
	char Name[20];
	float avg;
	struct Batsman* next;
}Bn;
void main(){

	Bn obj1,obj2,obj3;
	Bn *head = &obj1;
	obj1.jerNo = 1;
	strcpy(obj1.Name,"KL Rahul");
	obj1.avg=3.00;
	obj1.next = &obj2;
	
	obj2.jerNo = 2;
	strcpy(obj2.Name,"Vadapav");
	obj2.avg=5.00;
	obj2.next = &obj3;

	obj3.jerNo = 3;
	strcpy(obj3.Name,"Jadeja");
	obj3.avg=5.00;
	obj3.next = NULL;

	printf("%d\n",head->jerNo);
	printf("%s\n",head->Name);
	printf("%f\n",head->avg);
	
	printf("%d\n",head->next->jerNo);
	printf("%s\n",head->next->Name);
	printf("%f\n",head->next->avg);
	
	printf("%d\n",(*(*head).next).jerNo);
	printf("%s\n",(*((*head).next)).Name);
	printf("%f\n",head->next->avg);
	
	printf("%d\n",(*((*((*head).next)).next)).jerNo);
	printf("%s\n",(*((*((*head).next)).next)).Name);
	printf("%f\n",head->next->next->avg);
}
