
#include<stdio.h>
#include<stdlib.h>
char *mystrcpy(char*dest,char*src){

	while(*src!='\0'){
	
		*dest=*src;
		dest++;
		src++;
	}
	*dest='\0';
}

struct Apartment{

	char name[20];
	char wing;
	struct flat{
	
		int flatno;
		float BHK;
		float price;
	}obj;
};

void main(){

	struct Apartment *ptr =(struct Apartment*)malloc(sizeof(struct Apartment));
	mystrcpy(ptr->name,"Sun Universe");
	ptr->wing='A';
	ptr->obj.flatno=310;
	ptr->obj.BHK=3.0;
	ptr->obj.price=3.0;
	
	printf("%s\n",ptr->name);

	printf("%c\n",(*ptr).wing);
	printf("%d\n",(*ptr).obj.flatno);
	printf("%.1f\n",(*ptr).obj.BHK);
	printf("%f Cr.\n",(*ptr).obj.price);
	free(ptr);
}
