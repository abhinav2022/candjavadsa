
#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{

	char name[20];
	int id;
	struct Employee * next;
	
}Emp; 

int countNode(Emp*h){

	int c=0;
	while(h!=NULL){
	
		c++;
		h=h->next;		
	}
//	printf("Count is %d\n",c);

	return c;
}

Emp* createNode(){

	Emp* newNode= (Emp*)malloc(sizeof(Emp));

	char ch;
	int i=0;
	getchar();

	printf("Enter name\n");
	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	printf("Enter id\n");
	scanf("%d",&newNode->id);
	newNode->next=NULL;
	return newNode;
}

Emp* addNode(Emp* head){

	Emp* newNode=createNode();
	
		if(head==NULL){
		
			return head=newNode;
		}else{
		
			Emp* temp= head;

			while(temp->next!=NULL){
			
				temp=temp->next;
			}
	
			temp->next=newNode;
		}
}

void addLast(Emp*head){

	Emp* temp = createNode();

	while(head->next!=NULL){
	
		head=head->next;
	}
	head->next=temp;
}

void addPosAt(Emp**head,int pos){

	if(pos<=0 || pos>1+countNode(*head) ){
	
		printf("Wrong position entered %d total nodes are %d\n",pos,countNode(*head));
		return;
	}
	Emp* temp= createNode();
	Emp*temp2 = *head;
	while(pos-2>0){
	
		temp2=temp2->next;
		pos--;
	}
	if(pos==1){
	
		temp->next=*head;
		*head=temp;
	}else{

	temp->next=temp2->next;
	temp2->next=temp;	
	}
}

void addFirst(Emp**head){

	Emp* temp = createNode();

	temp->next =*head;
	*head = temp;
}

void printLL(Emp* head){

	while(head!=NULL){
	
		printf("|%s -> ",head->name);
		printf("%d | ",head->id);
		head=head->next;
	}
	printf("\n");
}

void main(){

	Emp* head =NULL;
	int n;

	printf("Enter number of nodes\n");
	scanf("%d",&n);

	for(int i=0;i<n;i++){
		
		if(i==0)
			head=addNode(head);

		else
			addNode(head);
	}
	printf("Count is %d\n",countNode(head));
	
	printLL(head);

//	addFirst(&head);
	
//	printLL(head);

//	addLast(head);

//	printLL(head);

	int k;
	printf("Enter position\n");
	scanf("%d",&k);
	addPosAt(&head,k);

	printLL(head);

}
