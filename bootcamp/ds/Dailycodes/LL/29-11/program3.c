
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{

	float imdb;
	char name[40];
	int num;
	struct Movie*next;
}n;
void accessData(n*ptr){

	printf("%f\n",ptr->imdb);
	printf("%s\n",ptr->name);
	printf("%d\n",ptr->num);
	printf("%p\n",ptr->next);
}
void main(){

	n *m1=(n*)malloc(sizeof(n));
	n *m2 = (n*)malloc(sizeof(n));
	m1->next = m2;
	m1->next->next=NULL;


	m1->imdb= 8.8;
	strcpy(m1->name,"Drishyam");
	m1->num=7;

	m1->next->imdb=9.0;
	strcpy(m1->next->name,"Drishyam 2");
	m1->next->num=8;

	accessData(m1);
	accessData(m2);

	free(m1);
	free(m2);
	m1=NULL,m2=NULL;
}
