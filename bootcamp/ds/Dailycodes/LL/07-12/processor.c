
#include<stdio.h>
#include<stdlib.h>

typedef struct processor {

	char name[20];
	char company[20];
	unsigned int core;
	unsigned int thread;
	unsigned int gen;
	unsigned int cache;
	float freq;
	struct processor *next;
}pro; 

pro * createNode(){

	pro *newNode = (pro*)malloc(sizeof(pro));

	char ch;
	int i=0;
	getchar();
	printf("Enter processor name\n");
	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	i=0;
	printf("Enter company name\n");
	while((ch=getchar())!='\n'){
	
		newNode->company[i]=ch;
		i++;
	}
	printf("Enter cores in processor\n");
	scanf("%d",&newNode->core);
	printf("Enter generation\n");
	scanf("%d",&newNode->gen);
	printf("Enter thread\n");
	scanf("%d",&newNode->thread);
	printf("Enter cache\n");
	scanf("%d",&newNode->cache);
	printf("Enter freq number\n");
	scanf("%f",&newNode->freq);

	newNode->next=NULL;

	return newNode;
}

void addNode(pro **head){

	pro * newNode = createNode();

	if(*head == NULL){
	
		*head = newNode;

	}else{
	
		pro *temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int countNode(pro*head){

	int c=0;

	while(head!=NULL){
	
		c++;
		head=head->next;
	}

	return c;
}

void printLL(pro*head){

	
	while(head!=NULL){
	
		printf("|%s -> ",head->name);
		printf("%s -> ",head->company);
		printf("%d -> ",head->core);
		printf("%d -> ",head->thread);
		printf("%d -> ",head->gen);
		printf("%f | ",head->freq);
		head=head->next;
	}
	printf("\n");
}
void addFirst(pro**head){

	pro * temp2 =createNode();

	if(!(*head)){
	
		*head = temp2;
	}else{
	
		temp2->next=*head;
		*head=temp2;
	}
}

void addLast(pro**head){

	addNode(head);
}

void addAtPos(pro**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("Invalid node position %d Total nodes are %d\n",pos,countNode(*head));	

	}else{
	
		if(pos==1){
		
			addFirst(head);

		}else if(pos==countNode(*head)+1){
		
			addNode(head);
		}else{
		
			pro *newNode= createNode();
			pro *temp= *head;

			while(pos-2){
			
				temp=temp->next;
				pos--;
			}

			newNode->next=temp->next;
			temp->next=newNode;
		}
	}
}

void deleteFirst(pro**head){

	if(*head == NULL){
	
		printf("No node present\n");

	}else if((*head)->next == NULL){
	
		free(*head);
		*head = NULL;
	}else{
	
		pro *temp=*head;

		*head = temp->next;

		free(temp);
	}
}

void deleteLast(pro**head){

	if(*head==NULL){
	
		printf("No node present\n");
	}else if((*head)->next==NULL){
	
		free(*head);
		*head=NULL;
	}else{
	
		pro*temp=*head;

		while(temp->next->next!=NULL){
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

void delAtPos(pro**head,int pos){

	if(pos<=0 || pos>countNode(*head)){
	
		printf("Invalid position %d for Total nodes %d\n",pos,countNode(*head));
	}else{
	
		if(*head==NULL){
		
			printf("No nodes present\n");

		}else if(pos==1){
			deleteFirst(head);
			
		}else if(pos==countNode(*head)){
		
			deleteLast(head);			
		}else{
		
			pro*temp1 = *head;

			while((pos--)-2){
			
				temp1=temp1->next;
			}
			pro*temp2 = temp1->next;

			temp1->next = temp1->next->next;
			free(temp2);
		}
	}
}
void main(){

        pro* head= NULL;

        char choice;

        do{
                printf("1.addNode\n");
                printf("2.addfirst\n");
                printf("3.addAtposition\n");
                printf("4.addLast\n");
                printf("5.deletefirst\n");
                printf("6.deletelast\n");
                printf("7.deleteAtPos\n");
                printf("8.countNodes\n");
                printf("9.printll\n");
                printf("10.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);
		
                switch(ch){

                        case 1:
                                addNode(&head);
                                break;

                        case 2:
                                addFirst(&head);
                                break;

                        case 3:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);
                                        addAtPos(&head,pos);
                                }
                                break;

                        case 4:
                                addLast(&head);
                                break;

			case 5:
                                deleteFirst(&head);
                                break;

                        case 6:
                                deleteLast(&head);
                                break;
                        case 7:
				{
					int pos;
					printf("Enter number\n");
					scanf("%d",&pos);
                                	delAtPos(&head,pos);
				}
                                break;

                        case 8:
                                printf("Total number of nodes are %d\n",countNode(head));
                                break;

                        case 9:
                                printLL(head);
                                break;

			case 10:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }

                getchar();
                printf("Do you want to continue \n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
