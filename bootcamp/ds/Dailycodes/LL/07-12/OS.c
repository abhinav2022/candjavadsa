
#include<stdio.h>
#include<stdlib.h>

typedef struct OS {

	char name[20];
	char founder[20];
	unsigned int year;
	unsigned int noVer;
	struct OS *next;
}OS; 

OS * createNode(){

	OS *newNode = (OS*)malloc(sizeof(OS));

	char ch;
	int i=0;
	getchar();
	printf("Enter OS name\n");
	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	i=0;
	printf("Enter founder name\n");
	while((ch=getchar())!='\n'){
	
		newNode->founder[i]=ch;
		i++;
	}
	printf("Enter year of release \n");

	scanf("%d",&newNode->year);
	printf("Enter version number\n");
	scanf("%d",&newNode->noVer);

	newNode->next=NULL;

	return newNode;
}

void addNode(OS **head){

	OS * newNode = createNode();

	if(*head == NULL){
	
		*head = newNode;

	}else{
	
		OS *temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int countNode(OS*head){

	int c=0;

	while(head!=NULL){
	
		c++;
		head=head->next;
	}

	return c;
}

void printLL(OS*head){

	if(head==NULL){
	
		printf("No nodes present\n");
	}
	else{	
	while(head!=NULL){
	
		printf("|%s -> ",head->name);
		printf("%s -> ",head->founder);
		printf("%d -> ",head->year);
		printf("%d | ",head->noVer);
		head=head->next;
	}
	}
		printf("\n");
}
void addFirst(OS**head){

	OS * temp2 =createNode();

	if(!*head){
	
		*head = temp2;
	}else{
	
		temp2->next=*head;
		*head=temp2;
	}
}

void addLast(OS**head){

	addNode(head);
}

void addAtPos(OS**head,int pos){

	if(pos<=0 || pos>countNode(*head)){
	
		printf("Invalid node position %d Total nodes are %d\n",pos,countNode(*head));	

	}else{
	
		if(pos==1){
		
			addFirst(head);

		}else if(pos==countNode(*head)+1){
		
			addNode(head);
		}else{
		
			OS *newNode= createNode();
			OS *temp= *head;

			while(pos-2){
			
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	}
}

void deleteFirst(OS**head){

	if(*head == NULL){
	
		printf("No node present\n");

	}else if((*head)->next == NULL){
	
		free(*head);
		*head = NULL;
	}else{
	
		OS *temp=*head;

		*head = temp->next;

		free(temp);
	}
}

void deleteLast(OS**head){

	if(*head==NULL){
	
		printf("No node present\n");
	}else if((*head)->next==NULL){
	
		free(*head);
		*head=NULL;
	}else{
	
		OS*temp=*head;
		while(temp->next->next!=NULL){
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

void delAtPos(OS**head,int pos){

	if(pos<=0 || pos>countNode(*head)){
	
		printf("Invalid position %d for Total nodes %d\n",pos,countNode(*head));
	}else{
	
		if(*head==NULL){
		
			printf("No nodes present\n");

		}else if(pos==1){
			deleteFirst(head);
			
		}else if(pos==countNode(*head)){
		
			deleteLast(head);			
		}else{
		
			OS*temp1 = *head;

			while((pos--)-2){
			
				temp1=temp1->next;
			}
			OS*temp2 = temp1->next;

			temp1->next = temp1->next->next;
			free(temp2);
		}
	}
}
void main(){

        OS* head= NULL;

        char choice;

        do{
                printf("1.addNode\n");
                printf("2.addfirst\n");
                printf("3.addAtposition\n");
                printf("4.addLast\n");
                printf("5.deletefirst\n");
                printf("6.deletelast\n");
                printf("7.deleteAtpos\n");
                printf("8.countNodes\n");
                printf("9.printll\n");
                printf("10.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);
		
                switch(ch){

                        case 1:
                                addNode(&head);
                                break;

                        case 2:
                                addFirst(&head);
                                break;

                        case 3:
                                {
                                        int pos;
                                        printf("Enter position\n");
                                        scanf("%d",&pos);

                                        addAtPos(&head,pos);
                                }
                                break;

                        case 4:
                                addLast(&head);
                                break;

			case 5:
                                deleteFirst(&head);
                                break;

                        case 6:
                                deleteLast(&head);
                                break;
                        case 7:{
				       int p;
				       printf("Enter number\n");
				       scanf("%d",&p);
                                	delAtPos(&head,p);
			       }
                                break;

                        case 8:
                                printf("Total number of nodes are %d\n",countNode(head));
                                break;

                        case 9:
                                printLL(head);
                                break;

                        case 10:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }

                getchar();
                printf("Do you want to continue \n");
                scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');
}
