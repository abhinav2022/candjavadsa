
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int d;
	struct Demo* next;
}d;

d* createNode(){

	d* newNode= (d*)malloc(sizeof(d));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;

	return newNode;
}

void addNode(d** head){

	d* newNode = createNode();

	if(*head==NULL){
	
		 *head = newNode;
	}else{
	
		d* temp= *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int countNode(d*h){

	int p=0;

	while(h!=NULL){
	
		p++;
		h=h->next;
	}
	return p;
}

void addFirst(d**head){

	d* temp = createNode();

	if(*head==NULL){
	
		*head= temp;
	}else{
		
		temp->next=*head;
		*head=temp;
	}
}

void deleteFirst(d**head){

	if(*head==NULL){
		printf("No Node present\n");
		return;
}
	d*temp=*head;

	*head=(*head)->next;

	free(temp);
}

void deleteLast(d**head){

	d*temp=*head;

	if(*head==NULL){
		printf("No Node present\n");
		return;
	}
	else if(temp->next==NULL){
	
		free(*head);
		*head=NULL;
	}
	else{	
		while(temp->next->next!=NULL){
	
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

void addLast(d**head){

	addNode(head);
}
void addpos(d**head,int pos){

	d*temp= createNode();
	d*temp2=*head;
	if(pos<=0 || pos>countNode(temp2)+1){
	
		printf("Wrong position %d total nodes are %d\n",pos,countNode(temp));
		return;
	}

	while(pos-2>0){
	
		temp2=temp2->next;
		pos--;
	}

	if(pos==1){
	
		temp->next=*head;
		*head=temp;
	}else{
	
		temp->next=temp2->next;
		temp2->next=temp;
	}
}
void printll(d*temp){

	if(temp==NULL){
	
		printf("No nodes are present\n");
	}
	else{
	
		while(temp!=NULL){
			printf("|%d| ",temp->d);
			temp=temp->next;
		}
		printf("\n");
	}
}

void main(){

	d* head= NULL;

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addAtposition\n");
		printf("4.addLast\n");
		printf("5.deletefirst\n");
		printf("6.deletelast\n");
		printf("7.countNodes\n");
		printf("8.printll\n");
		printf("9.Exit\nYour choice :- ");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:				
				addNode(&head);
				break;

			case 2:
				addFirst(&head);
				break;

			case 3:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);

					addpos(&head,pos);
				}
				break;

			case 4:
				addLast(&head);
				break;

			case 5:
				deleteFirst(&head);
				break;

			case 6:
				deleteLast(&head);
				break;

			case 7:
				printf("Total number of nodes are %d\n",countNode(head));
				break;

			case 8:
				printll(head);
				break;

			case 9:
				printf("Quit\n");
				exit(0);
				break;
				
			default:
				printf("Wrong choice entered\n");
				break;

		}

		getchar();
		printf("Do you want to continue \n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
