
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int d;
	struct Demo* next;
}d;

d* createNode(){

	d* newNode= (d*)malloc(sizeof(d));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;

	return newNode;
}

void addNode(d** head){

	d* newNode = createNode();

	if(*head==NULL){
	
		 *head = newNode;
	}else{
	
		d* temp= *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int countNode(d*h){

	int p=0;

	while(h!=NULL){
	
		p++;
		h=h->next;
	}
	return p;
}

void addFirst(d**head){

	d* temp = createNode();

	temp->next=*head;
	*head=temp;
}
void deleteLast(d**head){

	d*temp=*head;

	if(*head==NULL){
		printf("No Node present\n");
		return;
	}
	else if(temp->next==NULL){
	
		free(*head);
		*head=NULL;
	}
	else{	
		while(temp->next->next!=NULL){
	
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
void deleteFirst(d**head){

	if(*head==NULL){
		printf("No Node present\n");
		return;
	}
	
	d*temp=*head;

	*head=(*head)->next;

	free(temp);
}

void deleteAtPos(d**head,int pos){

	if(*head==NULL){
	
		printf("No node present\n");
		
	}else if(pos<=0 || pos>countNode(*head)){
	
		printf("Invalid node %d , Total nodes are %d\n",pos,countNode(*head));
		
	}else if(pos==1){
	
		deleteFirst(head);
		
	}else if(pos==countNode(*head)){
	
		deleteLast(head);

	}else{
		d* temp=*head;

		while(pos-2){
	
			temp=temp->next;
			pos--;
		}
		d* temp2 = temp->next;
		temp->next=temp->next->next;
		free(temp2);
	}
}

void posNode(d*temp,int num){

	if(temp==NULL){
	
		printf("No node present\n");

	}else{
					//2nd last position
		d *temp2=temp;
		int c=0,p=0,p1=0,p2=0;
		while(temp2!=NULL){
		
			p++;
			if(num==temp2->d){
			
				c++;
				p1=p2;
				p2=p;
			}
			temp2=temp2->next;

		}
		if(c==1){
		
			printf("Only one node present");
		}
		else if(c>1){
		
			printf("Position is %d\n",p1);
		}
	}
}

void addLast(d*head){

	d*temp=createNode();

	while(head->next!=NULL){
	
		head=head->next;
	}
	head->next=temp;
}

void addpos(d**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("Wrong position %d total nodes are %d\n",pos,countNode(*head));
		return;
	}
	else if(pos==1){
	
		addFirst(head);
	}else{
	
		d*temp= createNode();
		d*temp2=*head;
	
		while(pos-2>0){
	
			temp2=temp2->next;
			pos--;
		}
		temp->next=temp2->next;
		temp2->next=temp;
	}
}

void printll(d*temp){

	if(temp==NULL){
	
		printf("No nodes are present\n");
	}
	else{
	
		while(temp!=NULL){
			printf("|%d| ",temp->d);
			temp=temp->next;
		}
		printf("\n");
	}
}

void main(){

	d* head= NULL;

	char choice;
	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addAtposition\n");
		printf("4.addLast\n");
		printf("5.deletefirst\n");
		printf("6.deletelast\n");
		printf("7.deleteAtPos\n");
		printf("8.countNodes\n");
		printf("9.posNodes\n");
		printf("10.printll\n");
		printf("11.Exit\nYour choice :- ");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:				
				addNode(&head);
				break;

			case 2:
				addFirst(&head);
				break;

			case 3:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);

					addpos(&head,pos);
				}
				break;

			case 4:
				addLast(head);
				break;

			case 5:
				deleteFirst(&head);
				break;

			case 6:
				deleteLast(&head);
				break;
			case 7:
				{
					int pos;
					printf("Enter pos\n");
					scanf("%d",&pos);
					deleteAtPos(&head,pos);
						
				}
				break;

			case 8:
				printf("Total number of nodes are %d\n",countNode(head));
				break;
			case 9:
				{
					int num;
					printf("Enter number\n");
					scanf("%d",&num);
					posNode(head,num);
				}
				break;

			case 10:
				printll(head);
				break;

			case 11:
				printf("Quit\n");
				exit(0);
				break;
				
			default:
				printf("Wrong choice entered\n");
				break;

		}

		getchar();
		printf("Do you want to continue \n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
