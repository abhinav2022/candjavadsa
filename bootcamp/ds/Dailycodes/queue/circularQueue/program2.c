
#include<stdio.h>
#include<stdlib.h>

int flag=0,size=0;

typedef struct Node{

	int d;
	struct Node*next;
}n;

n*front=NULL,*rear=NULL;

int countNode(){

	n*temp=front;
	int c=0;
	if(front==NULL)
		return c;
	else{
	
		while(temp!=rear){
		
			c++;
			temp=temp->next;
		}
			return c+1;
	}
}

n*createNode(){

	n*newNode= (n*)malloc(sizeof(n));

	if(newNode==NULL){
	
		printf("Memory full\n");
		exit(-1);
	}
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->next=NULL;
	return newNode;
}

int Enqueue(){

	int c  = countNode();

	if(c==size){
	
		return -1;
	}else{
	
		n*newNode=createNode();
	
		if(front==NULL){
		
			front=newNode;
			rear=newNode;
		}
		rear->next=newNode;
		rear=newNode;
		rear->next=front;
	}
}

int Dequeue(){

	if(front==NULL){
	
		flag=0;
		return -1;
	}else{
	
		flag=1;
		int c=front->d;
		if(front==rear){
		
			free(front);
			front=NULL;
			rear=NULL;
		}else{

			n* temp=front;
			front=front->next;
			free(temp);
		}
		return c;
	}
}

int Front(){

	if(front==NULL){
	
		flag=0;
		return -1;
	}else{
		flag=1;	
		return front->d;
	}
}

void printQueue(){

	if(front==NULL){
	
		printf("Queue Empty\n");
	}
	else{
	
		n*temp=front;

		while(temp!=rear){
		
			printf("%d->",temp->d);
			temp=temp->next;
		}
			printf("%d\n",temp->d);
	}
}

void main(){

	int ch;
	char choice;

	printf("Enter size\n");
	scanf("%d",&size);
	do{
	
		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.front\n");
		printf("4.print\n");
		printf("Your choice\n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				{
					int e=Enqueue();

					if(e==-1){
					
						printf("Queue Overflow\n");
					}
				}
				break;

			case 2:
				{
					int val = Dequeue();
					if(flag==0)
						printf("Queue Underflow\n");
					else
						printf("%d Dequeued\n",val);
				
				}
				break;

			case 3:
				{
					int val =Front();
					if(flag==0)
						printf("Queue Empty\n");
					else
						printf("%d \n",val);
				}
				break;

			case 4:
				printQueue();
				break;

			case 5:
				{
				
					int d = countNode();
					printf("count is %d\n",d);
				}
				break;

			default:
				printf("Wrong choice\n");
				break;
		}
		flag=0;
		printf("Continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
