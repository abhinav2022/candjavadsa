
#include<stdio.h>

int front=-1,rear=-1;
int flag=0,size=0;

int Enqueue(int*arr){

	if((rear==size-1 && front==0)||(rear==front-1)){
	
		return -1;
	}else{
	
		if(front==-1){
		
			front++;
		}else{
		
			if(rear ==size-1 && front!=0){
			
				rear=-1;
			}
		}
		
		rear++;
		printf("Enter number\n");
		scanf("%d",arr+rear);
	}
}

int Dequeue(int*arr){

	if(front==-1){
	
		flag=0;
		return -1;
	}else{
	
		flag=1;

		int val= arr[front];
		if(rear==front){
		
			front=-1;
			rear=-1;
		}else{
		
			if(front==size-1){
			
				front=-1;
			}
			front++;
		}
		return val;
	}
}

int Front(int*arr){

	if(front==-1){
	
		flag=0;
		return -1;
	}else{
		flag=1;	
		return *(arr+front);
	}
}

void printQueue(int*arr){

	if(front==-1){
	
		printf("Queue Empty\n");
	}
	else{
	
		int x =front;

		for(int i=0;i<size;i++){
		
			//x++;
			if(x==size)
				x=0;

			printf("%d->",*(arr+x));
			if(x==rear)
				break;
			x++;
		}
	}
}

void main(){

	int ch;
	char choice;

	printf("Enter size\n");
	scanf("%d",&size);
	int arr[size];
	do{
	
		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.front\n");
		printf("4.print\n");
		printf("Your choice\n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				{
					int e=Enqueue(arr);

					if(e==-1){
					
						printf("Queue Overflow\n");
					}
				}
				break;

			case 2:
				{
					int val = Dequeue(arr);


					if(flag==0)
						printf("Queue Underflow\n");

					else
						printf("%d Dequeued\n",val);
				
				}
				break;

			case 3:
				{
				
					int val =Front(arr);

					if(flag==0)
						printf("Queue Empty\n");

					else
						printf("%d \n",val);
				}
				break;

			case 4:
				printQueue(arr);
				break;

			 default:
                                printf("Wrong choice\n");
                                break;
		}
		flag=0;
		printf("Continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
