
#include<stdio.h>
#include<stdlib.h>

int flag=0;

typedef struct Node{

	int d;
	struct Node*next;
}n;

n*front =NULL,*rear=NULL;

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));

	if(newNode==NULL){
	
		printf("Memory full\n");
		exit(-1);
	} else {
	
		printf("Enter number\n");
		scanf("%d",&newNode->d);
		newNode->next=NULL;
	}
	return newNode;
}

int Enqueue(){

	n*newNode=createNode();
	if(front==NULL){
	
		front=newNode;
		rear=newNode;
	}else{

		rear->next=newNode;
		rear=newNode;
	}
}

int Dequeue(){

	if(front==NULL){
	
		flag=0;
		return -1;
	}else{
	
		flag=1;
		int c=0;
		if(front==rear){
		
			c=front->d;
			free(front);
			front=NULL;
			rear = NULL;
		}else {
		
			c=front->d;
			n*temp=front;
			front=front->next;
			free(temp);
		}
		return c;
	}
} 

int Front(){

	if(front==NULL)
		flag=0;

	else{
	
		flag=1;
		return front->d;
	}

}

int printQueue(){

	if(front==NULL){
		return -1;
	}else{
		
		n*temp=front;
		
		while(temp!=NULL){
			printf("%d->",temp->d);
			temp=temp->next;
		}
	}
	
	
}

void main(){

	char choice;

	do{
	
		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.Front\n");
		printf("4.PrintQueue\n");
		printf("Your Choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				Enqueue();
				break;

			case 2:{
					int ch=Dequeue();

					if(flag==0){
					
						printf("Queue empty\n");
					}else{
					
						printf("Dequeued %d\n",ch);
					}
			       }
			       break;

			case 3:
			       {
			       
				       int ch=Front();
				       if(flag==0)
					       printf("Empty queue\n");
				       else
					       printf("Front %d \n",ch);
			       }
			       break;

			case 4:
			       {
				       int ch=printQueue();
				       if(ch==-1){
				       
					       printf("Queue empty\n");
				       }
			       }
		}
			       getchar();
			       printf("continue\n");
			       scanf("%c",&choice);
			       flag=0;
	}while(choice=='y'||choice=='Y');
}
