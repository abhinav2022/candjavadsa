
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	int prior;
	struct Node*next;
}n;

n*head=NULL;

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));
	if(newNode==NULL){

                printf("memory full\n");
                exit(-1);
        }
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	printf("Enter priority\n");
	scanf("%d",&newNode->prior);
	newNode->next=NULL;

	return newNode;
}

int countNode(){

	n*temp=head;
	int c=0;
	while(temp!=NULL){
	
		temp=temp->next;
		c++;
	}
	return c;
}

int addFirst(n*temp){

	temp->next=head;
	head=temp;
}

int addAtPos(int pos,n*temp5){

	int c= countNode();
	if(pos<=0||pos>c+1){
	
		return -1;
	}else{
	
	       n*temp2=head;
               while(pos-2>0){

                        temp2=temp2->next;
                        pos--;
                }
                temp5->next=temp2->next;
                temp2->next=temp5;
        
	}
}
int flag=0;
int deleteLast(){

        if(head==NULL){
                flag=0;
                return -1;

        }else{
                flag=1;

                if(head->next==NULL){

                        free(head);
                        head=NULL;
                }else{

                        n*temp=head;
			while(temp->next->next!=NULL)
                        	temp=temp->next;
                        free(temp->next);
			temp->next=NULL;
                }
        }
}
int printQueue(){

	n*temp=head;
	if(temp==NULL){
	
		return -1;
	}else{

		while(temp->next!=NULL){
			
			printf("%d->",temp->d);
			temp=temp->next;
		}
		printf("%d\n",temp->d);
	}
}

int Sort(){

	n*newNode=createNode();

	if(head==NULL)
		head=newNode;

	else{
	
		n*temp=head;

		if(newNode->prior < temp->prior){
		
			addFirst(newNode);
		} else {
		
			int c=1;
			while(temp!=NULL){
			
				if(newNode->prior < temp->prior){
				
					break;
				}
					c++;
				temp=temp->next;
			}

			addAtPos(c,newNode);
		}
	}
}

void main(){

	char choice;

	do{

                printf("1.Sort\n");
                printf("2.printll\n");
                printf("3.delete\n");
		printf("4.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                Sort();
                                break;

			case 2:{
                                        int t=printQueue();
                                        if(t==-1)
                                                printf("No nodes present\n");
                               }
                                break;


			case 3:{
                                       int t=deleteLast();
                                       if(flag==0)
                                               printf("No node\n");
                               }
                                break;
                        case 4:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }

                getchar();
                printf("Do you want to continue \n");
		scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');

}
