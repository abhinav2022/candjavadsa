
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int d;
	int prior;
	struct Node*next;
}n;

n*front=NULL,*rear=NULL;

n*createNode(){

	n*newNode=(n*)malloc(sizeof(n));
	if(newNode==NULL){
	
		printf("memory full\n");
		exit(-1);
	}
	printf("Enter number\n");
	scanf("%d",&newNode->d);
	do{
	printf("Enter priority\n");
	scanf("%d",&newNode->prior);
	}while(newNode->prior<0);

	newNode->next=NULL;

	return newNode;
}

int countNode(){

	n*temp=front;
	int c=0;
	while(temp!=NULL){
	
		temp=temp->next;
		c++;
	}
	return c;
}

int flag=0;
int deleteFirst(){

        if(front==NULL){
                flag=0;
                return -1;

        }else{
                flag=1;
		int t=front->d;
                if(front->next==NULL){
		
                        free(front);
                        front=NULL;
			rear=NULL;
                }else{
                        n*temp=front;
                        front=front->next;
                        free(temp);
                }
		return t;
        }
}

int addFirst(n*temp){

	temp->next=front;
	front=temp;
}

int addAtPos(int pos,n*temp5){

	int c= countNode();
	if(pos<=0||pos>c+1){
	
		return -1;
	}else{
	
	       n*temp2=front;
        	if(pos==c+1)
			rear=temp5;
               while(pos-2>0){

                        temp2=temp2->next;
                        pos--;
                }
                temp5->next=temp2->next;
                temp2->next=temp5;
	}
}

int printQueue(){

	n*temp=front;
	if(temp==NULL){
	
		return -1;
	}else{

		while(temp!=rear){
			
			printf("%d->",temp->d);
			temp=temp->next;
		}
		printf("%d\n",temp->d);
	}
}

int Sort(){

	n*newNode=createNode();

	if(front==NULL){
		front=newNode;
		rear=newNode;
	}

	else{
	
		n*temp=front;

		if(newNode->prior < temp->prior){
		
			addFirst(newNode);
		} else {
		
			int c=1;
			while(temp!=NULL){
			
				if(newNode->prior < temp->prior){
				
					break;
				}
					c++;
				temp=temp->next;
			}

			addAtPos(c,newNode);
		}
	}
}

void main(){

	char choice;

	do{
                printf("1.Sort\n");
                printf("2.printll\n");
                printf("3.delete\n");
		printf("4.Exit\nYour choice :- ");
                int ch;
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                Sort();
                                break;

			case 2:{
                                        int t=printQueue();
                                        if(t==-1)
                                                printf("No nodes present\n");
                               }
                                break;

			case 3:{

                                       int t=deleteFirst();

                                       if(flag==0)
                                               printf("No node\n");
				       else
					       printf("%d Dequeued\n",t);
                               }
                                break;

                        case 4:
                                printf("Quit\n");
                                exit(0);
                                break;

                        default:
                                printf("Wrong choice entered\n");
                                break;

                }

                getchar();
                printf("Do you want to continue \n");
		scanf("%c",&choice);

        }while(choice=='y'||choice=='Y');

}
