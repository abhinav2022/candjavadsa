
#include<stdio.h>

void ms(int*arr,int start,int end){

	if(start<end){
	
		int piv = part(arr,start,end);

		ms(arr,start,piv-1);
		ms(arr,piv+1,end);
	}
}

void swap(int*t1,int*t2){

	int temp = *t1;
	*t1=*t2;
	*t2=temp;
}

int part(int*arr,int start,int end){

	int itr=start-1,piv = arr[end];

	for(int i=start;i<end;i++){
	
		if(arr[i]<piv){
		
			itr++;

			swap(arr+i,arr+itr);
		}
	}

	swap(arr+itr+1,arr+end);

	return itr+1;
}
