
#include<stdio.h>

int ss(int*,int,int);
//  approach
void ms(int*arr,int start,int end){

	if(start<end){
		int pivot = ss(arr,start,end);
		printf("pivot %d = %d\n",pivot,arr[pivot]);
		ms(arr,start,pivot-1);
		ms(arr,pivot+1,end);
	}
}
int h = 1;

void swap(int*t1,int*t2){

	int temp=*t1;
	*t1=*t2;
	*t2=temp;
}

int ss(int*arr,int start,int end){

	int piv = arr[end],itr = start-1;
	
	printf("------bef----------\n");
	for(int i=0;i<5;i++){
	
		printf("%d ",arr[i]);
	}
	for(int i=start;i<end;i++){
	
		if(arr[i]<piv){
		
			itr++;

			swap(arr+i,arr+itr);
		}
	}
	swap(arr+itr+1,arr+end);
	printf("\n------aft----------\n");
	for(int i=0;i<5;i++){
	
		printf("%d ",arr[i]);
	}

	return itr+1;
}
void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter numbers in array\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	ms(arr,0,n-1);
	printf("-----------------array----------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
