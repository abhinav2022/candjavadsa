
#include<stdio.h>

void is(int*arr,int n){

	for(int i=1;i<n;i++){
	
		int j = i-1,val=*(arr+i);

		for(;(j>=0 && *(arr+j)>val);j--){
		
			*(arr+j+1)=*(arr+j);
		}

		*(arr+j+1)=val;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter numbers in array\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	is(arr,n);
	printf("-----------------array----------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
