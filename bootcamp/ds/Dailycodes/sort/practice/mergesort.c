
#include<stdio.h>

void ss(int*,int,int,int);
void ms(int*arr,int start,int end){

	if(start<end){
	
		int mid = (start+end)/2;
		ms(arr,start,mid);
		ms(arr,mid+1,end);
		ss(arr,start,mid,end);
	}
}

void ss(int*arr,int start,int mid,int end){

	int ele1 = mid- start +1;
	int ele2 = end - mid;

	int arr1[ele1],arr2[ele2];

	for(int i=0;i<ele1;i++){
	
		arr1[i]=arr[start+i];
	}

	for(int i=0;i<ele2;i++){
	
		arr2[i]=arr[mid+1+i];
	}

	int itr1=0,itr2=0,itr3=start;

	while(itr1<ele1 && itr2<ele2){
	
		if(*(arr1+itr1)<*(arr2+itr2)){
					
			*(arr+itr3)=*(arr1+itr1);
			itr1++;
		}else{
		
			*(arr+itr3)=*(arr2+itr2);
			itr2++;
		}
		itr3++;
	}
	while(itr1<ele1){
	
		*(arr+itr3)=*(arr1+itr1);
                        itr1++;
			itr3++;
	}
	while(itr2<ele2){
	
		*(arr+itr3)=*(arr2+itr2);
		itr2++,itr3++;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter numbers in array\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	ms(arr,0,n-1);
	printf("-----------------array----------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
