
#include<stdio.h>

void countsort(int*arr,int n,int pos){

	int cont[10];

	for(int i=0;i<10;i++){
	
		cont[i]=0;
	}
	
	for(int i=0;i<n;i++){
	
		cont[(arr[i]/pos)%10]++;
	}
	for(int i=1;i<10;i++){
	
		cont[i]+=cont[i-1];
	}
	int out[n];

	for(int i=n-1;i>=0;i--){
	
		out[cont[(arr[i]/pos)%10]-1]=arr[i];

		cont[(*(arr+i)/pos)%10]--;
	}
	printf("----------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(out+i));
		arr[i]=out[i];
	}
	printf("\n");
}

void radix(int*arr,int n){

	int max = arr[0],min=arr[0];

	for(int i=1;i<n;i++){
	
		if(arr[i]<min)
			min=arr[i];
	}

	if(min<0){
	
		for(int i=0;i<n;i++){
		
			arr[i]+=(-(min)+1);
		}
	}
	for(int i=0;i<n;i++){
	
		if(arr[i]>max)
			max=arr[i];
	}
	
	for(int pos=1;max/pos>0;pos=pos*10){
	
		countsort(arr,n,pos);
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	radix(arr,n);
	printf("----------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",arr[i]);
	}

}
