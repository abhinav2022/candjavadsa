
#include<stdio.h>

void swap(int *arr1,int*arr2){

	int t=*arr1;
	*arr1=*arr2;
	*arr2=t;
}

int part(int *arr,int start,int end){

	int pivot=*(arr+end),itr=start-1;

	for(int i=start;i<end;i++){
	
		if(*(arr+i)<pivot){
		
			itr++;

			swap(arr+i,arr+itr);
		}
	}
	swap(arr+itr+1,arr+end);
	return itr+1;
}

void quick(int*arr,int start,int end){

	if(start<end){
	
		int pivot = part(arr,start,end);

		quick(arr,start,pivot-1);
		quick(arr,pivot+1,end);
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	quick(arr,0,n-1);
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}
}
