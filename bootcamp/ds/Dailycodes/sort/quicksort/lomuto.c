
#include<stdio.h>

void swap(int*t1,int*t2){

	int temp=*t1;
	*t1 = *t2;
	*t2=temp;
}

int part(int*arr,int start,int end){

	int  piv = arr[end],itr=start-1;

	for(int i=start;i<end;i++){
	
		if(arr[i]<piv){
		
			itr++;

			swap(arr+i,arr+itr);
		}
	}
	swap(arr+end,arr+itr+1);
	
	return itr+1;
}

void ss(int*arr,int start,int end){
	
	if(start<end){
	
		int piv = part(arr,start,end);

		ss(arr,start,piv-1);
		ss(arr,piv+1,end);
	}
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);
	int arr[n];
	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	ss(arr,0,n-1);

	printf("-------------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
