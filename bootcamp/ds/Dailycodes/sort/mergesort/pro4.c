
#include<stdio.h>

void sort(int *arr,int n){

	for(int i=0;i<n;i++){
	
		for(int j=0;j<n-1;j++){
		
			if(*(arr+j)>*(arr+j+1)){
			
				int t = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=t;
			}
		}
	}
}

void ms(int *arr1,int*arr2,int*arr3,int n,int m){

	int y=0,k=0,flag=0;
	for(int i=0;i<n+m;i++){
	
		if(flag==0 && *(arr1+y)<*(arr2+k) ){
		
			*(arr3+i)=*(arr1+y);
			y++;
			if(y==n)
				flag=1;
		}else{
		
			*(arr3+i)=*(arr2+k);
			k++;
		}
	}
}

void main(){

	int n,m;
	printf("Enter sizes\n");
	scanf("%d",&n);
	scanf("%d",&m);

	int arr1[n];
	int arr2[m];

	printf("Enter elements in array1\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr1+i);
	}
	printf("Enter elements in array2\n");
	for(int i=0;i<m;i++){
	
		scanf("%d",arr2+i);
	}
	int arr3[n+m];

	sort(arr1,n);
	sort(arr2,m);
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr1+i));
	}
	for(int i=0;i<m;i++){
	
		printf("%d ",*(arr2+i));
	}
	ms(arr1,arr2,arr3,n,m);
	printf("------------------\n");
	for(int i=0;i<n+m;i++){
	
		printf("%d ",*(arr3+i));
	}
	printf("\n");
	

}
