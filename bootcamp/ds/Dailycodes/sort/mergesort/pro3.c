
#include<stdio.h>

int recfun(int*arr,int start,int end){

	if(start<end){
	
		int mid = (start+end)/2;
		return mid+recfun(arr,start,mid)+recfun(arr,mid+1,end);
	}else 
		return 0;
}

void main(){

	int arr[]={7,2,4,1,9,6,5,3};

	int s = 8;
	int l =recfun(arr,0,7);

	printf("%d\n",l);
}
