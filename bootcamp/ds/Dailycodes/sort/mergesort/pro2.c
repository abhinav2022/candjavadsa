
#include<stdio.h>

void recfun(int*arr,int start,int end){

	if(start<end){
	
		int mid = (start+end)/2;
		recfun(arr,start,mid);
		recfun(arr,mid+1,end);
	}
}

void main(){

	int arr[]={7,2,4,1,9,6,5,3};

	int s = 8;
	recfun(arr,0,7);
}
