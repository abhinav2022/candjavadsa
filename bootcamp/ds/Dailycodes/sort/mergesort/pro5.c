
#include<stdio.h>

void ms(int *arr,int start,int mid,int end){

	int ele1=mid - start+1;
	int ele2 = end-mid;

	int arr1[ele1],arr2[ele2];

	for(int i=0;i<ele1;i++){
	
		*(arr1+i)=*(arr+start+i);
	}
	for(int i=0;i<ele2;i++){
	
		*(arr2+i)=*(arr+mid+1+i);
	}

	int itr3=start,itr2=0,itr1=0;

	while(itr1<ele1 && itr2<ele2){
	
		if(*(arr1+itr1)>*(arr2+itr2)){
		
			*(arr+itr3)=*(arr2+itr2);
			itr2++;
		}else{
		
			*(arr+itr3)=*(arr1+itr1);
			itr1++;
		}
		itr3++;
	}	

	while(itr1<ele1){
	
		*(arr+itr3)=*(arr1+itr1);
		itr1++;
		itr3++;
	}
	while(itr2<ele2){
	
		*(arr+itr3)=*(arr2+itr2);
		itr3++;
		itr2++;
	}
		
}
void sort(int *arr,int start,int end){

	int mid;
	if(start<end){
	
		mid=(start+end)/2;
		
		sort(arr,start,mid);
		sort(arr,mid+1,end);
		ms(arr,start,mid,end);
	}
}

void main(){

	int n;
	printf("Enter sizes\n");
	scanf("%d",&n);

	int arr1[n];

	printf("Enter elements in array1\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr1+i);
	}
	sort(arr1,0,n-1);
	printf("------------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d ",*(arr1+i));
	}
	printf("\n");
	

}
