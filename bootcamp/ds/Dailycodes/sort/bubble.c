
#include<stdio.h>

void sort(int *arr,int s){

	int j=0;
	for(int i=0;i<s;){
	
		if(j<s-i-1){
		
			if(*(arr+j)>*(arr+j+1)){
			
				int temp = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=temp;
			}
			j++;
		}else{
		
			j=0;
			i++;
		}
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("\nSorted is \n");	
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}	
}
