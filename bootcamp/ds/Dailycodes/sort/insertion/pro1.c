
#include<stdio.h>

void sort(int *arr,int k){
	

	for(int i=1;i<k;i++){
	
		int v=arr[i];
		int j = i-1;
		for(;j>=0 && arr[j]>v;j--){
		
			arr[j+1]=arr[j];
		}
		arr[j+1]=v;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	printf("-------\n");
	printf("enter elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("-------\n");
	
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}

}
