
#include<stdio.h>

void sort(int *arr,int s){

	for(int i=0;i<s-1;i++){
	
		int minIndex = i;

		for(int j=i+1;j<s;j++){
		
			if(*(arr+minIndex)>*(arr+j))
				minIndex = j;
		}

		int temp = *(arr+i);
		*(arr+i)=*(arr+minIndex);
		*(arr+minIndex)=temp;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("\nSorted is \n");	
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}	
}
