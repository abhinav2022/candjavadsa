
#include<stdio.h>

int factor(int a){

	int b=0;

	for(int i=1;i<=a;i++){
	
		if(a%i==0){
		
			b++;
		}
	}
	return b;
}

void sort(int *arr,int s){

	for(int i=0;i<s;i++){
	
		for(int j=0;j<s-1;j++){
		
			if(factor(*(arr+j))>factor(*(arr+j+1))){
				int temp = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=temp;
			}
			else if(*(arr+j)>*(arr+j+1)){
				int temp = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=temp;
			}
	}
}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("\nSorted is \n");	
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}	
}
