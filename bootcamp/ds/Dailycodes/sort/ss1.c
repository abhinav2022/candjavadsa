
#include<stdio.h>

int factor(int a){

	int b=0;

	for(int i=1;i<=a;i++){
	
		if(a%i==0){
		
			b++;
		}
	}
	return b;
}

void sort(int *arr,int s){

	for(int i=0;i<s-1;i++){
	
		int minIndex = i;
		int j=i+1;
		for(;j<s;j++){
		
			if(factor(*(arr+minIndex))>factor(*(arr+j))){
				minIndex = j;
			}

			else if(factor(*(arr+minIndex))==factor(*(arr+j))){
			
				if(*(arr+minIndex)>*(arr+j))
					minIndex = j;
			}

		//	else if(*(arr+minIndex)>*(arr+j))
		//		minIndex = j;
		}

		int temp = *(arr+i);
		*(arr+i)=*(arr+minIndex);
		*(arr+minIndex)=temp;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("\nSorted is \n");	
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}	
}
