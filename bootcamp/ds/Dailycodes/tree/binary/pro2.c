
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
typedef struct node{

	int d;
	struct node *left;
	struct node *right;
}n;

typedef struct Stack{

	n* btNode;
	struct Stack *prev;
}st;

n* root = NULL;

st* top = NULL;

void push(n* r){

	st* newNode =(st*) malloc(sizeof(st));
	newNode->btNode = r;
	newNode->prev = top;
	top = newNode;
}

n* pop(){

	n*ret = top->btNode;
	st* temp = top;
	top=top->prev;
	free(temp);
	return ret;
}

bool isEmpty(){

	if(top==NULL)
		return true;
	else
		return false;
}

void itrOrd(n*p){

	if(root ==NULL)
		return;

	n*temp = p;

	while(!isEmpty() || temp!=NULL){
	
		if(temp!=NULL){
		
			push(temp);
			temp=temp->left;
		}else{
		
			n* u = pop();
			printf("%d ",u->d);
			temp=u->right;
		}
	}
}

n* createNode(){

	n* newNode = (n*)malloc(sizeof(n));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->left=NULL;
	newNode->right=NULL;
	return newNode;
}



void addNode(n*head,int l){

	if(root==NULL){
	printf("At level %d\n",l);
	n * newNode = createNode();
		root = newNode;
		addNode(newNode,l+1);
	}
	else{
		char p;
	getchar();
	printf("Do you want to continue in left level = %d\n",l);
	scanf("%c",&p);
	getchar();

	if(p=='y'){
		n * newNode = createNode();
		head->left=newNode;
		addNode(head->left,l+1);
	}

	printf("Do you want to continue in right level = %d\n",l);
	scanf("%c",&p);
	getchar();

	if(p=='y'){
		n * newNode = createNode();
		head->right=newNode;
		addNode(head->right,l+1);
	}
	}
}

void printPre(n*head){

	if(head!=NULL){
	printf("%d ",head->d);
	printPre(head->left);
	printPre(head->right);
	}
}
void printin(n*head){

	if(head!=NULL){
	printin(head->left);
	printf("%d ",head->d);
	printin(head->right);
	}
}
void printpost(n*head){

	if(head!=NULL){
	printpost(head->left);
	printpost(head->right);
	printf("%d ",head->d);
	}
}

void main(){

	addNode(root,0);
	printf("\n");
	printf("preorder ");
	n* p = root;
	printPre(p);
	printf("\n");
	printf("inorder ");
	printin(p);
	printf("\n");
	printf("postorder ");
	printpost(p);
	printf("\n");
	printf("itrorder ");
	itrOrd(p);
	printf("\n");
	
}
