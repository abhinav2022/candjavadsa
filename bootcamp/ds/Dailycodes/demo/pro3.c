
#include<stdio.h>
#include<stdbool.h>

int sqint(int a){

	int c=0;

	for(int i=1;i*i<=a;i++){
	
		c++;
	}
	return c;
}

bool isPrime(int s){

	int c=0,k=sqint(s);

	for(int i=2;i<=k;i++){
	
		if(s%i==0){
		
			c++;
			break;
		}
	}

	if(c==0)
		return false;

	else
		return true;

}

void main(){

	int s;

	printf("Enter number\n");
	scanf("%d",&s);

	if(isPrime(s)){
	
		printf("%d is not prime number\n",s);

	}else{
	
		printf("%d is prime number\n",s);
	}
}
