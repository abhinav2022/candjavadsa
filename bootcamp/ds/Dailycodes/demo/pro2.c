
#include<stdio.h>

int FUN(int *arr,int n){

	int even=0,odd=0;

	for(int i=0;i<n;i++){
	
		if(*(arr+i)%2==0 && *(arr+i)>even){
		
			even= *(arr+i);

		}else if(*(arr+i)%2!=0 && *(arr+i)<odd){
		
			odd = *(arr+i);
		}
	}

	return even - odd;
}

void main(){

	int size=0,sum=0;
	printf("Enter number\n");
	scanf("%d",&size);

	int arr[size];

	for(int i=0;i<size;i++){
	
		scanf("%d",arr+i);
	}

	printf("Even - odd = %d\n",FUN(arr,size));
}
