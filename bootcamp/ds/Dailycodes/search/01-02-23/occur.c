
#include<stdio.h>

int ifpresent(int *arr,int n,int k){

	for(int i=0;i<n;i++){
	
		if(*(arr+i)==k)
			return i;
	}

	return -1;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n],k;
	
	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}
	printf("Enter key\n");
	scanf("%d",&k);
	
	int index=ifpresent(arr,n,k);

	if(index==-1)
		printf("Number not found\n");

	else
		printf("Number found at %d\n",index);

}
