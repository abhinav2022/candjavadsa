
#include<stdio.h>

int binsearch(int *arr,int n,int k){

	int start = 0;
	int end = n-1;
	int mid;

	while(start<=end){
	
		mid = (start+end)/2;

		if( *(arr+mid)==k)
			return mid;

		if(*(arr+mid)<k)
			start = mid+1;
		else
			end = mid-1;
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	int k;
	printf("Enter key\n");
	scanf("%d",&k);

	int index = binsearch(arr,n,k);

	if(index ==-1)
		printf("Number not found\n");

	else
		printf("number found at index %d\n",index);
}
