
#include<stdio.h>
#include<stdbool.h>

bool fun(int *arr,int i,int l){

	if(i<l-1){
	
		if(*(arr+i)>*(arr+i+1))
			return false;
		else
			return fun(arr,++i,l);
	}
	else
	return true;

	/*if(i==l-1)
		return true;

	else
	return arr[i]<arr[i+1] && fun(arr,++i,l);*/
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	if(fun(arr,0,n))
		printf("sorted\n");
	else
		printf("unsorted\n");

}
