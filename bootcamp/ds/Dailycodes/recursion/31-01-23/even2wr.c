
#include<stdio.h>
#include<stdbool.h>

bool fun(int *arr,int l){

	int a =0;

	for(int i=0;i<l;i++){
	
		if(a!=2){
		
			if(*(arr+i)%2==0)
				a++;
		}else
			break;
	}

	if(a==2)
		return true;
	else
		return false;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	if(fun(arr,n)){
	
		printf("Even");
	}else
		printf("Not even\n");
}
