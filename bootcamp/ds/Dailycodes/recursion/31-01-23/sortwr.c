
#include<stdio.h>
#include<stdbool.h>

bool fun(int *arr,int l){

	for(int i=0;i<l-1;i++){
	
		if(*(arr+i)>*(arr+i+1))
			return false;
	}
	return true;
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);

	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	if(fun(arr,n))
		printf("sorted\n");
	else
		printf("unsorted\n");

}
