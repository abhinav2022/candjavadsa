
#include<stdio.h>

int fun(int n){

	int a=0;

	while(n>0){
	
		if(n%2==0){
		
			n/=2;
		}
		else
			n=n-1;

		a++;
	}
	return a;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	
	printf("Count is %d\n",fun(n));
}
