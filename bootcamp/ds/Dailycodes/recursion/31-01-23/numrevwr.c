
#include<stdio.h>

int rev(int n){

	int b = 0;

	while(n>0){
	
		b = b*10 + n%10;
		n/=10;
	}

	return b;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	printf("Reverse is %d\n",rev(n));
}
