
#include<stdio.h>

int flag=0;
int fun(int n){

	if(n>=0){

		if(n%10==0 && n>0){
	
			return 1+fun(n/10);

		}else if(n==0 && flag==0){
		
			return 1;
		}else if(n==0 && flag==1){
		
				return 0;

		}else{
			if(n/10==0)
				flag=1;

			return fun(n/10);
		}
	}
	else
		return 0;
}

void main(){

	printf("Enter number\n");
	int n;
	scanf("%d",&n);
	printf("Number of zeros are %d\n",fun(n));
}
