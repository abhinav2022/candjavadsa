
#include<stdio.h>

int fun(long n){

	int cnt = 0;

	while(n>0){
	
		if(n%10==0)
			cnt++;

		n/=10;
	}
	return cnt;
}

void main(){

	long n ; 

	printf("Enter number\n");
	scanf("%ld",&n);
	printf("no. of zeros are %d\n",fun(n));
}
