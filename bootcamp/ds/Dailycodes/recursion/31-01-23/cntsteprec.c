
#include<stdio.h>

int fun(int n){

	if(n>0){
	
		if(n%2==0)
			return 1 + fun(n/2);
		else
			return 1 + fun(n-1);
	}else 
		return 0;

}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	
	printf("Count is %d\n",fun(n));
}
