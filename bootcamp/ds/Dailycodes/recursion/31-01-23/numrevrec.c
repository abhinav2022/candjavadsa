
#include<stdio.h>

int rev(int n){

	static int b = 0;
	
	if(n==0)	
		return b;
	else{
	
		b = b*10 + n %10;
		return rev (n/10);
	}
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	printf("Reverse is %d\n",rev(n));
}
