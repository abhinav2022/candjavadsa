
#include<stdio.h>

int sumarray(int*arr,int s){

	if(s==0)
		return *(arr+s);

	return sumarray(arr,s-1) + *(arr + s);
}

void main(){

	int a;
	printf("Enter number\n");
	scanf("%d",&a);
	int arr[a];

	for(int i=0;i<a;i++){
	
		scanf("%d",arr+i);
	}
	int ret = sumarray(arr,a-1);

	printf("%d\n",ret);
}
