
#include<stdio.h>

int factorial(int n){

	static int fac=1;

	if(n==0)
		return fac;

	fac=fac*n;

	return factorial(--n);

}

void main(){

	int y;
	printf("Enter number\n");
	scanf("%d",&y);
	printf("%d\n",factorial(y));
}
