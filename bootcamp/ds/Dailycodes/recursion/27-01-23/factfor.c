
#include<stdio.h>

int factorial(int n){

	int fac=1;

	for(int i=n;i>1;i--){
	
		fac=fac*i;
	}
	return fac;
}

void main(){
	
	int y;
	printf("Enter number\n");
	scanf("%d",&y);
	printf("%d! = %d\n",y,factorial(y));
}
