
#include<stdio.h>

int sumArr(int *arr,int size){

	if(size>0){
	
		return *(arr+size-1)+sumArr(arr,size-1);
	}else
		return 0;
}

void main(){

	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",arr+i);
	}

	printf("\nSum = %d\n",sumArr(arr,size));
}
