
#include<stdio.h>
#include<stdlib.h>

int op=-1;
int size=0;

typedef struct CPU{

	struct CPU*prev;
	char name[20];
}c;

c* top =NULL;
c*createNode(){

	c*newNode=(c*)malloc(sizeof(c));
	printf("Enter name\n");
	char ch;
	int i=0;

	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	newNode->prev=NULL;
	return newNode;
}

void addNode(c**head){

	c* newNode = createNode();
	if(*head==NULL){
		*head=newNode;
	}
	newNode->prev=top;
	top=newNode;
}

int push(c**stack){

	if(op==size-1){
	
		printf("Stack Overflow\n");
		return -1;
	}else{	
		op++;
		addNode(stack);
		return 0;
	}
}

int pop(c **stack){

	if(op==-1){
		printf("Stack Underflow\n");
		return -1;
	}else{
	
		if(op!=-1){
			c*temp=top;
			printf("%s popped\n",temp->name);
			top=top->prev;
			free(temp);
		}
		op--;
		return 0;		
	}
}

int peek(){

	if(op==-1){
	
		printf("Stack Empty\n");
		return -1;

	}else{
		c*temp=top;
		printf("peeked value is %s\n",temp->name);
	}
}

void main(){

	char choice;
	printf("Enter stack size\n");
	scanf("%d",&size);
	c *head=NULL;

	do{
	
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("Enter choice :-\n");
		int d;
		scanf("%d",&d);
		getchar();

		switch(d){
		
			case 1:
				push(&head);
				break;

			case 2:
				pop(&head);
				break;

			case 3:
				peek();
				break;

			default:
				printf("Wrong choice\n");
				break;
		}
		printf("Do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');

}
