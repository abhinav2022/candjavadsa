
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

int size=0;

typedef struct CPU{

	int cache; 
	struct CPU*next;
}c;

c* top=NULL;
int flag=0;

c*createNode(){

	c*newNode=(c*)malloc(sizeof(c));
	printf("Enter cache memory\n");
	scanf("%d",&newNode->cache);
	newNode->next=NULL;
	
	return newNode;
}

bool isEmpty(c*temp){

	if(temp==NULL){
	
		return true;

	} else {
	
		return false;
	}
}

int count(c*temp){

	int c=0;

	while(temp!=NULL){
		c++;
		temp=temp->next;
	}
	return c;
}

bool isFull(c*temp){

	if(size==count(temp)){
	
		return true;
	}else{
	
		return false;
	}
}

void addNode(c**head){

	c* newNode = createNode();

	if(isEmpty(*head)){
	
		*head=newNode;

	} else {
	
		c*temp=*head;

		while (temp->next!=NULL) {
		
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int deleteLast(c**head){

	if(*head==NULL){
		flag=0;
		return -1;
	} else if ((*head)->next==NULL) {
		int ret = (*head)->cache;
		free(*head);
		*head=NULL;
		return ret;
	}else{
	
		top=*head;
		while(top->next->next!=NULL){
			top=top->next;
		}
		int r=top->next->cache;
		free(top->next);
		top->next=NULL;
		return r;
	}
}

int push(c**stack){

	if(isFull(*stack)){
		flag=0;
		return -1;
	}else{	
		flag=1;
		addNode(stack);
		return 0;
	}
}

int pop(c **stack){

	if(isEmpty(*stack)){

		flag=0;
		return -1;
	}else{
		flag=1;	
		return deleteLast(stack);
	}
}

int peek(c **stack){

	if(isEmpty(*stack)){
	
		flag=0;
		return -1;

	}else{

		flag=1;
		c*temp=*stack;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		return temp->cache;
	}
}

void main(){

	char choice;
	printf("Enter stack size\n");
	scanf("%d",&size);
	c *head=NULL;

	do{
	
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("Enter choice :-\n");
		int d;
		scanf("%d",&d);

		switch(d){
		
			case 1:{
				       push(&head);

				       if(flag==0){
				       	       
					       printf("Stack Overflow\n");
				       }
			       }
				break;

			case 2:{
				       int ret=pop(&head);

				       if(flag==0)
					       printf("Stack Underflow\n");
				       else
					       printf("%d popped\n",ret);
			       }
				break;

			case 3:{
				       int ret = peek(&head);

				       if(flag==0)
					       printf("Stack Empty\n");
				       else
					       printf("%d peeked\n",ret);
			       }
				break;

			default:
				printf("Wrong choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
