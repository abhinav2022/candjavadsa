
#include<stdio.h>

int top=-1;
int size=0;
int flag=0;
int push(int *stack){

	if(top==size-1){
	
		printf("Stack Overflow\n");
		return -1;
	}else{
	
		flag=0;
		top++;
		printf("Enter value in array\n");
		scanf("%d",stack+top);
		return 0;
	}
}

int pop(int *stack){

	if(top==-1){
		flag=1;
		printf("Stack Underflow\n");
		return -1;
	}else{
	
		flag=0;
		top--;
		return *(stack+top+1);		
	}
}

int peek(int *stack){

	if(top==-1){
	
		flag=1;
		printf("Stack Empty\n");
		return -1;
	}else{
	
		flag=0;
		return *(stack+top);
	}
}

void main(){

	char choice;
	printf("Enter stack size\n");
	scanf("%d",&size);
	int arr[size];

	do{
	
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("Enter choice :-\n");
		int d;
		scanf("%d",&d);

		switch(d){
		
			case 1:
				push(arr);
				break;

			case 2:
				{
					int p=pop(arr);
					if(flag==0){
					
						printf("%d popped\n",p);
					}
				}
				break;

			case 3:
				{
				
					int p = peek(arr);
					if(flag==0){
					
						printf("peeked value is %d\n",p);
					}
				}
				break;

			default:
				printf("Wrong choice\n");
				break;


		}

		getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
