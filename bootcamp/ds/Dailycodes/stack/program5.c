
#include<stdio.h>
#include<stdlib.h>

int top=-1;
int size=0;

typedef struct CPU{

	struct CPU*prev;
	char name[20];
	struct CPU*next;
}c;

c*createNode(){

	c*newNode=(c*)malloc(sizeof(c));

	printf("Enter name\n");
	char ch;
	int i=0;

	while((ch=getchar())!='\n'){
	
		newNode->name[i]=ch;
		i++;
	}
	newNode->next=NULL;
	newNode->prev=NULL;

	return newNode;
}

void addNode(c**head){

	c* newNode = createNode();
	if(*head==NULL){
	
		*head=newNode;

	}else{
	
		c*temp=*head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		newNode->prev=temp;
		temp->next=newNode;
	}
}

void deleteLast(c**head){

	if(*head==NULL){
	
		printf("Stack empty\n");
	}else if((*head)->next==NULL){
		
		free(*head);
		*head=NULL;
	}else{
	
		c*temp=*head;

		while(temp->next->next!=NULL){
		
			temp=temp->next;
		}

		free(temp->next);
		temp->next=NULL;
	}
}
int push(c**stack){

	if(top==size-1){
	
		printf("Stack Overflow\n");
		return -1;
	}else{	
		top++;
		addNode(stack);
		return 0;
	}
}

int pop(c **stack){

	if(top==-1){

		printf("Stack Underflow\n");
		return -1;
	}else{
	
		if(top!=-1){

			c*temp=*stack;
			while(temp->next!=NULL){
			
				temp=temp->next;
			}
			printf("%s popped\n",temp->name);
			deleteLast(stack);
		}
		top--;
		return 0;		
	}
}

int peek(c **stack){

	if(top==-1){
	
		printf("Stack Empty\n");
		return -1;

	}else{
	
			c*temp=*stack;
			while(temp->next!=NULL){
			
				temp=temp->next;
			}
		printf("peeked value is %s\n",temp->name);
	}
}

void main(){

	char choice;
	printf("Enter stack size\n");
	scanf("%d",&size);
	c *head=NULL;

	do{
	
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("Enter choice :-\n");
		int d;
		scanf("%d",&d);
		getchar();

		switch(d){
		
			case 1:
				push(&head);
				break;

			case 2:
				pop(&head);
				break;

			case 3:
				peek(&head);
				break;

			default:
				printf("Wrong choice\n");
				break;
		}
		printf("Do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');

}
