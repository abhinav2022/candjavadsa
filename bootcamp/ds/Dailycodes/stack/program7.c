
#include<stdio.h>

int size=0;
int top1=0,top2=0;
int flag=0,flag1=0;
int push(int*arr,int ch){

	switch(ch){

		case 1:
			{
				if(top2-top1==1){
	
					return flag=-1;
				}else{
	
					flag=1;
					top1++;
					printf("Enter number\n");
					scanf("%d",arr+top1);
					return 0;
					}
			}
			break;

		case 2:
			{
				if(top2-top1==1){
					
					return flag1=-1;
				}else{
	
					flag1=1;
					top2--;
					printf("Enter number\n");
					scanf("%d",arr+top2);
					return 0;
				}
			}
			break;
		default:
		       	printf("Wrong choice\n");
			break;
	}
}

int pop(int *arr,int d){

	switch(d){
	
		case 1:{
			       if(top1==-1){
			       
				       flag=0;
				       return -1;
			       }else{
			       
				       int ret= *(arr+top1);
				       top1--;
				       flag=1;
				       return ret;
			       }
		       }
		       break;
		case 2:
		       {	
			       if(top2==size){
				       flag1=0;
				       return -1;
			       }else{
				       int ret=*(arr+top2);
				       top2++;
				       flag1=1;
				       return ret;
			       }
		       }
		       break;

		default:
		       	printf("Wrong choice\n");
			break;
	}
}

int peek(int *arr,int d){

	switch(d){
	
		case 1:
			{
				if(top1==-1){
					flag=0;
					return -1;
				}
				else{
					flag=1;
					printf("peeked value from stack1 \n");
					return *(arr+top1);
				}
			}
			break;

		case 2:
			{
				if(top2==size){

					flag1=0;
					return -1;
				}else{
				
					flag1=1;
					printf("peeked value from stack2 \n");
					return *(arr+top2);
				}
			}
			break;
	}
}

void main(){

	char choice;
	printf("Enter array size\n");
	scanf("%d",&size); 
	int arr[size];
	top1=-1;
	top2=size;

	do{
		int d;
		printf("1.push1\n");
		printf("2.push2\n");
		printf("3.pop1\n");
		printf("4.pop2\n");
		printf("5.peek1\n");
		printf("6.peek2\n");
		printf("Enter choice\n");
		scanf("%d",&d);

		switch(d){
		
			case 1:{
			       	       int ret = push(arr,1);
					if(flag==-1)
						printf("Stack1 Overflow\n");
			       }
			       break;
			case 2:{
			       	       int ret = push(arr,2);
					if(flag==-1)
						printf("Stack1 Overflow\n");
			       }
			       break;

			case 3:
			       {
				       int ret = pop(arr,1);

					       if(flag==0 )
					       		printf("stack1 underflow\n");
					       else
					       	       printf("%d\n",ret);
			       }
			       break;
			case 4:
			       {
				       int ret = pop(arr,2);


				       	   if(flag1==0)
					       printf("stack2 underflow\n");
				           else
					       printf("%d\n",ret);
			       }
			       break;

			case 5:
			       {
				       int ret = peek(arr,1);
					       if(flag==0 )
					       		printf("stack1 underflow\n");
					       else
				       			printf("%d\n",ret);
			       }
			       break;
			case 6:
			       {
				       int ret = peek(arr,2);
					       if(flag1==0 )
					       		printf("stack2 underflow\n");
					       else
				       			printf("%d\n",ret);
			       }
			       break;
		}
		flag=0,flag1=0;
		getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
