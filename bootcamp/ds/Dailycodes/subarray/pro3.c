
//print sum of every subarray

#include<stdio.h>

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array \n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}
	int sum = 0;
	printf("----------------\n");
	for(int i=0;i<n;i++){
		sum = 0;
		for(int j=i;j<n;j++){
		
			sum += arr[j];

			printf("%d\n",sum);
		}
	}
}
