
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo* prev;
	int d;
	struct Demo* next;
}d;

void deleteLast(d**);
void deleteFirst(d**);

d* createNode(){

	d* newNode= (d*)malloc(sizeof(d));

	printf("Enter number\n");
	scanf("%d",&newNode->d);
	newNode->prev= NULL;
	newNode->next=NULL;
	return newNode;
}

void addNode(d** head){

	d* newNode = createNode();

	if(*head==NULL){
	
		 *head = newNode;
	}else{
	
		d* temp= *head;

		while(temp->next!=NULL){
		
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev = temp;
	}
}

int countNode(d*h){

	int p=0;

	while(h!=NULL){
	
		p++;
		h=h->next;
	}
	return p;
}
void delAtPos(d**head,int pos){

	if(pos<=0 || pos>countNode(*head)){
	
		printf("Invalid node %d Total nodes are %d\n",pos,countNode(*head));
	}
	else if(*head==NULL){
	
		printf("No nodes present\n");		

	}else if(pos==1){
	
		deleteFirst(head);
	}else if(pos==countNode(*head)){
	
		deleteLast(head);
	}else{
	
		d* temp=*head;
		while((pos--)-2){
		
			temp = temp->next;
		}
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next->prev=temp;
	}
}
void addFirst(d**head){

	d* temp = createNode();

	if(*head==NULL){
	
		*head=temp;
	}else{
		d* temp1 = *head;

		temp->next=*head;
		(*head)->prev=temp;
		*head=temp;
	}
}

void deleteFirst(d**head){

	if(*head==NULL){
		printf("No Node present\n");
		return;
	}else if((*head)->next==NULL){

		free(*head);
		*head= NULL;
	}else{
	
		(*head)=(*head)->next;
		free((*head)->prev);
		(*head)->prev=NULL;
	}
}

void deleteLast(d**head){

	d*temp=*head;

	if(*head==NULL){
		printf("No Node present\n");
		return;
	}
	else if(temp->next==NULL){
	
		free(*head);
		*head=NULL;
	}
	else{	
		while(temp->next->next!=NULL){
	
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

void addLast(d**head){

	addNode(head);
}
void addpos(d**head,int pos){

	if(pos<=0 || pos>countNode(*head)+1){
	
		printf("Wrong position %d total nodes are %d\n",pos,countNode(*head));
		
	}

	else if(pos==1){
		addFirst(head);

	}else if(pos==countNode(*head)+1){
	
		addLast(head);
	}
	else{
		d* newNode= createNode();		
		d*temp2  =*head;
		while(pos-2>0){
	
			temp2=temp2->next;
			pos--;
		}
		newNode->next = temp2->next;
		newNode->prev = temp2;
		temp2->next->prev = newNode;
		temp2->next= newNode;
	}
}
void printll(d*temp){

	if(temp==NULL){
	
		printf("No nodes are present\n");
	}
	else{
	
		while(temp!=NULL){
			printf("|%d| ",temp->d);
			temp=temp->next;
		}
		printf("\n");
	}
}

void main(){

	d* head= NULL;

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addAtposition\n");
		printf("4.addLast\n");
		printf("5.deletefirst\n");
		printf("6.deletelast\n");
		printf("7.deleteatpos\n");
		printf("8.countNodes\n");
		printf("9.printll\n");
		printf("10.Exit\nYour choice :- ");
		int ch;
		scanf("%d",&ch);

		switch(ch){
		
			case 1:				
				addNode(&head);
				break;

			case 2:
				addFirst(&head);
				break;

			case 3:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);

					addpos(&head,pos);
				}
				break;

			case 4:
				addLast(&head);
				break;

			case 5:
				deleteFirst(&head);
				break;

			case 6:
				deleteLast(&head);
				break;
			case 7:{
				       int pos;
				       printf("Enter position\n");
				       scanf("%d",&pos);
				delAtPos(&head,pos);
			       }
				break;

			case 8:
				printf("Total number of nodes are %d\n",countNode(head));
				break;

			case 9:
				printll(head);
				break;

			case 10:
				printf("Quit\n");
				exit(0);
				break;
				
			default:
				printf("Wrong choice entered\n");
				break;

		}

		getchar();
		printf("Do you want to continue \n");
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
