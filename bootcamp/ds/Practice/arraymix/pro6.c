
#include<stdio.h>

int fun(int* arr,int n){

	int cnt = 0;
	for(int i=0;i<n;i++){
	
		for(int j=0;j<n;j++){
		
			if(*(arr+j)>*(arr+i)){
			
				cnt++;
				break;
			}
		}
	}

	return cnt;
}

void main(){

	int n;

	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	printf("o/p : %d\n",fun(arr,n));
}
