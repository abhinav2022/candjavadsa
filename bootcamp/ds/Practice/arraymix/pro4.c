
#include<stdio.h>

void fun(int* arr,int i,int j){

	while(i<j){
	
		int temp = arr[i];
		arr[i]=arr[j];
		arr[j]=temp;

		i++;
		j--;
	}	
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}
	printf("------------\n");
	fun(arr,0,n-1);

	for(int i=0;i<n;i++){
	
		printf("%d ",arr[i]);
	}
	
}
