#include<stdlib.h>
#include<stdio.h>

int* fun(int (*arr)[],int n,int b){

	int *ptr = (int*)malloc(n*sizeof(int));
	for(int i=0;i<n;i++){
		int sum =0;
		for(int j=0;j<b;j++){
			sum+=*(*(arr)+j+b*i) ;
		}
			*(ptr+i)=sum ;
	}

	return ptr;
}

void main(){

	int n,b;
	printf("Enter number\n");
	scanf("%d",&n);
	scanf("%d",&b);
	int arr[n][b];

	for(int i=0;i<n;i++){
	
		for(int j=0;j<b;j++)
		scanf("%d",*(arr+i)+j);
	}
	printf("------------\n");
	int *ptr=fun(arr,n,b);

	for(int i=0;i<n;i++){
	
		printf("%d ",ptr[i]);
	}
	
}
