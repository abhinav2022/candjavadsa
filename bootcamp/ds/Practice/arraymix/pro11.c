#include<stdlib.h>
#include<stdio.h>

int **fun(int (*arr)[],int r,int c){

	int **p = (int**)malloc((2*r-1)*sizeof(int*));
	int h=0;
	for(int i=0;i<2*r-1;i++){
	
		*(p+i)=(int *)malloc(c*sizeof(int));
		for(int j=0;j<c;j++){
			
			*(*p + i*c +j)=0;
		}
	}
	for(int i=0;i<2*r-1;i++){
		h=0;
		//*(p+i)=(int *)malloc(c*sizeof(int));
		for(int j=0;j<r;j++){
		
			for(int k=0;k<c;k++){
			
				if(i==j+k)
					*(*p +i*c +h++)=*(*(arr)+j*c+k);
			}
		}

	}

	return p;
}

void main(){

	int r,c;
	printf("Enter number\n");
	scanf("%d",&r);
	scanf("%d",&c);
	int arr[r][c];

	for(int i=0;i<r;i++){
	
		for(int j=0;j<c;j++)
		scanf("%d",*(arr+i)+j);
	}
	printf("------------\n");
	int **ptr=fun(arr,r,c);

	for(int i=0;i<2*r-1;i++){
	
		for(int j=0;j<c;j++){
				
			printf("%d ",*(*(ptr)+ i*c+j));
		}
		printf("\n");
	}
		printf("\n");
	
}
