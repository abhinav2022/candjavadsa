#include<stdlib.h>
#include<stdio.h>

int fun(int (*arr1)[],int (*arr2)[],int i,int j,int p,int q){
	
	if(i<p){
	
		if(j<q){
			if(*(*(arr1)+i*q + j) != *(*(arr2)+i*q+j)){
	
				return 0;
			}
			if(*(*(arr1)+i*q + j) == *(*(arr2)+i*q+j)){
	
				return fun(arr1,arr2,i,j+1,p,q);
			}
		}
		else
			return fun(arr1,arr2,i+1,0,p,q);
	}else
		return 1;

}

void main(){

	int p,q ;
	scanf("%d",&p);
	scanf("%d",&q);

	int arr1[p][q],arr2[p][q];

	for(int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			scanf("%d",*(arr1+i)+j);
		}
	}
	printf("-----------------\n");	
	for(int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			scanf("%d",*(arr2+i)+j);
		}
	}
	printf("-----------------\n");	
	int r = fun(arr1,arr2,0,q-1,p,q);

	printf("%d ",r);
	printf("\n");
}
