#include<stdlib.h>
#include<stdio.h>

int gun(int (*arr)[],int i,int j,int p,int q){

	if(j<q)
		return *(*(arr) + i*q + j) + gun(arr,i,j+1,p,q);

	else
		return 0;
}

void fun(int (*arr)[],int*ptr,int i,int j,int p,int q){
	//int cnt = 0;
	if(i<p){
	
		*(ptr+i)=gun(arr,i,j,p,q);

		fun(arr,ptr,i+1,j,p,q);
	}
}

void main(){

	int p,q ;
	scanf("%d",&p);
	scanf("%d",&q);

	int arr[p][q];

	for(int i=0;i<p;i++){
	
		for(int j=0;j<q;j++){

			scanf("%d",*(arr+i)+j);
		}
	}
	
	int *ptr = (int*)malloc(p*sizeof(int));
		
	fun(arr,ptr,0,0,p,q);

	for(int i=0;i<p;i++){
	
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}
