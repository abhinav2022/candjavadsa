
#include<stdio.h>

int fun(int* arr,int i,int j,int s){
	//int cnt = 0;
	if(i<=s){
	
		if(j==s+1)
			return fun(arr,++i,0,s);

		else if(j<=s && arr[i]<arr[j])
			return 1 + fun(arr,++i,0,s);

		else 
			return fun(arr,i,++j,s);
	}else
		return 0;
}

void main(){

	int p ;
	scanf("%d",&p);

	int arr[p];

	for(int i=0;i<p;i++){
	
		scanf("%d",arr+i);
	}

	int m = fun(arr,0,0,p-1);

	printf("m %d\n",m);
}
