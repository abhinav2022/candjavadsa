
#include<stdio.h>

int fun(int*arr,int s,int k){

	int p=-1;
	int start=0;
	int end = s-1;
	int mid;

	 while(start<=end){
	 
		 mid=(start+end)/2;
		 if(*(arr+mid)==k){
		 
			 return mid;
		 }
		 if(*(arr+mid)<*(arr+start)){
		 
			 if(*(arr+start)>=k && *(arr+mid)<=k){
			 
				 start=mid+1;
			 }else
				 end=mid-1;
		 }else{
		 
			 if(*(arr+mid)>k && *(arr+end)<k){
			 
				 start  =mid+1;
			 }else
				 end=mid-1;
		 }

		 /*if(*(arr+mid)>k){
		 
			 end =mid -1;
		 }else if(*(arr+mid)<k){
		 
			 start = mid+1;
		 }*/
	 }

	return -1;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	for(int i =0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	int t ,l;

	printf("Enter key number\n");
	scanf("%d",&t);

	l = fun(arr,n,t);

	if(l==-1){
	
		printf("Not found\n");
	}
	else
		printf("number is %d\n",l);
}
