
#include<stdio.h>

int fun(int*arr,int s){

	int p=*(arr+s-1);
	int start=0;
	int end = s-1;
	int mid;

	while(start<=end){
	
		mid=(end+start)/2;
		
		if(*(arr+mid)>*(arr+mid+1)){
		
			return *(arr+mid);
		}
		if(*(arr+mid-1)>*(arr+mid)){
		
			return *(arr+mid-1);
		}
		if(*(arr+mid)<*(arr+start)){
		
			end = mid-1;
		}else
			start = mid+1;

		/*if(start==end){
		
			return *(arr+mid);
		}
		if(*(arr+mid)>*(arr+start)){

			start=mid+1;
		}else
			end --;*/

	}
	return p;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	for(int i =0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	int t ,l;

	l = fun(arr,n);

	if(l==-1){
	
		printf("Not found\n");
	}
	else
		printf("number is %d\n",l);
}
