
#include<stdio.h>

int fun(int*arr,int s,int k){

	int p=-1;
	int start=0;
	int end = s-1;
	int mid;

	while(start<=end){
	
		mid=(end+start)/2;

		if(*(arr+mid)==k){
		
			return *(arr+mid);

		} 
		if(*(arr+mid)<k){
		
			p=*(arr+mid);
			start = mid+1;
		}else{
		
			end = mid -1;
		}
	}
	return p;
}

void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);
	int arr[n];

	for(int i =0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	int t ,l;

	printf("Enter key number\n");
	scanf("%d",&t);

	l = fun(arr,n,t);

	if(l==-1){
	
		printf("Not found\n");
	}
	else
		printf("number is %d\n",l);
}
