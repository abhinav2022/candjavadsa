
#include<stdio.h>
#include<stdlib.h>

int *fun(int (*arr)[],int c,int r){

	int *p = (int*)malloc(r*sizeof(int));

	for(int i=0;i<r;i++){
	
		for(int j=0;j<c;j++){
		
			*(p+i)+=*(*(arr)+j*r+i);
		}
	}

	return p;
}

void main(){

	int p,m;
	printf("Enter size\n");
	scanf("%d",&p);
	scanf("%d",&m);

	int arr[p][m];

	for(int i=0;i<p;i++){
	
		for(int j=0;j<m;j++){
		
			scanf("%d",(*(arr+i)+j));
		}
	}

	int * o =fun(arr,p,m);
	printf("---------\n");
	for(int i=0;i<m;i++){
	
		printf("%d\n",*(o+i));
	}

}
