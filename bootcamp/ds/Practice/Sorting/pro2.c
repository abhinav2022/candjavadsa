
#include<stdio.h>

int factor(int a){

	int h=0;
	for(int i=1;i<a;i++){
	
		if(a%i==0)
			h++;
	}
	return h;
}

void sort(int*arr,int n){

	for(int i=0;i<n;i++){
	
		for(int j=0;j<n-1;j++){
		
			if(factor(*(arr+j))>factor(*(arr+j+1))){
			
				int t = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=t;
			}
		}
	}
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);
	int arr[n];

	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	printf("-----------------\n");
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}
}
