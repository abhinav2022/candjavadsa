
#include<stdio.h>

void sort(int*arr,int n){

	for(int i=0;i<n;i++){
	
		for(int j=0;j<n-1-i;j++){
		
			if(*(arr+j)>*(arr+j+1)){
			
				int t = *(arr+j);
				*(arr+j)=*(arr+j+1);
				*(arr+j+1)=t;
			}
		}
	}
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);
	int arr[n];

	printf("Enter array elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}

	sort(arr,n);
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(arr+i));
	}
}
