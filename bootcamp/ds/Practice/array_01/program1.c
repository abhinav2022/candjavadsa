
#include<stdio.h>

int fun(int *arr,int s){

	int max = 0,cnt=0;
	for(int i=0;i<s;i++){
	
		if(*(arr+i)>max)
			max=*(arr+i);
	}

	for(int i=0;i<s;i++){
	
		if(*(arr+i)<max)
			cnt++;
	}
	return cnt;
}

void main(){

	int n;
	printf("Enter size\n");
	scanf("%d",&n);

	int arr[n];

	printf("Enter number\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",arr+i);
	}
	int p= fun(arr,n);
	printf("%d\n",p);
}
