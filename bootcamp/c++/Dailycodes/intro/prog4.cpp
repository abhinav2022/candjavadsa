
#include "iostream"

void p(int (*arr)[],int s,int a){


	for(int i=0;i<s;i++){
	
		for(int j=0;j<a;j++){
		
			std::cout<<*(arr+i*a)+j<<" ";
		}
		std::cout<<std::endl;
	}
	//std::cout<<"bfs\n";
}

int main(){

	int arr[][3]={1,2,3,4,5,6,7,8,9};

	std::cout<<sizeof(arr)<<std::endl;
	std::cout<<sizeof(arr[0])<<std::endl;

	p(arr,3,3);
	/*std::cout<<"dfs\n";
	p();
	std::cout<<p();
	*/
}
