
#include<iostream>

class Demo{

	int x = 10;
	static int y;
	public:

	Demo(){
	
		std::cout<<"NO args"<<std::endl;
	}
	Demo(int x){
	
		std::cout<<" args"<<std::endl;
		std::cout<<x<<std::endl;
	}
	Demo(Demo &x){
	
		std::cout<<"copy"<<std::endl;
		std::cout<<&x<<std::endl;
		std::cout<<this<<std::endl<<std::endl;
	}


	void disp(){
	
		std::cout<<x<<" "<<y<<std::endl;
	}

	static void info(){
	
		//std::cout<<x<<std::endl;
		std::cout<<y<<std::endl;
	}
};

int Demo::y=80;
int main(){

	Demo obj;
	//Demo::info();
	Demo obj1 {10};
	Demo obj2 (80);
	Demo obj3();//method declaration
	Demo obj4{};

	Demo obj5 = obj1;

	Demo obj6;
	obj6 =obj1;

	Demo *obj7=new Demo(obj);

}
