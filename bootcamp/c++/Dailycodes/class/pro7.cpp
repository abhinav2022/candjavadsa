
#include<iostream>

class Demo{

	int x = 10;
	static int y;

	public:
	void disp(){
	
		std::cout<<x<<" "<<y<<std::endl;
	}

	static void info(){
	
		std::cout<<y<<std::endl;
	}
};

int Demo::y=10;

int main(){

	Demo::info();
}
