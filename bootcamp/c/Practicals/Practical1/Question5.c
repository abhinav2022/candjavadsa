/*WAP that takes a number from 0 to 5 and prints its spelling, if the number is greater than 5 print entered number is greater than 5  */
#include<stdio.h>
void main(){

	int ip1;
	printf("Enter value \n");
	scanf("%d",&ip1);

	if(ip1<0){
		printf("Number is negative\n");
	}
	else{
	switch(ip1){
		case 0:
			printf("Zero\n");
			break;

		case 1:
			printf("One\n");
			break;

		case 2:
			printf("Two\n");
			break;

		case 3:
			printf("Three\n");
			break;

		case 4:
			printf("Four\n");
			break;

		case 5:
			printf("Five\n");
			break;

		default:
			printf("%d is greater than 5\n",ip1);
			break;

	}	}
}
