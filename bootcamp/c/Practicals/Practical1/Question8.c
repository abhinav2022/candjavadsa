/*Wap to confirm triangle by angles*/

#include<stdio.h>

void main(){

	float a,b,c;

	printf("Enter angle of 1st side\n");
	scanf("%f",&a);
	
	printf("Enter angle of 2nd side\n");
	scanf("%f",&b);
	
	printf("Enter angle of 3rd side\n");
	scanf("%f",&c);

	if(a<=0||b<=0||c<=0){
	
		if(a<0){
			printf("Wrong input in a %.2f\n",a);
		}if(b<0){
			printf("Wrong input in b %.2f\n",b);
		} if(c<0){
			printf("Wrong input in c %.2f\n",c);
		}
	}else{

	
		if(a+b+c==180){
		
			printf("It is a triangle \n");
		}
		else{
		
			printf("Triangle with angles %.2f,%.2f, and %.2f is an invalid one\n",a,b,c);
		}
	}
}
