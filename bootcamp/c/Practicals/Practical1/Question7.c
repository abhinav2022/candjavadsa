/*WAP to check pythagorean triplet or not*/

#include<stdio.h>
void main(){

	int a,b,c;

	printf("Enter value of 1st side\n");
	scanf("%d",&a);

	printf("Enter value of 2nd side\n");
	scanf("%d",&b);

	printf("Enter value of hypotenuse \n");
	scanf("%d",&c);

	if(a<=0 || b<=0 || c<=0){
	
		if(a<0){
			printf("Wrong input %d\n",a);
		}
		if(b<0){
			printf("Wrong input %d\n",b);
		}
		if(c<0){
			printf("Wrong input %d\n",c);
		}
	}
	else{
	
		if(c*c==a*a+b*b){
		
			printf("%d,%d,%d are pythagorean triplets \n",a,b,c);
		}
		else{

			printf("%d,%d,%d are not pythagorean triplets \n",a,b,c);
		
		}
	}
}
