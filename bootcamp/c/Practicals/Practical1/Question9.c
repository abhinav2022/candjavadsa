/*Wap to print the count of divisors of entered num*/

#include<stdio.h>

void main(){

	int x,count=0;
	printf("Enter value \n");
	scanf("%d",&x);

	for(int i=1;i<=x;i++){
	
		if(x%i==0){
			printf("%d is factor of %d\n",i,x);
			count++;
		}
	}
	printf("Total number of divisors are %d\n",count);
}
