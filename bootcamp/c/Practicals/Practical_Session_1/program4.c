
#include<stdio.h>

void main(){

	int size;

	printf("Enter array size \n");
	scanf("%d",&size);
	
	if(size<0){
	
		printf("Invalid size of array\n");
		return;
	}
	int arr[size];
	printf("Enter elements\n");
	for(int i=size-1;i>=0;i--){
	
		scanf("%d",&arr[i]);
	}
	printf("\n");
	for(int i=0;i<size;i++){
	
		printf("%d\n",*(arr+i));
	}
}
