
#include<stdio.h>

void main(){

	int size;

	printf("Enter size of array\n");
	scanf("%d",&size);

	if(size<0){
		
		printf("Invalid array size\n");
		return;

	}
	int arr[size],count=0;
	printf("Enter elements in array\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",&arr[i]);
	}

	for(int i=0;i<size;i++){
	
		if(arr[i]%2==0){
		
			count++;
		}
	}

	printf("Count of even numbers in array is %d\n",count);
	printf("Count of odd numbers in array is %d\n",size - count);
}
