
#include<stdio.h>

void main(){

	int temp,ip1,ip2;

	printf("Enter value of ip1\n");
	scanf("%d",&ip1);
	
	printf("Enter value of ip2\n");
	scanf("%d",&ip2);

	int *iptr1 =&ip1,*iptr2=&ip2;

	temp = *iptr1;
	*iptr1=*iptr2;
	*iptr2=temp;

	printf("ip1 is %d & ip2 is %d\n",ip1,ip2);
}
