/*1. WAP to find the given element from the array
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 14
Ip : enter element : 15
Op: 15 is present*/
#include<stdio.h>

void main(){

	int size ;
	printf("Enter array size \n");
	scanf("%d",&size);
	
	if(size<0){
	
		printf("Invalid array size\n");
		return;
	}

	int arr[size];
	printf("Enter elements in array\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",&arr[i]);
	}
	int search,flag=0,count=0;
	printf("Enter search element \n");
	scanf("%d",&search);

	for(int i=0;i<size;i++){
	
		if(arr[i]==search){
		
			flag = 1;
			break;
		}
		count++;
	}
	if(flag==1){
	
		printf("Element found in array at %d index\n",count);
	}else{
	
		printf("Element not found \n");
	}
}
