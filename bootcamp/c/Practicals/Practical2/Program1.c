/*WAp to find the sum of numbers that are not divisible by 3 up to a given number*/

#include<stdio.h>

void main(){

	int x,sum=0;
	printf("Enter number upto which operation would be done\n");
	scanf("%d",&x);
	
	if(x<=0){
	
		printf("Invalid input\n");
	}else{
		for(int i=1;i<=x;i++){
	
			if(i%3!=0){
		
				sum=sum + i;
			}
		}

		printf("Sum is %d\n",sum);
	}
}
