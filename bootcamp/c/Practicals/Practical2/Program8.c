/*Take input from user and print the product of digits*/

#include<stdio.h>

void main(){

	int num,r1,r2,mul=1;

        printf("Enter number \n");
        scanf("%d",&num);

        r1=num;
        if(num<0){

                printf("Invalid input \n");
        }else{

                while(r1>0){

                        r2 = r1%10;
                        mul = mul*r2 ;

                        r1/=10;
                }

                printf("Product of digits is %d\n",mul);
        }
}
