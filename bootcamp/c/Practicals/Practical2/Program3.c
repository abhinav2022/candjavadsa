/*WAP to print the divisors & count of divisors of the entered num */

#include<stdio.h>

void main(){

	int num,count=0,sum=0;

	printf("Enter value of num\n");
	scanf("%d",&num);

	if(num<0){
	
		printf("Wrong input\n");
	}else{
	
		printf("The divisors are ");

		for(int i=1;i<=num;i++){
	
	
			if(num%i==0){
		
		
				printf("%d ",i);
			
				count++;
			
				sum+=i;
		
			}
	}
	
		printf("\nThe count of divisors is %d\n",count);
	
		printf("The sum of divisors are %d\n",sum);
	
	}
}
