/*Take number from user and print the fibonacci series up to that number*/

#include<stdio.h>

void main(){

	int i=0,num1=0,num2=1,num3=0,next=0,u=1,ip,n=0;

	printf("Enter number\n");
	scanf("%d",&ip);


	printf("0 1 ");
	while(i<ip){
		num3 = num1 + num2;
		if(num3 > ip){
			break;
		}
		printf("%d ",num3);
	
		num1= num2;		
		num2 = num3;

		i++;
	}
	printf("\n");
}
