/*WAP to print all even numbers in reverse order and odd nnumbers in standard way.Bot separately. Within a range*/
/*2 9 
 *
 * 8 6 4 2 
 * 3 5 7 9
 *
 * */
#include<stdio.h>

void main(){

	int ip1,ip2,n=0;
	
	printf("Start value \n");
	scanf("%d",&ip1);

	printf("End value \n");
	scanf("%d",&ip2);
	
	if(ip1<0 || ip2<0){
		
		printf("Invalid input \n");

	}	
	else{
		
		for(int i=ip2;i>=ip1;){
	
			if(n==0){
		
				if(i%2==0 ){
					printf("%d ",i);
				}
				if(i<=ip1){
					
					n=1;
					i=ip2;
					printf("\n");
				}	
				if(n==0){
				
					i--;
				}

			}else{
				if(ip1%2!=0){

					printf("%d ",ip1);
				
				}
					ip1++;
			}
	
		}
		printf("\n");
}
}
