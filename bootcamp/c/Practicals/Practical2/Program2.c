/*WAP to print the addition of 1 to 10 with 10 to 1*/

#include<stdio.h>

void main(){

	int x=1,y=10;

	for(int i=1;i<=10;i++,y--,x++){
		
		printf("%d + %d = %d \n",x,y,x+y);
		
	}
}
