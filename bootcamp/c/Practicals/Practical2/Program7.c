/*WAP input number from the user and print the sum of digits*/

#include<stdio.h>

void main(){

	int num,r1,r2,sum=0;

	printf("Enter number \n");
	scanf("%d",&num);

	r1=num;
	if(num<0){
	
		printf("Invalid input \n");
	}else{
	
		while(r1>0){
		
			r2 = r1%10;
			sum = sum+ r2 ;

			r1/=10;
		}	

		printf("sum of digits is %d\n",sum);
	}
}
