/*Take input number from the user and print the product of digits*/

#include<stdio.h>

void main(){

	int num,r1,r2,sum=0;

        printf("Enter number \n");
        scanf("%d",&num);

        r1=num;
        if(num<0){

                printf("Invalid input \n");
        }else{

                while(r1>0){

                        r2 = r1%10;
                        sum = sum*10 +r2 ;

                        r1/=10;
                }

                printf("reverse of digits is %d\n",sum);
        }
}
