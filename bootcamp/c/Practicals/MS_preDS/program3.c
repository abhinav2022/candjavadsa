//WAP to allocate memory for salary n friends

#include<stdio.h>
#include<stdlib.h>

void main(){

	int n;
	printf("Enter number of friends\n");
	scanf("%d",&n);

	double *ptr=(double*)calloc(n,sizeof(double));

	printf("Enter salary\n");
	for(int i=0;i<n;i++){
	
		scanf("%lf",ptr+i);
	}
	for(int i=0;i<n;i++){
	
		printf("%.2lf\n",*(ptr+i));
	}
	free(ptr);
	
}
