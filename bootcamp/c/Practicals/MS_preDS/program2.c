//WAP to show the use of dangling pointer in malloc.

#include<stdio.h>
#include<stdlib.h>
void main(){

	int *ptr=(int*)malloc(sizeof(int));
	printf("Enter number\n");
        scanf("%d",ptr);
	printf("number is \n");
        printf("%d\n",*ptr);

	int *ptr1=ptr;
        printf("%d\n",*ptr1);
        free(ptr);
        printf("ptr1 %d\n",*ptr1);
        printf("%p\n",ptr1);	
        printf("ptr %d\n",*ptr);
        printf("%p\n",ptr);	
        free(ptr1);
}
