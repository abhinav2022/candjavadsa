//WAP to free memory without using free function

#include<stdio.h>
#include<stdlib.h>
void main(){

	int n;
	printf("Enter number\n");
	scanf("%d",&n);

	int *ptr= (int*)calloc(5,sizeof(int));

	printf("Enter elements\n");
	for(int i=0;i<n;i++){
	
		scanf("%d",ptr+i);
	}
	printf("elements\n");
	for(int i=0;i<n;i++){
	
		printf("%d\n",*(ptr+i));
	}
	ptr=(int*)realloc(ptr,0); //Can be used as free

}
