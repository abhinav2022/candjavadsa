/*7. take no of rows from the user
1 2 3 4
25 36 49 64
9 10 11 12
169 196 225 256*/
#include<stdio.h>

void main(){

	int row,col,x=1,l;

        printf("Enter number of rows \n");
        scanf("%d",&row);

        printf("Enter number of column \n");
        scanf("%d",&col);
        l=col;

        if(row<=0 || col<=0){

                printf("Invalid input\n");
                return;
        }
        for(int i=1;i<=row;){

                if(l>0){
			
			if(i%2!=0){
                       		 printf("%d ",x);
			}else{
				printf("%d ",x*x);
			}
			x++;
                        l--;
                }else{

                        printf("\n");
                        l=col;
                        i++;
                }
        }
}
