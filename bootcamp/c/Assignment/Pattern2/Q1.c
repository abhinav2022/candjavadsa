/*1. take no of rows from the user
4 3 2 1
5 4 3 2
6 5 4 3
7 6 5 4
*/
#include<stdio.h>

void main(){

	int row,col,x=4,y,l;

	printf("Enter number of row\n");
	scanf("%d",&row);
	
	printf("Enter number of column\n");
	scanf("%d",&col);

	l=col;
	y=x;

	if(row<=0 || col<=0){
	
		printf("Invalid input\n");
		return;
	}
	for(int i=1;i<=row;){
	
		if(l>0){
		
			printf("%d ",x);
			x--;
			l--;
		}else{
		
			printf("\n");
			x=++y;
			l=col;
			i++;
		}
	}
}
