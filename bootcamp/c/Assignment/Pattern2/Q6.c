/*6. take no of rows from the user
= = = =
$ $ $ $
= = = =
$ $ $ $*/
#include<stdio.h>

void main(){

        int row,col,x=4,l;
	char ch1='=';

        printf("Enter number of rows \n");
        scanf("%d",&row);

        printf("Enter number of column \n");
        scanf("%d",&col);

        l=col;

        if(row<=0 || col<=0){

                printf("Invalid input\n");
                return;
        }
        for(int i=1;i<=row;){

                if(l>0){

                        printf("%c ",ch1);
                        l--;
                }else{

                        printf("\n");
                        i++;
			ch1='=';
			if(i%2==0){
				ch1='$';
			}
                        l=col;
		}
        }
}
