/*4. take no of rows from the user
I H G
F E D
C B A*/
#include<stdio.h>

void main(){

	char ch = 'I';
        int row,col,l;

        printf("Enter number of rows \n");
        scanf("%d",&row);

        printf("Enter number of column \n");
        scanf("%d",&col);

        l=col;

        if(row<=0 || col<=0){

                printf("Invalid input\n");
                return;
        }
        for(int i=1;i<=row;){

                if(l>0){

                        printf("%c ",ch);
			ch--;
                        l--;
                }else{

                        printf("\n");
                        l=col;
                        i++;
                }
        }
}
