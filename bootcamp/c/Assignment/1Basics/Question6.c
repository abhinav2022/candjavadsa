/*6. Write a program to print reverse from 100-1*/

#include<stdio.h>

void main(){

	int ip1;
	int ip2;

	printf("Enter starting value \n");
	scanf("%d",&ip1);

	printf("Enter ending value \n");
	scanf("%d",&ip2);

	for(int i=ip2;i>=ip1;i--){
	
		printf("%d \n",i);
	}

}
