/*10. Write a program to print the product of the first 10 numbers*/

#include<stdio.h>

void main(){

	 int x=1,m=1,ip1,ip2;

        printf("Enter number from start\n");
        scanf("%d",&ip1);

        printf("Enter number where it end\n");
        scanf("%d",&ip2);

        for(int i=ip1;i<=ip2;i++){
	
		if(m>=10){
			break;
		}
                        x*=i;
			m++;

        }
        printf("Product of odd numbers from range %d to %d is %d\n",ip1,ip2,x);
}
