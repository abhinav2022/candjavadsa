/*8. Write a program to print a table of 11 in reverse order*/

#include<stdio.h>

void main(){

	int x,ip1,ip2;

        printf("Enter number whose table is to be printed\n");
        scanf("%d",&x);

        printf("Enter number from where table will start\n");
        scanf("%d",&ip1);

        printf("Enter number where table will end\n");
        scanf("%d",&ip2);

        for(int i=ip2;i>=ip1;i--){

                printf(" %d x %d = %d\n",x,i,x*i);
        }
}
