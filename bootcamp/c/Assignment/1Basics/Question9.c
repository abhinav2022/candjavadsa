/*9. Write a program to print the sum of the first 10 odd numbers*/

#include<stdio.h>

void main(){

	int x=0,ip1,ip2;

	printf("Enter number from start\n");
	scanf("%d",&ip1);
	
	printf("Enter number where it end\n");
	scanf("%d",&ip2);
	
	for(int i=ip1;i<=ip2;i++){

		if(i%2!=0){
		     			x+=i;
		}
	}
	printf("Sum of odd numbers from range %d to %d is %d\n",ip1,ip2,x);
}
