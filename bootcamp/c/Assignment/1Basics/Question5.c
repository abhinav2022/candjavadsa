/*5. Write a program to print ASCII values from 0 to 127*/

#include<stdio.h>

void main(){

	int ip1;
	int ip2;

	printf("Enter value of ip1\n");
	scanf("%d",&ip1);

	printf("Enter value of ip2\n");
	scanf("%d",&ip2);

	for(int i=ip1;i<=ip2;i++){
	
		printf("ascii value of %c = %d \n",i,i);
	}
}
