/*Program 10: Write a program to check if a character is a vowel or
consonant. Take all the values from the user
Input: var= ”A”;
Output: A is a vowel.
Input: var= ”D”;
Output: D is a consonant.
*/
#include<stdio.h>

void main(){

	char a;
	printf("Enter value of character a \n");
	scanf("%c",&a);

	if(a=='A'||a=='E'||a=='I'||a=='O'||a=='U'||a=='a'||a=='e'||a=='i'||a=='o'||a=='u'){
	
		printf("%c is vowel character \n",a);

	}else if((a>=0 && a<=46)||(a>=91 && a<=96)||(a>122 && a<=127)){
	
		printf("%c is not an alphabet \n",a);

	}else if(a>47 && a<58){
		
		printf("%c is a number\n",a);

	}else {
	
		printf("%c is consonant character \n",a);
	}
}
