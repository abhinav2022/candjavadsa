/*
Program 4: WAP to find min among 2 numbers. Take all the values from the user
Input : 2 3
Output: 2
*/
#include <stdio.h>

void main(){

	int a,b;

	printf("Enter value of a\n");
	scanf("%d",&a);

	printf("Enter value of b\n");
	scanf("%d",&b);

	if(a<b){
	
		printf("Min value among %d and %d is %d \n",a,b,a);
		
	}else if(b<a){
	
		printf("Min value among %d and %d is %d \n",a,b,b);
	}else{
	
		printf("Both values are same\n");
	}
}
