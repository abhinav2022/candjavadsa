/*Program 3: WAP to find max among 2 numbers. Take all the values from the
user
Input : 2 4
Output: 4
*/
#include<stdio.h>

void main(){

	int a,b;

	printf("Enter value of a \n");

	scanf("%d",&a);
	printf("Enter value of b \n");

	scanf("%d",&b);

	if(a>b){
		
		printf("Max value among %d and %d is %d \n",a,b,a);

	}else if(b>a){
		
		printf("Max value among %d and %d is %d \n",a,b,b);

	}else{
		printf("Both values are same \n");
	}
}
