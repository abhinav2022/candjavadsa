/*
Program 9: Write a program, take a number and print whether it is positive or
negative. Take all the values from the user
Input: var=5
Output: 5 is a positive number
Input: var=-9
Output: -9 is a negative number
*/
int printf(const char *,...);

int scanf(const char*,...);

void main(){

	int var ;

	printf("Enter value of integer\n");

	scanf("%d",&var);

	if(var>0){
		
		printf("%d is positive number\n",var);
	
	}else if(var<0){
	
		printf("%d is negative number\n",var);
	
	}else{
	
		printf("Wrong input\n");
	}

}
