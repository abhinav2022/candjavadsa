/*Program 2: WAP print the value of the below expressions.
 x =9 ;
ans = ++x + x++ + ++x
Print ans value and print x

ans1= ++x + ++x + ++x + ++x
Print ans1 value and print x

ans2 = x++ + x++ + ++x + x++ + ++x
Print ans2 value and print x

ans3 = x++ + x++ + x++ + x++
Print ans3 value and print x */

#include<stdio.h>

void main(){

	int x,ans,ans2,ans3,ans1;
	printf("Enter value of x\n");
	scanf("%d",&x);
	ans =  ++x + ++x + x++ + ++x; //35

	printf("Value of ans = ++x + x++ + ++x is %d\n",ans);
	printf("Value of x is %d\n",x);
	
	ans1 = ++x + ++x + ++x + ++x; //
	printf("Value of ans1 = ++x + ++x + ++x + ++x is %d\n",ans1); 
	printf("Value of x is %d\n",x);
	
	ans2 = x++ + x++ + ++x + x++ + ++x;
	printf("Value of ans2 = x++ + x++ + ++x + x++ + ++x is %d\n",ans2);
	printf("Value of x is %d\n",x);
	
	ans3 = x++ + x++ + x++ + x++;
	printf("Value of ans3 = x++ + x++ + x++ + x++ is %d\n",ans3);
	printf("Value of x is %d\n",x);

}
