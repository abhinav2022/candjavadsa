/*
Program 8: Write a program, take a character and print whether it is in
UPPERCASE or lowercase. Take all the values from the user
Input: var = A
Output: You entered the UPPERCASE character.
*/
int printf(const char *,...);

int scanf(const char *,...);

void main(){

	char var ;
	printf("Enter character value\n");
	scanf("%c",&var);

	if(var>=65 && var<=90){
	
		printf("%c is UpperCase letter\n",var);

	}else if(var>=97 && var<=122){
	
		printf("%c is LowerCase letter\n",var);
	}else if(var>=48 && var<=57){
	
		printf("%c is a number \n",var);
	}
	else{
	
		printf("Not a alphabet \n");
	}
}
