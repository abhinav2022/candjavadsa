/*Program 1:
Write a program to print the value and size of the below variables.Take all
the values from the user

num=10
chr = ‘S’
rs = 55.20
crMoney = 542154313480.544543*/

int printf(const char*,...);

int scanf(const char*,...);

void main(){

	int num =14;
	char chr ;
	float rs = 55.2f;
	double crMoney ;

	printf("Enter value of num \n");
	scanf("%d",&num);
	printf("Enter value of chr \n");
	scanf(" %c",&chr);
	printf("Enter value of rs \n");
	scanf("%f",&rs);
	printf("Enter value of crMoney \n");
	scanf("%lf",&crMoney);

	printf("\nValue of num is %d\n",num);
	printf("Size of integer num is %ld\n",sizeof(num));
	printf("Value of character chr is %c\n",chr);
	printf("Size of character chr is %ld\n",sizeof(chr));
	printf("Value of float rs is %f\n",rs);
	printf("Size of rs is %ld\n",sizeof(rs));
	printf("Value of double is %lf\n",crMoney);
	printf("Size of crMoney is %ld\n",sizeof(crMoney));
}
