
#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int*)calloc(2,sizeof(int));

	*ptr=9;
	*(ptr+1)=8;

	printf("%d\n",*ptr);
	printf("%d\n",*(ptr+1));

	free(ptr);

	printf("%d\n",*ptr);
	printf("%d\n",*(ptr+1));
}
