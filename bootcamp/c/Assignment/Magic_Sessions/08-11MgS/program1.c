
#include<stdio.h>

int mystrlen(char * a){

	int count = 0;

	while(*a!='\0'){
	
		count++;
		a++;
	}
	return count;
}

void main(){

	char arr[200];
	printf("Enter String\n");
	gets(arr);

	int p = mystrlen(arr);
	puts("Length of entered string is :");
	printf("%d\n",p);
}
