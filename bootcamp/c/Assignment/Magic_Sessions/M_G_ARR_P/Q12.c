
#include<stdio.h>

void main(){

	int row,col;
	printf("Row size\n");
	scanf("%d",&row);
	printf("Column size\n");
	scanf("%d",&col);

	int arr[row][col],sum=0;
	printf("Enter elements\n");
	for(int i=0;i<row;i++){
	
		for(int j=0;j<col;j++){
		
			scanf("%d",(*(arr+i)+j));
		}
	}
	
	for(int i=0;i<row;i++){
	
		for(int j=0;j<col;j++){
		
			if(i==j || i+j==col-1){
			
				sum+=*(*(arr+i)+j);
			}
		}
	}

	printf("\nSum is %d\n",sum);
}
