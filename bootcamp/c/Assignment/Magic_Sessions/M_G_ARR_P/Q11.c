
#include<stdio.h>

void main(){

	int size;

	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",arr+i);
	}

	for(int i=0;i<size;i++){
	
		for(int j=i+1;j<size;j++){
		
			if(*(arr+i)>*(arr+j)){
			
				int temp = *(arr+i);
				*(arr+i)=*(arr+j);
				*(arr+j)=temp;
			}
		}
	}

		printf("\n2nd largest element is %d \n",*(arr+size-2));
}
