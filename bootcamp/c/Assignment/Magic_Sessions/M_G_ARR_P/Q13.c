
#include<stdio.h>

void main(){

	int x = 10;
	char ch = 'A';
	float f = 10.5f;
	double d = 20.5;

	void *ptr;

	ptr = &x;

	printf("%d\n",*(int *)ptr);

	ptr = &ch;

	printf("%c\n",*(char *)ptr);

	ptr = &f;

	printf("%f\n",*(float*)ptr);

	ptr = &d;

	printf("%lf\n",*(double *)ptr);
}
