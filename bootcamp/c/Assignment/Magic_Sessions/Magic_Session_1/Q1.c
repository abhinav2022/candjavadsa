/*1. WAP To Print all the divisors of given no
Input: 30
Output: 1 2 3 5 6 10 15 30
*/
#include<stdio.h>

void main(){

	int ip1;

	printf("Enter number \n");
	scanf("%d",&ip1);
	
	printf("Divisors of %d are \n",ip1);
	if(ip1==0){
	
		printf("1\n");
		return;
	}else if(ip1>0){

		for(int i=1;i<=ip1;i++){
			
			if(ip1%i==0){
		
				printf("%d",i);
				if(i!=ip1){
			
					printf(",");
				}
			}
		}
	}else{
	
		printf("1,");
		
		for(int i=-1;i>=ip1;i--){
		
			if(ip1%i==0){
			
				printf("%d",i);
				if(i!=ip1){
			
					printf(",");
				}
			}
		}

	}
	printf("\n");
}
