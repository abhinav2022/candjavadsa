/*D e F g
  e D c B
  F g H i
  g F e D */
#include<stdio.h>

void main(){

	int row;
	
	printf("Enter number of rows\n");
	scanf("%d",&row);

	char ch = 64+row,ch1=ch;

	if(row<=0){
	
		printf("Invalid input\n");
		return;
	
	}else{
	
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){
			
				if((i+j)%2==0){
				
					if(ch>=97 && ch<=122){
					
						ch-=32;
					}
				}else{
				
					if(ch>=65 && ch<=90){
					
						ch+=32;
					}
				}
				printf("%c ",ch);

				if(i%2==0){
				
					ch--;
				}else{
				
					ch++;
				}
			}
			printf("\n");
			ch=++ch1;
		}
	}
}
