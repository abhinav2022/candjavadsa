/*4. take no of rows from the user
* - - -
- * - -
- - * -
- - - * */
#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows\n");
	scanf("%d",&row);

	if(row<0){
		
		printf("Invalid input\n");
		return;
	
	}else{
	
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){
			
				if(i==j){
				
					printf("* ");
				
				}else{
				
					printf("_ ");
				}
			}
			printf("\n");
		}
	}
}
