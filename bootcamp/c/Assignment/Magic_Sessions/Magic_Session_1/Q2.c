/*2. WAP to count digits in given no
Input: 94211
Output: digit count is 5*/
#include<stdio.h>

void main(){

	int ip1,count=0;
	printf("Enter number\n");
	scanf("%d",&ip1);

	if(ip1==0){
	
		printf("0\n");
	}
	while(ip1>0){
	
		count++;
		ip1/=10;
	}

	printf("Digit count is %d\n",count);
}
