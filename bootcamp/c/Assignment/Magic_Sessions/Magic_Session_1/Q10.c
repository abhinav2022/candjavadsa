/*10. take no of rows from the user
D D D D
  C C C
    B B
      A*/
#include<stdio.h>

void main(){

	int row;

	printf("Enter number of rows\n");
	scanf("%d",&row);
	
	char ch = 64 + row;

	for(int i=1;i<=row;i++){
	
		for(int j=1;j<=row;j++){
		
			if(i>j){
			
				printf("  ");
			
			}else{
			
				printf("%c ",ch);
			}
		}
		ch--;
		printf("\n");
	}

}
