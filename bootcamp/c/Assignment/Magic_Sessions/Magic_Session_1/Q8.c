/*8. take no of rows from the user
0
1 1
2 3 5
8 13 21 34 */
#include<stdio.h>

void main(){

	int row,z=0,prev=0,curr=1,dig;
	printf("Enter no. of rows\n");
	scanf("%d",&row);

	for(int i=1;i<=row;i++){
	
		for(int j=1;j<=i;j++){
		
			if(z<=1){
			
				printf("%d ",z);
				z++;
			
			}else{
			
				dig = prev + curr;
				printf("%d ",dig);
				prev = curr;
				curr = dig;
			}
		}
		printf("\n");
	}
}
