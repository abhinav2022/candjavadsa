/*9. take no of rows from the user
      1
    1 2
  1 2 3
1 2 3 4*/
#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows\n");
	scanf("%d",&row);

	printf("\n");

	if(row<=0){
	
		printf("Invalid input\n");
		return;
	
	}else{
	
		for(int i=1;i<=row;i++){
		
			int l=1;

			for(int j=1;j<=row-i;j++){
			
				printf("  ");
			}

			for(int j=1;j<=i;j++){
			
				printf("%d ",l);
				l++;
			}
			printf("\n");
		}
	}
}
