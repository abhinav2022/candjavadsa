/*3. WAP to print all the composite numbers between a given range
Input : Enter Start 1
Enter End 15
Output:
4 6 8 9 10 12 14 15
Input : Enter Start 31
Enter End 35
Output:
32 33 34 35*/
#include<stdio.h>

void main(){

	int start,end;

	printf("Enter starting value in range\n");
	scanf("%d",&start);

	printf("Enter end value in range\n");
	scanf("%d",&end);

	if(start<=0 || end<=0){
	
		printf("Invalid input\n");
		return;
	}

	if(start<=end){

		for(int i=start;i<=end;i++){
			int count=0;
			for(int j=1;j<=i;j++){
		
				if(i%j==0){
					count++;
				}
			}

		if(count>2){
		
			printf("%d ",i);
		}
	}
	}else{
	
		for(int i=end;i<=start;i++){
			int count=0;
			for(int j=1;j<=i;j++){
		
				if(i%j==0){
					count++;
				}
			}

		if(count>2){
		
			printf("%d ",i);
		}

	}}
	printf("\n");
}
