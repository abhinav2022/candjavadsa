
#include<stdio.h>

void main(){

	int size1;

	printf("Enter size of 1D array \n");
	scanf("%d",&size1);

	int arr1[size1];
	printf("Enter elements\n");
	for(int i=0;i<size1;i++){
	
		scanf("%d",&arr1[i]);
	}

	printf("Elements are :-\n");
	for(int i=0;i<size1;i++){
	
		printf("arr[%d] = %d \n",i,arr1[i]);
	}

	int row,col;

	printf("Enter size of 2D array row\n");
	scanf("%d",&row);
	printf("Enter size of 2D array col\n");
	scanf("%d",&col);

	int arr2[row][col];
	printf("Enter elements\n");
	for(int i=0;i<row;i++){

		for(int j=0;j<col;j++){
	
			scanf("%d",&arr2[i][j]);
		}
	}

	printf("Elements are :-\n");
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
	
		printf("arr[%d][%d] = %d \n",i,j,arr2[i][j]);

		}
	}
	int row1,col1,plane;

	printf("Enter size of 3D array plane\n");
	scanf("%d",&plane);
	printf("Enter size of 3D array row\n");
	scanf("%d",&row1);
	printf("Enter size of 3D array col\n");
	scanf("%d",&col1);

	int arr3[plane][row1][col1];
	printf("Enter elements\n");
	for(int k=0;k<plane;k++){
	for(int i=0;i<row1;i++){

		for(int j=0;j<col1;j++){
	
			scanf("%d",&arr3[k][i][j]);
		}
	}
	}

	printf("Elements are :-\n");
	for(int k=0;k<plane;k++){
	for(int i=0;i<row1;i++){
		for(int j=0;j<col1;j++){
	
		printf("arr[%d][%d][%d] = %d \n",k,i,j,arr3[k][i][j]);

		}
	}
	}
}
