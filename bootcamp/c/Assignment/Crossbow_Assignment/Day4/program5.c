
#include<stdio.h>

int reverse(int* y){

	int temp=*y;
	int p=0,sum=0;

	while(temp>0){
	
		p=temp%10;
		sum=p+sum*10;
		temp/=10;
	}
	
	return sum;
}

void main(){

	int x;
	printf("Enter number \n");
	scanf("%d",&x);

	printf("\nreverse of %d is %d\n",x,reverse(&x));

}
