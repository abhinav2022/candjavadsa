
#include<stdio.h>

void swap(int *j,int*l){

	printf ("Before swapping j and l are %d %d\n",*j,*l);
	int temp= *j;
	*j= *l;
	*l= temp;
	printf("After swapping j and l are %d %d \n",*j,*l);
}

void main(){

	int x=4,y=5;

	printf("Before swapping x and y %d %d \n\n",x,y);
	swap(&x,&y);
	printf("\nAfter swapping x and y %d %d \n",x,y);
}
