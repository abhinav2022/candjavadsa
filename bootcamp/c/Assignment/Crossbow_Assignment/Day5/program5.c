
#include<stdio.h>

void retArr(char *ptr,int size){

	printf("Enter characters\n");
	for(int i=0;i<size;i++){
	
		getchar();
		scanf("%c",ptr+i);
	}
}

void main(){

	int size;
	printf("Enter size\n");
	scanf("%d",&size);

	char arr[size];

	retArr(arr,size);

	printf("Values are :-\n");
	for(int i=0;i<size;i++){
	
		printf("index - %d - %c\n",i,*(arr+i));
	}
}
