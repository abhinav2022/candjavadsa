//5.Draw a Neat Diagram,output and Arithmetic for Below Code.

#include<stdio.h>

void main(){

	int arr[]={10,20,30,40,50};

	int *ptr1= &arr[1];
	int *ptr2 = &arr[2];

	printf("%p\n",ptr1); //104
	printf("%p\n",ptr2);//108

	printf("%d\n",*ptr1); //104
	printf("%d\n",*ptr2);//108
//	printf("%d\n",--*ptr1); //19
//	printf("%d\n",arr[1]); //19
	ptr1++; //104 +4
	--ptr2; //108 - 4

	printf("%d\n",*ptr1); //30
	printf("%d\n",*ptr2); //20

	printf("%p\n",ptr1); //108
	printf("%p\n",ptr2); //104

	printf("%ld\n",(ptr1-ptr2)+2); //3
	printf("%p\n",ptr2+(ptr1-ptr2)); //104 + 1*4 (size of int)
	printf("%d\n",*(ptr2+(ptr1-ptr2))); //30

	printf("%p\n",(ptr1*2)); //error
	//printf("%p\n",(2**2)); //error
	printf("%d\n",*(ptr1*ptr2)); //error
	printf("%p\n",ptr1/2); //error
}
