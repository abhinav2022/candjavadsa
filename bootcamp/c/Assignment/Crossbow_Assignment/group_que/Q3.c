/*WAP to take an array from the user int main function and print that array in another function.Take the array size from the user.*/

#include<stdio.h>

void fun(int (*ptr)[],int size){

	printf("\n");
	for(int i=0;i<size;i++){
	
		printf("%d\n",*(*ptr+i));
	}
}

void main(){

	int size;
	printf("Enter size\n");
	scanf("%d",&size);
	
	int arr[size];

	for(int i=0;i<size;i++){
	
		printf("Enter value of %d element \n",i);
		scanf("%d",arr+i);
	}

	fun(&arr,size);

}
