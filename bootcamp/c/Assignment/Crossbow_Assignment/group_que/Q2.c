/*WAP to takeone value from the user in another Function and print that value in Main function.*/

#include<stdio.h>

int fun(){

	int p;
	printf("Enter number\n");
	scanf("%d",&p);

	return p;
}

void main(){

	int x = fun();

	printf("Value of x is %d\n",x);
}
