/*WAP to take a value from the user in main function and print that value in another Function. */
#include<stdio.h>

void fun(int *ptr){

	printf("Value of ptr in fun function is %d\n",*ptr);
}

void main(){

	int x;
	printf("Enter number\n");
	scanf("%d",&x);
	fun(&x);
}
