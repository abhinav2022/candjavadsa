
#include<stdio.h>

void main(){

	int size;

	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr1[size];

	printf("enter elements\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",arr1+i);
	}

	printf("Elements are :-\n");
	for(int i=0;i<size;i++){
	
		printf("arr1[%d]=%d \n",i,*(arr1+i));
	}
	
	int row1,col1;

	printf("Enter row size\n");
	scanf("%d",&row1);

	printf("Enter column size\n");
	scanf("%d",&col1);

	int arr2[row1][col1];

	printf("Enter elements\n");
	for(int i=0;i<row1;i++){
	
		for(int j=0;j<col1;j++){
		
			scanf("%d",(*(arr2+i)+j));
		}
	}

	printf("elements are:-\n");

	for(int i=0;i<row1;i++){
	
		for(int j=0;j<col1;j++){
		
			printf("arr2[%d][%d] = %d \n",i,j,*(*(arr2+i)+j));
		}
	}


	int pl,row2,col2;

	printf("Enter plane size\n");
	scanf("%d",&pl);
	
	printf("Enter row size\n");
	scanf("%d",&row2);

	printf("Enter column size\n");
	scanf("%d",&col2);

	int arr3[pl][row2][col2];

	printf("Enter elements\n");
	for(int i=0;i<pl;i++){

		for(int j=0;j<row2;j++){
		
			for(int k=0;k<col2;k++){
			
				scanf("%d",(*(*(arr3+i)+j)+k));
			}
		}
	}
	printf("\nelements are:-\n");
	for(int i=0;i<pl;i++){

		for(int j=0;j<row2;j++){
		
			for(int k=0;k<col2;k++){
			
				printf("arr3[%d][%d][%d] = %d\n",i,j,k,*(*(*(arr3+i)+j)+k));
			}
		}
	}
}
