
#include<stdio.h>

void main(){

	int size;
	printf("Enter size\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter elements\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",arr+i);
	}
	
	printf("Elements are :-\n");

	for(int i=0;i<size;i++){
	
		for(int j=1;j<size;j++){
		
			if(*(arr+j-1)>*(arr+j)){
			
				int temp= *(arr+j-1);
				*(arr+j-1)=*(arr+j);
				*(arr+j)=temp;
			}
		}
	}

	for(int i=0;i<size;i++){
	
		printf("%d\n",*(arr+i));
	}
}
