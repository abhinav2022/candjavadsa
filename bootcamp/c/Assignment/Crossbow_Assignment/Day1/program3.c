
#include<stdio.h>

void main(){

	int plane,row,col,sum=0;
	printf("Enter plane size\n");
	scanf("%d",&plane);
	printf("Enter row size\n");
	scanf("%d",&row);
	printf("Enter column size\n");
	scanf("%d",&col);

	int arr[plane][row][col];

	printf("Enter elements in array\n");
	for(int i=0;i<plane;i++){
	
		for(int j=0;j<row;j++){
		
			for(int k=0;k<col;k++){
			
				scanf("%d",(*(*(arr+i)+j)+k));

				if(k+j==row-1){  //we can take column also if row == col
				
					sum+=*(*(*(arr+i)+j)+k);
				}
			}
		}
	}
	printf("Elements are \n");

	for(int i=0;i<plane;i++){
	
		for(int j=0;j<row;j++){
		
			for(int k=0;k<col;k++){
			
				printf("arr[%d][%d][%d]= %d\n",i,j,k,*(*(*(arr+i)+j)+k));
			}
			printf("\n");
		}
		printf("\n");
	}
	printf("%d \n",sum);

}
