
#include<stdio.h>

void main(){

	int size;
	printf("Enter size of array\n");
	scanf("%d",&size); // size for array
	if(size<0){
	
		printf("Entered wrong input \n"); // error size handling
		return;
	}

	int arr[size];

	printf("Enter elements\n");
	for(int i=0;i<size;i++){
	
		scanf("%d",arr+i);
	}

	printf("Elements are:-\n");

	for(int i=0;i<size;i++){
	
		printf("arr[%d]= %d\n",i,*(arr+i));
	}


}
