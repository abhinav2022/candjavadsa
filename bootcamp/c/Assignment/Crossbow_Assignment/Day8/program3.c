
#include<stdio.h>
static int h = 10; // it gets memory on data section but it is not accessible from outside of memory.
void main(){

	static int y = 20; // it gets separate memory in data section ,which is not globally accessible by other function.

	printf("%d\n",y);
	printf("%d\n",h);
}
