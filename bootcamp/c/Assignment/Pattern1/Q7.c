/*7. If possible take no of rows from the user
1 2 9 4
25 6 49 8
81 10 121 12
169 14 225 16

*/

#include<stdio.h>

void main(){

	int ip1,j=4,p=1;

        printf("Enter number of rows\n");
        scanf("%d",&ip1);
        
        for(int i=1;i<=ip1;){

                if(j>=1 && p%2!=0){

                        printf("%d ",p*p);
                        j--;
                        p++;

                }else if(j>=1 && p%2==0){

                        printf("%d ",p);
                        j--;
                        p++;

                }else{

                        printf("\n");
                        i++;
                        j=4;
                }
        }
}
