/*7. WAP to calculate the LCM of given two numbers*/
#include<stdio.h>

void main(){

	int x,y,pro=1;
	printf("Enter value of x\n");
	scanf("%d",&x);

	printf("Enter value of y\n");
	scanf("%d",&y);
	if(x<0 || y<0){
	
		printf("Invalid input\n");
		return;
	
	}else{
	
		if(x<y){
		
			for(int i=1;i<=x;i++){
			
				if(x%i==0 && y%i==0){
				
					pro=i;
				}
			}

			printf("lcm of %d and %d is %d \n",x,y,((x*y)/pro));
		
		}else{
		
			for(int i=1;i<=y;i++){
			
				if(x%i==0 && y%i==0){
				
					pro=i;
				}
			}

			printf("lcm of %d and %d is %d \n",y,x,((x*y)/pro));
		
		}
	}
}
