/*6. WAP to calculate the factorial of a given number*/

#include<stdio.h>

void main(){

	int ip1,pro=1;

	printf("Enter number \n");
	scanf("%d",&ip1);

	if(ip1<0){
	
		printf("Invalid input\n");
		return;

	}else{
	
		for(int i=ip1;i>=1;i--){
		
			pro*=i;
		}
		
		printf("factorial of %d is %d \n",ip1,pro);
	}
}
