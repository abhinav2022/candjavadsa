/*3. WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately. Within a range */

#include<stdio.h>

void main(){

	int x,n=0,l;
	
	printf("Enter value of x\n");
	scanf("%d",&x);

	printf("Enter value of l\n");
	scanf("%d",&l);

	if(x<0 || l<0){
	
		printf("Invalid input \n");
		return ;
	}

	else{
		
		if(x<l){
		
			for(int i=l;i>=x;){
			
				if(n==0){
				
					if(i%2==0){
					
						printf("%d ",i);

					}if(i<=x){
						n=1;
						i=l;
						printf("\n");

					}if(n==0){
					
						i--;
					}

				}else{
					if(x%2!=0){
						printf("%d ",x);
					}
						x++;
				}
			}
			printf("\n");
		}else{
			for(int i=x;i>=l;){
			
				if(n==0){
				
					if(i%2==0){
					
						printf("%d ",i);

					}if(i<=l){
						n=1;
						i=x;
						printf("\n");

					}if(n==0){
					
						i--;
					}
				}else{
				
					if(l%2!=0){
					
						printf("%d ",l);
					}
						l++;
				}
			}
			printf("\n");
			
		}
		
	}
}
