/*4. WAP to Find the sum of numbers that are divisible by 5 in the given range*/
#include<stdio.h>
void main(){

	int ip1,ip2,sum=0;
	printf("Enter input 1\n");
	scanf("%d",&ip1);

	printf("Enter input 2\n");
	scanf("%d",&ip2);

	if(ip1<0 || ip2<0){
	
		printf("Invalid input\n");
		return;
	}else{
	
		if(ip1<ip2){
		
			for(int i=ip1;i<=ip2;i++){
			
				if(i%5==0){
				
					sum+=i;
				}

			}
			printf("Sum of numbers divisible by 5 is %d\n",sum);

		}else{
			for(int i=ip2;i<=ip1;i++){
			
				if(i%5==0){
				
					sum+=i;
				}
			}
			printf("Sum of numbers divisible by 5 is %d\n",sum);

		}
	}
}
