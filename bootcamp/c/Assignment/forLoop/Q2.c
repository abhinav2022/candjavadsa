/*2. WAP to print the character whose ASCII is even*/
#include <stdio.h>

void main(){

	for(int i=0;i<128;i++){
		
		if(i%2==0){
		
			printf("Value of character at ascii number %d is %c \n",i,i);

		}
	}
}
