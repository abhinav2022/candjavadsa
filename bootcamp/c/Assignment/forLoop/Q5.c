/*5. WAP to take the Number input and print all the factors of that number*/

#include<stdio.h>

void main(){

	int ip1;

	printf("Enter number \n");
	scanf("%d",&ip1);
	
	if(ip1>=0){
	
		if(ip1==0){
		
			printf(" 0 ");
			return;
		
		}else{
		
			for(int i=1;i<=ip1;i++){
			
				if(ip1%i==0){
				
					printf("%d is factor of %d \n",i,ip1);
				}
			}


		}
	}
}
