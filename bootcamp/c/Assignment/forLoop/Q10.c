/*
10. WAP to print the numbers in a given range and their multiplicative inverse.
Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
The expected output for range 1 - 5
1 = 1
2 = 1/2 i.e 0.5
3 = 1/3 i.e 0.33
4 = 0.25
5 = 0.20
*/
#include<stdio.h>
void main(){

	int ip1,ip2;

	printf("Enter value of ip1\n");
	scanf("%d",&ip1);
	
	printf("Enter value of ip2\n");
	scanf("%d",&ip2);

	if(ip1<=0 || ip2<=0){
	
		printf("Invalid input\n");
	
	}else{
		
		if(ip1<=ip2){
		
			for(int i=ip1;i<=ip2;i++){
			
				printf("%d inverse is %.3lf \n",i,(double)(1/(double)i));
			}
		}else{
		
			for(int i=ip2;i<=ip1;i++){
			
				printf("%d inverse is %.3lf \n",i,(double)(1/(double)i));
			}

		}
	}
}
