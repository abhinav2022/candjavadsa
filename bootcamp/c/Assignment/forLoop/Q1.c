/*1. WAP a program to see a given number is a multiple of 3*/
#include<stdio.h>

void main(){

	int x;
	
	printf("Enter number\n");
	scanf("%d",&x);

	if(x%3==0){
	
		printf("%d is divisible by 3 \n",x);
		
	}else{
	
		printf("%d is not divisible by 3 \n",x);
	
	}

}
