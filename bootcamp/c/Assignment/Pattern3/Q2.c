/*2. take no of rows from the user
3 b 1 d
a 2 c 0
3 b 1 d
a 2 c 0*/
#include<stdio.h>

void main(){

	int row;

	printf("Enter row\n");
	scanf("%d",&row);

	for(int i=1;i<=row;i++){
		int k=3;
		char ch='a';
	
		for(int j=1;j<=row;j++){
		
			if((i+j)%2==0){
			
				printf("%d ",k);
			}else{
			
				printf("%c ",ch);
			}
			k--;
			ch++;
		}
		printf("\n");
	}
}
