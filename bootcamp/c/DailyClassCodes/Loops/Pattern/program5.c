
#include<stdio.h>

void main(){

	int row;

	scanf("%d",&row);

	for(int i=1;i<=row;i++){
	
		char ch = 'A';

		for(int j=1;j<=row-i;j++){
		
			printf("_ ");
		}
		for(int j=1;j<=i;j++){
		
			printf("%c ",ch);
			ch++;
		}
		printf("\n");
	}
}
