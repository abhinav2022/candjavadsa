
#include<stdio.h>

struct movie{

	char name[20];
	float imDb_rate;
	char rate[3];
	float time;
}obj2={"Kabir Singh",7.2,"A",2.57};

struct Requirements{

	char game[20];
	char processor[30];
	char Os[15];
	char Gpu[30];
	float gpuvram;
	int dram;
	float storage;
	
}ob1={"COD MW 2019","intel core i5-9300HF","Windows 10","Nvidia Geforce GTX 1650",4,8,150};

void main(){

	struct movie obj1={"KGF",8.8,"U/A",3.02};

	struct Requirements ob2={"COD MW2 2022","intel core i5","Windows 10","Nvidia Geforce GTX 1650",4,8,150};

	printf("For movies\n");

	printf("%s\n",obj1.name);
	printf("%f\n",obj1.imDb_rate);
	printf("%s\n",obj1.rate);
	printf("%fhrs\n",obj1.time);

	printf("\n%s\n",obj2.name);
	printf("%f\n",obj2.imDb_rate);
	printf("%s\n",obj2.rate);
	printf("%fhrs\n",obj2.time);

	printf("\n\nFor Games\n");

	printf("%s\n",ob1.game);
	printf("%s\n",ob1.processor);
	printf("%s\n",ob1.Os);
	printf("%s\n",ob1.Gpu);
	printf("%fgb\n",ob1.gpuvram);
	printf("%dgb\n",ob1.dram);
	printf("%fgb\n",ob1.storage);

	printf("\n%s\n",ob2.game);
	printf("%s\n",ob2.processor);
	printf("%s\n",ob2.Os);
	printf("%s\n",ob2.Gpu);
	printf("%fgb\n",ob2.gpuvram);
	printf("%dgb\n",ob2.dram);
	printf("%fgb\n",ob2.storage);
}
