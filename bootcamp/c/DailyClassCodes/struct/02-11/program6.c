
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Society{

	char sName[20];
	int wings;
	double price;
};

void main(){

	struct Society *ptr = (struct Society *)malloc(sizeof(struct Society));

	strcpy((*ptr).sName,"Tata");
	ptr->wings=8;

	(*ptr).price=10.00;

	printf("%s\n",ptr->sName);
	printf("%d\n",(*ptr).wings);
	printf("%lf\n",(*ptr).price);
}
