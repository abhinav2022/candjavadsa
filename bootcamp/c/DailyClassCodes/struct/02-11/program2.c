
#include<stdio.h>

char *mystrcpy(char* dest,char* src){

	char *temp=dest;
	while(*src!='\0'){
	
		*dest= *src;
		dest++,src++;
	}
	*dest='\0';
	return temp;
}

struct Picnic{

	char location[10];
	int count;
	float distance;
	// initialization inside structure gives error 
};

void main(){

	struct Picnic obj1={"Goa",5,460.5};

	struct Picnic obj2;

	//obj2.location = "Kashmir";
	mystrcpy(obj2.location,"Kashmir");
	obj2.count=2;
	obj2.distance=1200.5;
	
	printf("%s\n",obj1.location);
	printf("%d\n",obj1.count);
	printf("%f\n",obj1.distance);
	printf("\n%s\n",obj2.location);
	printf("%d\n",obj2.count);
	printf("%f\n",obj2.distance);
}
