//Call by address call by value
#include<stdio.h>

struct Demo{

	int x;
	float f;
};

void fun(struct Demo obj){

	printf("%d\n",obj.x);
	printf("%f\n",obj.f);
	obj.f=34.56;
	printf("%f\n",obj.f);
}
void gun(struct Demo *ptr){

	printf("%d\n",ptr->x);
	printf("%f\n",(*ptr).f);
	(*ptr).f=20.4f;
}
void main(){

	struct Demo obj = {10,20.5f};

	fun(obj);
	printf("%f\n",obj.f);
	gun(&obj);
	printf("%f\n",obj.f);
}
