
int printf(const char *p,...);

int scanf(const char *p,...);

void main(){

	int i=23.4;
	printf("%3.6d\n",i);

	float f = 14.45f;
	printf("%8.8f\n",f);

	double d = 107.65f;
	printf("f %f\n",d);
	printf("lf %lf\n",d);
	printf(".5 %.5lf\n",d);
	printf(".6 %.6lf\n",d);
	printf("%.7lf\n",d);
	double e = 105.65;
	printf("f %f\n",e);
	printf("lf %lf\n",e);
	printf("%.5lf\n",e);
	printf("%.6lf\n",e);
	printf("%.7lf\n",e);
}
