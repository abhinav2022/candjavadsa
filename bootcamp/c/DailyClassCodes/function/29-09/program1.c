
#include<stdio.h>

int diag1(int (*ptr)[3],int row,int col){

	int sum = 0;

	for(int i=0;i<row;i++){
	
		for(int j = 0;j<col;j++){
			
		if(i==j)	
			sum+=*(*(ptr+i)+j);
			
		}
	}
	return sum;
}

int diag2(int (*ptr)[] ,int size){

	int sum =0;

	for(int i=0;i<size;i+=4){
		
		sum+=*(*ptr+i);
	}

	return sum;
}

void main(){

	int arr[][3]={1,2,3,4,5,6,7,8,9,10,11,12};
	printf("%d\n",diag1(arr,3,3));
	printf("%d\n",diag2(arr,9));
}
