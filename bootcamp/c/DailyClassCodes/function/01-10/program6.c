
#include<stdio.h>

void add(int x,int y){

	printf("%d\n",x+y);
}

void sub(int x,int y){

	printf("%d\n",x-y);
}

void mul(int x,int y){

	printf("%d\n",x*y);
}

void div(int x,int y){

	printf("%d\n",x/y);
}

void main(){

	void (*ptr[])(int,int)={add,sub,mul,div};

	for(int i=0;i<sizeof(ptr)/sizeof(void *);i++){
	
		ptr[i](20,10);
	}
}
