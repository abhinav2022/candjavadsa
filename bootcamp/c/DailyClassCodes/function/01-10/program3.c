
#include<stdio.h>

void sub(int x,int y){
	printf("In sub\n");
	printf("%d\n",x-y);
	printf("%d\n",x-y);
}

int add(int x,int y,void (*p)(int,int)){

	printf("%d\n",x+y);
	printf("%d\n",x+y);
	printf("%d\n",x+y);
	p(2,1);
}

void main(){

	int x;
	int (*ptr1)(int,int,void (*)(int,int));
	int (*ptr2)(int x,int y);
	ptr1 = &add;
	
	printf("&add %p\n",ptr1);
	ptr1(1,2,sub);
	ptr1++;
	printf("&add %p\n",ptr1);
	ptr1(5,6,sub);
	ptr1++;
	printf("&add %p\n",ptr1);
	ptr1(9,10,sub);
	//ptr(10,20);
	//ptr(20,30);
}
