
#include<stdio.h>

void main(){

	int arr1[5]={1,2,3,4,5};
	int arr2[5]={6,7,8,9,10};

	int (*ptr[])[5]={&arr1,&arr2};

	printf("%p\n",arr1);
	printf("%p\n",&arr1+1);

	printf("%d\n",*(*(*(ptr+0)+2)));
	printf("%d\n",*(ptr[0][2]));
}
