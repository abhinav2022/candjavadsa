
#include<stdio.h>

int *ptr;

void gun(){

	int x = 20;
	printf("In gun\n");
	printf("%d\nEnd gun\n",x);
}

void fun(){

	int x = 30;
	ptr = &x;

	printf("In fun\n");
	printf("%d\n",*ptr);
}

void main(){

	int x = 10;
	printf("In main\n");
	printf("%d\n",x);

	fun();
	gun();
	printf("%d\n",*ptr);
}
