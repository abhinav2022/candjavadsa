
#include<stdio.h>

void main(){

	char carr[]={'A','B','C','D'};

	char *cptr1 = &carr[2];
	char *cptr2 = &carr[3];

	printf("%c\n",*cptr1);
	printf("%c\n\n",*cptr2);
	printf("%p\n",(cptr1 + 2));
	printf("%p\n\n",(cptr2 + 1));
	printf("%p\n%p\ncptr1 %p\n cptr2 %p\n",cptr1,cptr2,&cptr1,&cptr2);
}
