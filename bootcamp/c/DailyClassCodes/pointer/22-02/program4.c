
#include<stdio.h>

void main(){

	char ch1[3]={'A','B','C'};
	char ch2[3]={'D','E','F'};
	char ch3[3]={'D','E','F'};
	char (ch4[3])={'D','E','F'};
	char (*pt2)[3]=&ch1;
	char (*pt3)[3] =&ch2;

	char ((*ptr[2])[3])={&ch1,&ch2};
	
	char (**l[2])[3]={&pt2,&pt3};

	printf("%c\n",**(*(*l)));
	//printf("%c\n",*(*(*(ptr))+1));
	
}
