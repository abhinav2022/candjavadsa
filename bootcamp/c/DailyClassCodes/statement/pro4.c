
#include<stdio.h>

void main(){

	char ch1 = 'A'; 
	char ch2 = 48;
	printf("Start main\n");

	if(ch1){
		printf("In first if block\n");
	}
	if(ch2){
		printf("In second block\n");
	}
	printf("End main\n");
	printf("ch2 c %c d %d \n",ch2,ch2);
}
