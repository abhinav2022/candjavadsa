
#include<stdio.h>

void main(){

	int a,b,c;

	printf("Enter value of 1st side\n");
	scanf("%d",&a);

	printf("Enter value of 2nd side \n");
	scanf("%d",&b);

	printf("Enter value of hypotenuse side \n");
	scanf("%d",&c);

	if(a<=0 || b<=0 || c<=0){
	
		if(a<=0){
			printf("%d is wrong input of side a",a);
		}
		if(b<=0){
			printf("%d is wrong input of side b",b);
		}
		if(c<=0){
			printf("%d is wrong input of side c ",c);
		}
	}
	else{
	
		if(c*c==a*a + b*b ){
		
			printf("Sides are pythagorean triplet \n");
		}else{
		
			printf("Sides are not pythagorean triplet\n");
		}
	}
}
