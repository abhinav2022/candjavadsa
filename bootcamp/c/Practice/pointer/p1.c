
#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int*)malloc(sizeof(int));
	*ptr = 3;
	printf("%d\n",*ptr);
	printf("%p\n",ptr);
	free(ptr);
	printf("%d\n",*ptr);
	*ptr = 6;
	printf("%p\n",ptr);
	printf("%d\n",*ptr);
}
