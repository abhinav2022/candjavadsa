
#include<stdio.h>

void main(){

	int row,n=1;
	printf("Enter row\n");
	scanf("%d",&row);

	for(int i=1;i<=row;i++){
	
		n=1;
		for(int j=i;j<row;j++){
		
			printf("\t");
		}
		for(int j=1;j<=i;j++){
		
			if(j%2!=0){
			
				printf("%d\t",n++);
			}else{
			
				printf("%c\t",96+n);
			}
		}
		printf("\n");
	}
}
