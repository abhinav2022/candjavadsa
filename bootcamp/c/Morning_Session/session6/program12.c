
#include<stdio.h>

void main(){

	int row;
	printf("Enter row\n");
	scanf("%d",&row);

	char ch = 'A';
	for(int i=1;i<=row;i++){
	
		int sp=1;
		for(int j=1;j<=row;j++){

			if(sp<i){
			
				printf("\t");
				sp++;
			}else {
			
				if(j%2!=0){
				
					printf("%c\t",ch);
				}else{
				
					printf("%c\t",ch+32);
				}
				ch++;
			}
		}
		printf("\n");
	}
}
