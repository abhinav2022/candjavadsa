
#include<stdio.h>

void main(){

	int row;
	printf("Enter row\n");
	scanf("%d",&row);

	char ch = 64+row;
	for(int i=1;i<=row;i++){
	
		for(int sp=i;sp<row;sp++){
		
			printf("\t");
		}
		for(int j=1;j<=2*i-1;j++){
		
			if(i>j){
		
				printf("%c\t",ch);
				if(ch>64 && ch<91){
				
					ch+=32;
				}else{
					ch-=32;
				}
				ch++;
			
			}else{
			
				printf("%c\t",ch);		
				if(ch>64 && ch<91){
				
					ch+=32;
				}else{
					ch-=32;
				}
				ch--;			
			}
		}
		printf("\n");
	}
}
