
#include<stdio.h>

void main(){

	int n,p=1;
	printf("Enter row\n");
	scanf("%d",&n);

	for(int i=0;i<n;i++){
	
		for(int j=i;j<n-1;j++){
		
			printf("\t");
		}
		for(int j=1;j<=2*i+1;j++){
		
			if(i+1>j){
			
				printf("%d\t",(n-i)*p++);
			}else{
			
				printf("%d\t",(n-i)*p--);
				if(j==2*i+1)
					p++;
			}
		}
		printf("\n");
	}
}
