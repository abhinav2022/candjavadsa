
#include<stdio.h>

void main(){

	int row;
	printf("Enter row\n");
	scanf("%d",&row);

	int num=1;
	for(int i=1;i<=row;i++){
	
		for(int j=i;j<row;j++){
		
			printf("\t");
		}
		for(int j=1;j<=2*i-1;j++){
		
			if(j<i)
			printf("%d\t",num++);

			else{
				printf("%d\t",num);
				num--;
			}
		}
		printf("\n");
		num=1;
	}
}
