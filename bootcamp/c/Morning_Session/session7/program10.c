
#include<stdio.h>

void main(){

	int row;
	printf("Enter rows\n");
	scanf("%d",&row);

	char ch = 'A';

	for(int i=1;i<=row;i++){
	
		for(int j=i;j<row;j++){
		
			printf("\t");
		}
		for(int j=1;j<=2*i-1;j++){
		
			if(i%2==0){

			
				if(i>j){
					printf("%c\t",ch+32);
					ch--;
				}else{
				
					printf("%c\t",ch+32);
					ch++;
					if(j==2*i-1)
						ch--;
				}
			}else{
			
				if(i>j){
					printf("%c\t",ch);
					ch+=2;
				}else{
				
					printf("%c\t",ch);
					ch-=2;
					if(j==2*i-1)
						ch+=2;
				}
			}
			
		}
		printf("\n");
		ch++;
	}
}
