
#include<stdio.h>

void main(){

	char ch ;
	printf("Enter character \n");
	scanf("%c",&ch);

	if((ch>=0 && ch<48) || (ch>90 && ch<97) || (ch>122 && ch<128)){
	
		printf("'%c' is special character \n",ch);

	}else if(ch>47 &&ch <58){
	
		printf("'%c' is a digit\n",ch);

	}else if(ch>64 && ch<=90){
	
		printf("'%c' is a capital alphabet\n",ch);

	}else if(ch>96 && ch<=122){
	
		printf("'%c' is a small alphabet\n",ch);

	}else{
	
		printf("'%c' is something else\n",ch);
	}
	
}
