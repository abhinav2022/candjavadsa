
#include<stdio.h>

void main(){

	int row;
	printf("Enter row\n");
	scanf("%d",&row);

	for(int i=1;i<=row;i++){
	
		for(int j=1;j<=row-i;j++){
		
			printf("\t");
		}
		for(int j=1;j<=i;j++){
		
			if(i%2!=0){
			
				printf("%d\t",j);
			}else{
			
				if(j%2!=0){
				
					printf("%c\t",64+j);
				}else{
				
					printf("%c\t",96+j);
				}
			}
		}
			printf("\n");
	}
}
