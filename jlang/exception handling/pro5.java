import java.io.*;

class MyExcept extends Exception{
	String msg;

	MyExcept(String str){
		msg = str;
	//	super(str);
	}
	public String toString(){
		
		return msg;
	}
}
class Demo{
	
	void m1() throws MyExcept{
		
		for(int i = 5;i>0;i--){
			System.out.println(i);
			if(i==2)
				throw new MyExcept("Vedya divide by 2");
		}
	}
	public static void main(String[] ar) throws MyExcept{
		Demo ob = new Demo();
		System.out.println(System.out);
		ob.m1();
	}
}
