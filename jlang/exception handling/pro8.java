
interface A{

	static void m1(){
		System.out.println("A In m1");
	}
}
interface B extends A{
	static void m1(){
		System.out.println("B In m1");
	}
}
class C implements A,B{

	public static void main(String[] ar){
		A ob = new C();
		A.m1();
	}
}
