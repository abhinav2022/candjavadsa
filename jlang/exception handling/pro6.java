
import java.io.*;

class MyExcept extends Exception{
	
	String msg;
	public MyExcept(String str){
		msg = str;
	//	System.out.println(msg);
	}

	public String toString(){
		System.out.println("Vedya tostring and out of balance");
		return msg;
	}
}

class DineOut{
	
	static int amt = 150; 

	static void KhauGalli() throws MyExcept{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nIn KhauGalli ");
		System.out.println("/********** Menu Card ************/");
		System.out.println("1. Pav Bhaji ₹60 \n2. Pani-Puri ₹20 \n3. Ragda Pattis ₹30 \n");
		try{
			System.out.println("choose what to eat");
		int c = Integer.parseInt(br.readLine());
		//br.skip(1);
		switch(c){
		
			case 1:
				if(amt<60){
					throw new MyExcept("Not having enough money for pavbhaji");
				}
				System.out.println("having pavbhaji");
				amt-=60;
				System.out.println("cash balance is "+amt);
				KhauGalli();
				break;
			case 2:
				if(amt<20){
					throw new MyExcept("Not having enough money for pani-puri");
				}
				System.out.println("pani puri");
				amt-=20;
				System.out.println("cash balance is "+amt);
				KhauGalli();
				break;
			case 3:
				if(amt<30){
					throw new MyExcept("Not having enough money for ragda pattis");
				}
				System.out.println("ragda pattis");
				amt-=30;
				System.out.println("cash balance is "+amt);
				KhauGalli();
				break;
			
			default:
				System.out.println("Returning to home \n\nreturn 0; ");
				break;	
		
		}
		}
		catch(NumberFormatException e){
			System.out.println("Error input");
		}
		catch(IOException e){
			System.out.println("Error input");
		}
	}
       public static void main(String[] ar) throws MyExcept{
       	
	       KhauGalli();
       }	
}
