
import java.io.*;

class FinallyDem{
	
	public static void main(String[] ar){
	
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		try{
			br1.close();
			br1.readLine();
		}
		catch(IOException e){
			System.out.println("Handling Code");
		}
		finally{
			try{
				br2.close();
				br2.readLine();
			}
			catch(IOException ie){
				System.out.println("Catch finally");
			}
			System.out.println("in finally");
		}
	}
}
