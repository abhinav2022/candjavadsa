
class Parent{
	void p1(){
		System.out.println("in p1");
	}
}
class Child extends Parent{
	void p2(){
		System.out.println("in p2");
	}
}
class Demo{
	
	void m1(Parent p){
		System.out.println("In m1 - parent");
	}

	void m1(Child p){
		System.out.println("m1 Child");
		p.p1();
		p.p2();
	}

	public static void main(String[] ar){
		Demo d = new Demo();
		//d.m1(new Child());
		Child c = new Child();
		d.m1(c);
	}
}
