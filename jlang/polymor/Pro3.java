
class Parent{}

class Child extends Parent{
}

class Demo{
	void m1(Parent p){
		System.out.println("m1 parent");
	}
	void m1(Child c){
		System.out.println("m1 Child");
	}

	public static void main(String[] ara){
		Demo d = new Demo();
		d.m1(new Child());
	}
}
