
class YieldDemo extends Thread{

	public void run(){
	
		for(int i=0;i<20;i++){
		
			System.out.println("In run");
			try{
			
				Thread.sleep(1000);
			}
			catch(InterruptedException ie){
			}
			if(i%5==0)
				yield();
		}
	}
}

class YD{

	public static void main(String[] ar) throws InterruptedException{
	
		YieldDemo ob = new YieldDemo();
		ob.start();

		for(int i =0 ;i<10;i++){
		
			System.out.println("in main");
			if(i%5==0)
				ob.yield();
		}
	}
}
