
class ThreadDemo extends Thread{

	ThreadDemo(){
		super();
	}

	ThreadDemo(String a){
	
		super(a);
	}
	public void run(){
	
		System.out.println("In run");
	}
	ThreadDemo(ThreadGroup tg, String st){
		super(tg,st);
	}
}

class ThreadGroupDemo{

	public static void main(String[] ar){
	
		ThreadDemo t1 = new ThreadDemo();
		t1.start();
		System.out.println(t1.getThreadGroup());

		ThreadGroup tg1 = new ThreadGroup("C2W group ");

		ThreadDemo t2 = new ThreadDemo(tg1,"Abhi");
		System.out.println(t2.getThreadGroup());
		System.out.println(t1.activeCount());

	}
}
