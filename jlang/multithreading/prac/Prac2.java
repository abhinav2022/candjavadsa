
class ThreadDemo1 extends Thread{

	ThreadDemo1(){
		super();
	}

	ThreadDemo1(String a){
	
		super(a);
	}
	public void run(){
		try{	
		System.out.println("In run");
		Thread.sleep(1000);
		}
		catch(InterruptedException E){
		
			System.out.println("Destroyed  "+ E);
		}
	}
	ThreadDemo1(ThreadGroup tg, String st){
		super(tg,st);
	}
}

class ThreadGroupDemo1{

	public static void main(String[] ar){
	
		ThreadDemo1 t1 = new ThreadDemo1();
		t1.start();
		System.out.println(t1.getThreadGroup());

		ThreadGroup tg1 = new ThreadGroup("C2W group ");

		ThreadDemo1 t2 = new ThreadDemo1(tg1,"Abhi");
		ThreadDemo1 t3 = new ThreadDemo1(tg1,"Abhi");
		ThreadDemo1 t4 = new ThreadDemo1(tg1,"Abhi");
		System.out.println(t2.getThreadGroup());
		System.out.println(t1.currentThread().activeCount());
		t3.start();
		t4.start();
		tg1.interrupt();

	}
}
