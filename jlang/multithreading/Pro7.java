
class MyThread extends Thread{
	
	public void run(){
		System.out.println("In run");

		System.out.println(Thread.currentThread().getName());
		//fun();
		MyThread t2 = new MyThread();
		t2.start();
	}

	void fun(){
		System.out.println("In fun");
	}

	public static void main(String[] ar){
		System.out.println(Thread.currentThread().getName());
		MyThread t1 =  new MyThread();
		//Thread t = new Thread(t1);
		//t.start();
		t1.start();

		System.out.println(Thread.currentThread().getName());
	}
}
