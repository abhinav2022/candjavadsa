
class Par{
	void m1(){
		System.out.println("In m1");
	}

}

class Child extends Par{
	void m2(){
		System.out.println("In m2");
	}
}

class Demo{
	
	public static void main(String[] ar){
		Par p = new Par(){
			void m1(){
				System.out.println("In m3");
			}
		};

		Child c =new Child(){
			void m4(){
				System.out.println("In m4");
			}
		};

	}
}
