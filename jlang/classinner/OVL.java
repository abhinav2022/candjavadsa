
class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void p1(){
	
		System.out.println("In p1");
	}
}

class Child extends Parent{
	Child(){
		System.out.println("Child construc");
	}
	void p2(){
		System.out.println("In p2");
	}
}

class Demo{
	Demo(){
		System.out.println("In demo constructor");
	}
	void m1(Parent p){
		System.out.println("In m1 parent");
	}

	void m1(Child p){
		System.out.println("m1 child");
		p.p1();
		p.p2();
	}
	public static void main(String[] ar){
		Demo d = new Demo();
		d.m1(new Child());
}
}
