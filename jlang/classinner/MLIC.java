

class Outer{

	int x = 10;
	static int y = 20;

	void m1(){
		class Inner{
			void m2(){
				System.out.println("In m2");
			}
		}
		System.out.println("In m1");

		Inner i = new Inner();
		i.m2();
	}
	public static void main(String[] ar){
		Outer o = new Outer();
		o.m1();
	}
}
