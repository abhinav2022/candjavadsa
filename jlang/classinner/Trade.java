class Military_Equipments_Trade_India extends Trade{
	static {

		System.out.println("Information about Military Equipments Deals");

	}
	Military_Equipments_Trade_India(){
		
	Information();	
	
	}

	
	static class Ammunition_Deal_with_Russia{
		int ak_series_rifle = 20000;
		int MI_17_chopper = 223;
		void Armory(){
			System.out.println("From Russia India have purchased ");
			System.out.println(ak_series_rifle+" AK Series Rifle");
			System.out.println(MI_17_chopper+" MI-17 choppers");
			System.out.println();
		}
	}
	static class Ammunition_Deal_with_Israel{
		 int tar21 = 2000;
		int Heron_Drones=4;
		int M4_series = 1000;
		void Armory(){
			System.out.println("From Israel India have purchased ");
			System.out.println(tar21+" Tar-21 rifles");
			System.out.println(M4_series+" M4 series rifle");
			System.out.println(Heron_Drones+" Heron Drones");
			System.out.println();
		}
		
	}
	static class Ammunition_Deal_with_USA{
		int Apache = 22;
		int Chinook = 15;
	int Predator_Drones=30;
		void Armory(){
			System.out.println("From USA India have purchased ");
			System.out.println(Apache+" Apache Helicopters");
			System.out.println(Chinook+" Chinook CH-47");
			System.out.println(Predator_Drones+" Predator Drones");
			System.out.println();
		}
	}
	static class Ammunition_Deal_with_France{
		int Rafael_Jets=30;

		void Armory(){
			System.out.println("From France India have purchased ");
			System.out.println(Rafael_Jets+" Rafael Jets");
			System.out.println();
		}
		
	}
	void Information(){	
		
		Ammunition_Deal_with_France F = new Ammunition_Deal_with_France();
		F.Armory();
		Ammunition_Deal_with_USA U = new Ammunition_Deal_with_USA();
		U.Armory();
		Ammunition_Deal_with_Russia R = new Ammunition_Deal_with_Russia();
		R.Armory();
		Ammunition_Deal_with_Israel I = new Ammunition_Deal_with_Israel();		
		I.Armory();
	}
}

class Oil_Trade extends Trade{
	static {
		System.out.println("Information about crude oil deal from other countries");
	}
	Oil_Trade(){
		Information();
	}
	static class Iran{
		int stocks = 1000;
		void Oil(){
			System.out.println("Oil barrels bought from Iran is "+stocks);
			System.out.println();
		}
	}
	static class UAE{
		int stocks = 1000;
		void Oil(){
			System.out.println("Oil barrel bought from UAE is "+stocks);
			System.out.println();
		}
	}
	static class Russia{
		int stocks = 10000;
		void Oil(){
			System.out.println("Now we purchased from Russia ");
			System.out.println("Oil barrels bought from Russia is "+stocks);
			System.out.println();
		}
	}
	void Information(){
		Iran Ir = new Iran();
		Ir.Oil();
		UAE u = new UAE();
		u.Oil();
		Russia r = new Russia();
		r.Oil();
	}	
}


class Trade{
	static{
		System.out.println("Independent Trade Policies of India\n");
	}
	//static Trade t = new Trade();
      public static void main(String[] ar){
	
      Military_Equipments_Trade_India M = new Military_Equipments_Trade_India();
	Oil_Trade o = new Oil_Trade();
	System.out.println("\nThese are some examples of international deals which are indepenent of each other\n");
}
}
