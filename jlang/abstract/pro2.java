
class Ajoba{
	private Ajoba(){
		System.out.println("In Ajoba Constructor\n");
	}
}

class Bapu extends Ajoba{
	Bapu(){
		System.out.println("In Bapu constructor");
	}
	void m1(){
		System.out.println("m1 Method");
	}
	public static void main(String[] ar){
		Bapu b = new Bapu();
		b.m1();
	}
}
