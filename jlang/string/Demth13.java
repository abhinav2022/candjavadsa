
class Parent{

	Parent(){
		System.out.println("Parent");
		System.out.println("1 "+this);
	}
}

class Childe extends Parent{

	Childe(){
		
		System.out.println("Childe");
		System.out.println(this);
	}
}
class Child extends Childe{

	Child(){
		
		System.out.println("Child");
		System.out.println(this);
	}
}

class Main{
	public static void main(String[]ar){
		Parent p = new Parent();
		System.out.println(System.identityHashCode(p));
		Child c = new Child();
		System.out.println(System.identityHashCode(c));
	}
}
