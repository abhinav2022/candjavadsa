
class StringMeth{

	public static void main(String[] ar){
		//1 concat
		String str1 = "Shashi";
		String str2 = "Shashi";

		//System.out.println(str1.concat(str2));

		//2 compareToIgnore()
			System.out.println(str1.compareToIgnoreCase(str2));
		//3 lenght()
			System.out.println(str1.length());	
		//4 equals()
			System.out.println(str1.equals(str2));
		//6 startsWith()
                     System.out.println(str1.startsWith("Sha"));	
		 // 7 endsWith()
		 	System.out.println(str1.endsWith("i"));   
		// 8 substring
			System.out.println(str1.substring(2));
		//9 getchar
			char arr[] = new char[5];
			String str3 = "Core2WebTech";	
			str3.getChars(3,5,arr,1);
			System.out.println(arr);
			char arr1[] = {'a','b','c'};
			System.out.println(arr1);
			// 10 CharArray
				char[] carr = str1.toCharArray();
				System.out.println(carr);
			//11 trim
			String str4 = "   C2W     ";	
			System.out.println(str4.trim());

			//12 compareTo
			 System.out.println(str1.compareTo(str2));

			 //13 replace
			 String stran = "JavaCode";
			System.out.println(stran.replace('a','p')); 
			//14 toUpperCase
				System.out.println(str1.toUpperCase());
			//15 toLowerCase
				System.out.println(stran.toLowerCase());

	}
}
