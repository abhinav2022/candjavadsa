
class Demth12{

	int x = 10;
	static int y = 10;

	Demth12(){
		System.out.println("In Demo()");
	}

	Demth12(int z){
		System.out.println("In Demo(int)");
	}

	Demth12(Demth12 obj){
	//	this();
		System.out.println("In Demo(Demo)");
		obj.m1();
	}

	void m1(){
		System.out.println(this);
	}

	public static void main(String[] ar){
		Demth12 obj1 = new Demth12();
		obj1.m1();

		Demth12 obj2 = new Demth12(10);
		obj2.m1();

		Demth12 obj3 = new Demth12(obj1);
		obj3.m1();
	}
}
