import java.io.*;

class Stringdemo{
	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		String a1 = br.readLine();
		String a2 = br.readLine();
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a1.toString()==a2.toString());
		System.out.println(a1.equals(a2));
	}
}
