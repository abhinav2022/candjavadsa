
class E1{
	
	int x = 10;
	static int y = 20;
}

class C1 extends E1{

	int z = 30;
	static int p = 40;

	void m1(){
		System.out.println("In m1");
		System.out.println(x);
		System.out.println(z);
		System.out.println(z);
		System.out.println(p);
	}
}

class M1{
	
	public static void main(String[] ar){
	
		C1 ob = new C1();
		ob.m1();
		System.out.println(ob);
	}
}
