
class IntTest{

	public static void main(String[] ar) {
		String str1="Core2web";
		String str2 = new String("Core2web");
		System.out.println("Before");	
		System.out.println(System.identityHashCode(str1));
			System.out.println(System.identityHashCode(str2));
			str2 = str2.intern();
		System.out.println("After");	
			System.out.println(System.identityHashCode(str1));
			System.out.println(System.identityHashCode(str2));

	}
}
