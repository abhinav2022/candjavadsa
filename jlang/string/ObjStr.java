
class ObjStr{
	
	ObjStr(){
		System.out.println("In Constructor ");
	}
	ObjStr(int x ){
		System.out.println("In para constructor");
	}

	public static void main(String[] ar){
		ObjStr ob1 = new ObjStr();
		ObjStr ob2 = new ObjStr(20);
		System.out.println(ob1);
		System.out.println(ob2);
	}
}
