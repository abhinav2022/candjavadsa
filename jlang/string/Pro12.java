
class PrivCon{
	
	//static PrivCon ob = new PrivCon();

	 PrivCon(){
		System.out.println("In private constructor");	
		
	}
	 PrivCon ob = new PrivCon();
	 PrivCon methCre(){
	
		return ob;
	}
}

class Pro12{

	public static void main(String[] ar){
		//PrivCon ret1 = PrivCon.methCre();
	//	char[] arr = {'u','y'};
	//	char[] arr1 = new char[]{'u','y'};
		
		PrivCon ret1 = PrivCon.methCre(); 
		System.out.println(System.identityHashCode(ret1));
	
		PrivCon p = new PrivCon();
		System.out.println("Ret2");
		PrivCon ret2=PrivCon.methCre(); 
		System.out.println(System.identityHashCode(ret2));
	
	//	System.out.println(System.identityHashCode(arr));
	//	System.out.println(System.identityHashCode(arr1));
	}
}
