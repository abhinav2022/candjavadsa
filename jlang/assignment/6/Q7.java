//Q7
import java.io.*;

class SumInArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int[] iarr = new int[a];
		
		System.out.println("Enter input");
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		int sum = 0;
		
		System.out.print("Sum of array number is :");

		for(int i =0;i<a;i++){
			sum = sum + iarr[i];
		}
		System.out.println(sum);
	
}
}
