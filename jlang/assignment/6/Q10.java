//Q10
import java.io.*;

class ArrSort {

	public static void main(String[] ar) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter input");
		int[] iarr = new int[a];
		for(int i = 0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		for(int i = 0;i<a;i++){
			for(int j = 1;j<a;j++){
				if(iarr[j-1]>iarr[j]){
					int temp = iarr[j-1];
					iarr[j-1]=iarr[j];
					iarr[j]=temp;
				}
			}
		}
		for(int i=0;i<a;i++){
			System.out.print(iarr[i]+" ");
		}
	}
}
