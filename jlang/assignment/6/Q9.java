//Q9
import java.io.*;

class StrongArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter array input ");
		int[] iarr = new int[a];

		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println();
		for(int i=0;i<a;i++){
			int b = iarr[i],c=0,mul=1,out=0;
			while(b!=0){
				c=b%10;
				for(int j=1;j<=c;j++){
					mul=mul*j;
				}
				b=b/10;
				out = out + mul;
				mul=1;	
			}

		 if(iarr[i] == out){
		
	 		 System.out.println(out);
		  	
		 }
		
		}
	}
}
