//Q5
import java.io.*;

class MaxInArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int[] iarr = new int[a];
		
		System.out.println("Enter input");
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		int max = 0;
		
			System.out.print("Max number is :");

		for(int i=0;i<a;i++){
			if(iarr[i]>max){
				max=iarr[i];
			}
		}

		System.out.println(max);
	}
}
