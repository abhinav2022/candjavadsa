//Q3
import java.io.*;

class InArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int[] iarr = new int[a];
		
		System.out.println("Enter input");
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Output:");
		for(int i=0;i<a;i++){
			System.out.println(iarr[i]);
		}
	}
}
