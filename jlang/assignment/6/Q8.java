//Q8
import java.io.*;

class FreqInArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int[] iarr = new int[a];
		
		System.out.println("Enter input");
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
				int j=0;
		for(int i =0;i<a;i++){
			int freq = 0;
			
			for( int t=0;t<a;t++){
				if(iarr[t]==j){
					freq++;
				}
			}
			j++;
			
			System.out.println("Frequency of "+ iarr[i] +" is "+ freq);
		}
		
	
}
}
