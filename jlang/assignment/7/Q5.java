//Q5
import java.io.*;

class MergeArr{

	public static void main(String [] ar) throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter array size");
		int b = Integer.parseInt(br.readLine());
		int[] iarr1 = new int[a];
		int[] iarr2 = new int[b];
		int[] iarr3 = new int[b+a];

		System.out.println("Enter input");
		for(int i = 0;i<a;i++){
			iarr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter input");
		for(int i = 0;i<b;i++){
			iarr2[i]=Integer.parseInt(br.readLine());
		}
		int j=0;
		for(int i = 0;i<a+b;i++){
			if(i<a){
				iarr3[i]=iarr1[i];
			}
			else if(i>=a){
				iarr3[i]=iarr2[j];
				j++;
			}
		}
		System.out.println("Merged array : ");
		for(int i=0;i<a+b;i++){
		System.out.print(iarr3[i] + " ");
		}
	System.out.println();
	}
} 
