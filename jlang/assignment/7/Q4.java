//Q4
import java.io.*;

class SumArrOE{

	public static void main(String [] ar) throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int[] iarr = new int[a];
		System.out.println("Enter input");
		for(int i = 0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		int esum=0,osum=0;
		for(int i = 0;i<a;i++){
			if(iarr[i]%2==0){
				esum = esum + iarr[i];
			}
			else{
				osum = osum + iarr[i];
			}
		}
		System.out.println("Sum of even numbers in array is "+esum);
		System.out.println("Sum of odd numbers in array is "+osum);
	
	}
} 
