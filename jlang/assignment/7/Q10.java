//Q10
import java.io.*;

class Trail{

	public static void main(String []ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter input");
		int a = Integer.parseInt(br.readLine());
		int c=a,n=0,b=0,e=0;
		while(c!=0){
			n++;
			c=c/10;
		}	
		System.out.println();
		int[] iarr = new int[n];
		for(int i=n-1;i>=0;i--){
			b=a%10;
			iarr[i]=b;
		       	a=a/10;
		}
		for(int i=n-1;i>=0;i--){
			if(iarr[i]==0){
				e++;
			}
			else{
				break;
			}
		}
		System.out.println("Number of trailing zeros "+e);
	}
} 
