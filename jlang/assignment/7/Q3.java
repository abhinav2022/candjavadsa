import java.io.*;

class EvenOddNO {

	public static void main(String []ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int b = 0,c = 0;
		int[] iarr = new int[a];
		System.out.println("Enter input");
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
		for(int i =0;i<a;i++){
			if(iarr[i]%2==0){
				b++;
			}
			else{
				c++;
			}
		}
		System.out.println("Number of even numbers in array "+ b);
		System.out.println("Number of odd numbers in array "+ c);
	}
} 
