//Q8
import java.io.*;

class ArmStrong{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int a =Integer.parseInt(br.readLine());
		System.out.println("Enter input");
		int[] iarr = new int[a];
		int j=0;
		for(int i=0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}
			
		System.out.println("New array ");
	
		for(int i = 0;i<a;i++){
			int b=iarr[i],c=0,n=0;
			while(b!=0){
				c=b%10;
				n = n+(c*c*c);
				b=b/10;
			}	
				b=0;	
			if(n==iarr[i]){
				System.out.print(iarr[i]+" ");
			}
		}
		System.out.println();	
	}
}
