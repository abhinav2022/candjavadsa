//Q1
import java.io.*;
class StringDem{
	
	public static void main(String[]ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String array size");
		int a = Integer.parseInt(br.readLine());
		String[] arr = new String[a];
		System.out.println("Enter String array");
		
		boolean check = false;

		for(int i = 0;i<a;i++){
			arr[i]=br.readLine();
		}
		System.out.println("enter string you want to search");
		String str = br.readLine();

		for(int i = 0; i<a;i++){
			if(str.equalsIgnoreCase(arr[i])){
				check = true;
				System.out.println("Index is "+i);
				break;
			}
		}

		if(check==false){
			System.out.println("String not found");
		}

	}
}
/*
	OUTPUT:-

abhinov@LAPTOP-ABHINAVK:/mnt/d/workspace/java/assignment/9$ java StringDem
Enter String array size
5
Enter String array
Rahul
Kanha
Badhe
Ashish
Shashi
enter string you want to search
Kanha
Index is 1
abhinov@LAPTOP-ABHINAVK:/mnt/d/workspace/java/assignment/9$ java StringDem
Enter String array size
3
Enter String array
Rahul
Shashi
Badhe
enter string you want to search
Kanha
String not found

*/
