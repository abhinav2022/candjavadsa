import java.io.*;
import java.util.*;

class Sorted{
	
	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter string");
			String str = br.readLine();
			StringTokenizer st = new StringTokenizer(str);
			int a = st.countTokens();
			String[] sarr = new String[a];
			char [] carr = new char[a];
			for(int i = 0;i<a;i++){
				sarr[i]= st.nextToken();
			}
			
			for(int i = 0;i<a;i++){
				for(int j =1;j<a;j++){
					if(sarr[j-1].charAt(0)>sarr[j].charAt(0)){
						String srt = sarr[j-1];
						sarr[j-1]=sarr[j];
						sarr[j]=srt;
					}
				}
			}
			for(int i = 0;i<a;i++){
				for(int j =1;j<a;j++){
					if(sarr[j-1].length()>sarr[j].length()){
						String srt = sarr[j-1];
						sarr[j-1]=sarr[j];
						sarr[j]=srt;
					}
				}
			}
			System.out.println();
			for(int i = 0 ; i<a;i++){
				System.out.print(sarr[i]+" ");
			}
			
	}

}
/*Enter string
Shashi Ashish Kanha Rahul Badhe

Badhe Kanha Rahul Ashish Shashi*/
