//Q4
import java.io.*;

class StringConcat{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int a = Integer.parseInt(br.readLine());
		String[] sarr = new String[a];
		System.out.println("Enter Strings");
		
		for(int i =0;i<a;i++){
			sarr[i]=br.readLine();
		}
		String str = new String(sarr[0]);
		for(int i =1;i<a;i++){
			str += sarr[i];
		}
	
		System.out.println("String is ");
		System.out.print(str);
		System.out.println();
	}
}
