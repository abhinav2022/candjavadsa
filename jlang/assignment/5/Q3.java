//Q3
import java.util.*;

class RangeSum {

	public static void main(String [] ar) {
		
		Scanner ob =new Scanner(System.in);
		System.out.println("Enter lower limit");
		int a = ob.nextInt();
		System.out.println("Enter upper limit");
		int b = ob.nextInt();
		
		int sum =0;

		for(int i=a;i<=b;i++){
			sum += i;
		}
		
		System.out.println("sum = "+sum);

	}
}
