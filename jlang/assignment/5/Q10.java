//Q10
import java.util.*;

class RangeRev {

	public static void main(String [] ar) {
		
		Scanner ob = new Scanner(System.in);
		System.out.println("Enter first number");
		int a = ob.nextInt();
		System.out.println("Enter end number");
		int c = ob.nextInt();
		for(int i=a;i<=c;i++){
		int b =0;
		int n =i;
		int sum = 0;
		while(n!=0){
			b=n%10;
			sum = (sum*10)+b;
			n=n/10;
		}
		System.out.print(sum+" ");
		}
	}
}
