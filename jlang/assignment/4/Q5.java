//Q5

import java.util.*;

class SeqMul {
	
	public static void main(String [] ar) {
		
		Scanner ob = new Scanner(System.in);
		
		System.out.println("Enter rows");
		
		int a = ob.nextInt();
		
		int b = 1;
		
		for(int i = 1; i<=a; i++) {
			
			for(int j = 1; j<=i;j++) {
				
				int c = b;
				
				System.out.print( c*j + " ");		
			}
			b++;
			System.out.println();
		}
	}
}
