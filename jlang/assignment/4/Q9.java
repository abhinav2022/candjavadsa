//Q9

import java.util.*;

class NTP {

	public static void main(String [] ar) {
		
		Scanner ob = new Scanner (System.in);

		System.out.println("Enter number of rows");
		int a = ob.nextInt();
		
		int b = 1;
		int c =b;
		for(int i = 1; i<=a; i++) {
			
			for(int j = 1;j<=i;j++) {
					
				System.out.print(c+" ");
				c++;
			}
			c=b;	
			System.out.println();
			b+=i;
		}
	}
}
