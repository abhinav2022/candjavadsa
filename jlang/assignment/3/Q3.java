//Q3

import java.util.*;

class Mul {

	public static void main(String [] ar) {
		
		Scanner ob = new Scanner(System.in);
		System.out.println("Enter Numbers:");
		int a = ob.nextInt();
		int b = ob.nextInt();

		if(a<0 || b<0) {
			System.out.println("Sorry negative number not allowed ");
		}
		else {
		
			int c = a*b;
			int d = 0;
			
			if(c%2==0) {
				d=1;
			}
			else {
				d=2;
			}

			switch(d){
				
				case 1:
					System.out.println("Even number");
					break;
				case 2:
					System.out.println("Odd number");
					break;
				default:
					System.out.println("Invalid expression");
					break;
			}
		}
	}
}
