import java.io.*;

class Jaggedstar{

	public static void main(String[]ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row size");
		int a = Integer.parseInt(br.readLine());
		char[][] carr = new char[a][];
		for(int i=0;i<a;i++){
			carr[i]=new char[i+1];
			for(int j=0;j<=i;j++){
				carr[i][j]='*';
			}
		}
		for(int i =0;i<a;i++){
			for(int j =0;j<=i;j++){
				System.out.print(carr[i][j]+" ");
			}
			System.out.println();
		}
	}
}
