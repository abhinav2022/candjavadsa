import java.io.*;

class User2D{
	
	public static void main(String[] ar) throws IOException	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int[][] iarr = new int[2][2];
		System.out.println("Enter input");
		for(int i =0;i<2;i++){
			for(int j =0;j<2;j++){
				iarr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Array output");
		for(int i =0;i<2;i++){
			for(int j =0;j<2;j++){
				System.out.print(iarr[i][j]+" ");
			}
			System.out.println();
		}
	}
	
}
