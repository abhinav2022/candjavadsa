import java.io.*;

class UserIn{
//q4	
	public static void main(String[] ar) throws IOException	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row size");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter col size");
		int b = Integer.parseInt(br.readLine());
		int[][] iarr = new int[a][b];
		System.out.println("Enter input");
		int c = 1;
		for(int i =0;i<a;i++){
			for(int j =0;j<b;j++){
			iarr[i][j]=c;
			c+=2;
			}
		}
		System.out.println("Array output");
		for(int i =0;i<a;i++){
			for(int j =0;j<b;j++){
				System.out.print(iarr[i][j]+" ");
			}
			System.out.println();
		}
	}
	
}
