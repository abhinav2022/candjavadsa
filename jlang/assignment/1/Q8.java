//Q8
class CharDiff {

	public static void main(String...ar) {
	
		char var1='a', var2='p';
		int a= var1 - var2;
		
		if(a==0) {
		
			System.out.println("var1 = "+var1+" var2 = "+var2);
		}

		else if(a<0) {
			System.out.println("Difference between "+var1+" and "+var2+" is "+ (-a));	
		}
	
		else {
			System.out.println("Difference between "+var1+" and "+var2+" is "+ a);	
		}
	}

}
