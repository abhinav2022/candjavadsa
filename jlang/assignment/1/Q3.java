//Q3

class UpperCase {

	public static void main(String [] args) {
	
		char var = 'A';

		if(var>64 && var<91) {
		
			System.out.println(var+" is uppercase character");
		}

		else if(var>=97 && var<=122) {
		
			System.out.println(var+" is lowercase character");
		}
		else {
		
			System.out.println(var+" is different character");
		}

	}

}
