//Q10
class PerfectNo {

	public static void main(String... ar) {
		
		int n = 28, s= 0;
		for(int i=1; i<n; i++) {
			
			if(n%i==0) {
			    s = s + i;
			}
		}	
		if(n==s) {
		
			System.out.println(n+" is a perfect number.");
		}
		else {
		
			System.out.println(n+" is not a perfect number.");
		}
	}

}
