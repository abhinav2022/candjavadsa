import java.io.*;

class ThreeD{

	public static void main(String[]ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter plane size");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter row size");
		int b = Integer.parseInt(br.readLine());
		System.out.println("Enter col size");
		int c = Integer.parseInt(br.readLine());
		int[][][] iarr= new int[a][b][c];
		System.out.println("Enter input ");
		for(int i = 0;i<a;i++){
			for(int j=0;j<b;j++){
				for(int k=0;k<c;k++){
					iarr[i][j][k]=Integer.parseInt(br.readLine());
				}
			}
		}
		System.out.println();
		for(int i = 0;i<a;i++){
			for(int j=0;j<b;j++){
				for(int k=0;k<c;k++){
					System.out.print(iarr[i][j][k]+" ");
				}
				System.out.println();
			}
			System.out.println("\n");
		}

	}
}
