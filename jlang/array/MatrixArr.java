import java.io.*;

class MatriX{

	public static void main(String[] ar) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int iarr[][] = new int[3][3];
		System.out.println("Enter Array inputs");

		for(int i = 0;i<3;i++){
			for(int j=0;j<3;j++){
				iarr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int i =0;i<3;i++){
			for(int j=0;j<3;j++){
				if(i==j || i+j==2){
				
					System.out.print(0+" ");
				}
				else{
					System.out.print(iarr[i][j]+" ");
				}
			}
			System.out.println();
		}

	}
}
