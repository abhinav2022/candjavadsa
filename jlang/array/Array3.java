import java.io.*;

class PalArr {

	public static void main(String []ar )throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int a = Integer.parseInt(br.readLine());
		int e = 0;
		System.out.println("Enter array elements");
		int[] iarr = new int[a];

		for(int i = 0;i<a;i++){
			iarr[i]=Integer.parseInt(br.readLine());
		}

		for(int i = 0; i < a ; i++){
			int b = iarr[i];
			int sum = 0;

			while(b!=0){
				int c = b%10;
				sum = (sum*10) + c;
				b=b/10;
			}
		
			if(iarr[i]==sum){
				System.out.println("Index of palindrome number is "+i);
				e++;
			}
		}
		
		if(e==0){
			System.out.println("Palindrome number not found \n\n error 404");
		}
	}
}
