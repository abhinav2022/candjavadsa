import java.io.*;

class SwiArr{

	public static void main(String[] ar) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter array size");
			int b = Integer.parseInt(br.readLine());
		System.out.println("Select which array you want");
		System.out.println("1.String array\n2.Integer array\n3.Char array");
		
		int a = Integer.parseInt(br.readLine());
		
		switch(a){
		
			case 1:	
				System.out.println("Enter input ");
				String[] sarr = new String[b];
				for(int i = 0;i<b;i++){
					sarr[i]=br.readLine();
				}
				System.out.println();
				for(int i = 0;i<b;i++){
					System.out.println(sarr[i]);
				}
				break;		
			case 2:	
				System.out.println("Enter input ");
				int[] iarr = new int[b];
				for(int i = 0;i<b;i++){
					iarr[i]=Integer.parseInt(br.readLine());
				}
				System.out.println();
				for(int i = 0;i<b;i++){
					System.out.println(iarr[i]);
				}
				break;		
			case 3:	
				System.out.println("Enter input ");
				char[] carr = new char[b];
				for(int i = 0;i<b;i++){
					carr[i]=(char)br.read();
					br.skip(1);
				}
				System.out.println();
				for(int i = 0;i<b;i++){
					System.out.println(carr[i]);
				}
				break;		
			
			
			
			default:
				System.out.println("Error 404");
				break;
		}
	}
}
