import java.io.*;

class EveArr {

	public static void main(String []ar) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size of array");
		
		int a =Integer.parseInt(br.readLine());

		System.out.println("Enter elements in array");

		int[] iarr = new int[a];

		for(int i = 0;i<a;i++){
		
			iarr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i = 0;i<a;i++){
			if(iarr[i]%2==0){
				System.out.print(iarr[i]+" ");
			}
			else{
				char b =(char)iarr[i];
				System.out.print(b+" ");
			}

		}

	}
}
