import java.io.*;

class Diagsum {

	public static void main(String [] arr) throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int[][][] iarr = new int[2][3][3];

		System.out.println("Enter input");
		for(int plane=0;plane<2;plane++){
		
			for(int row=0;row<3;row++){
				
				for(int col=0;col<3;col++){
					
					iarr[plane][row][col]=Integer.parseInt(br.readLine());
				}
			}
		}
		int sum1 = 0,sum2 = 0;
		for(int plane=0;plane<2;plane++){
		
			for(int row=0;row<3;row++){
				
				for(int col=0;col<3;col++){
					if(plane==0 && row==col){
					sum1=sum1+iarr[plane][row][col];
					}
					if(plane==1 && row==col){
					sum2=sum2+iarr[plane][row][col];
					}
				}
			}
		}

		System.out.println("Sum of diagonal numbers of both matrix is : "+sum1+" "+sum2);

	}
}
