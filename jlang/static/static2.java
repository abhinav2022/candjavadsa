class FileSystem{
	static void FAT16(){
		System.out.println("File Allocation Table for 16bits file system");
	}
	static void FAT32(){
		System.out.println("File Allocation Table for 32bits file system");
	}
	static void NTFS(){
		System.out.println("New Technology File System by Microsoft\n");
	}
	static void exfat(){
		System.out.println("extensible file allocation table");
	}
	static void Applefilesystem(){
		System.out.println("For Mac os apple files system");
	}
	public static void main(String [] args){
		System.out.println("Starting names of Filesystem\n");
		FAT16();
		FAT32();
		exfat();
		Applefilesystem();
		NTFS();
		System.out.println("Some Filesystems.");
	}
}
