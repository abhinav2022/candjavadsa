//
import java.io.*;

class Inp {

	public static void main(String [] args) throws IOException{
		
		InputStreamReader isr = new InputStreamReader (System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter Info ");
		System.out.println("Enter name");
		String name = br.readLine();

		System.out.println("Enter roll");

		int roll_no = Integer.parseInt(br.readLine());

		System.out.println("Enter year of study");

		String year = br.readLine();
		
		System.out.println("Name "+name);
		System.out.println("roll "+roll_no);
		System.out.println("year "+year);
	}
}
