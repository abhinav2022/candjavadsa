
class C2W{

	void prog(){
		System.out.println("Programming in classes");
	}
}
class MD extends C2W{
	void maths(){
		System.out.println("M3 class");
	}
}

class Stu extends MD{

	static Stu obj = new Stu();
	void edu(){
		maths();
		//prog();
	}

	public static void main(String[] ar){
		obj.edu();
	}
}
