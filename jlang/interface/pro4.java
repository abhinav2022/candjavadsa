interface A{
	
	 default void m1(){
		System.out.println("IN m1 default");
	}
}
interface B extends A{
	default void m1(){
		System.out.println("In m1 b");
	}
}
class I implements B,A{
	
      public void m1(){
		System.out.println("In B m1");
	}
public static void main(String[] ar){
		A o =new I();
		o.m1();
}
}
