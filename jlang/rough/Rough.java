import java.io.*;

interface A{
	
	default void m1(){
		System.out.println("THE m1 method in a interface");
	}
	class P{
		
		void b1(){
			System.out.println("Class P in A and b1");
		}
	}
}

interface B extends A{

	default void m1(){
		System.out.println("m1 in b");
	} 
}

class C implements A,B{
	public void E(){
		B.super.m1();
	}	
	public static void main(String[] ar) throws IOException{
		C ob = new C();
		//P o =new P();
		//A.super.b1();
		//ob.E();
		
		System.out.println("BUFFEREDREADER :--");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("SYSTEM.IN "+System.in);
		System.out.println("BR "+br);
	}
}
