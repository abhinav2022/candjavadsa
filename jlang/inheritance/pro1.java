
class Parent{
	
	Parent(){
		System.out.println("In parent constructor");
		System.out.println(Parent.this);
		Parent.this.m1();
		//Child o = new Child();
		//o.m1();
	//	.m1();
	}
	Parent(Child p){
		System.out.println("Parent with parameter parent p");
	}

	void m1(){
		System.out.println("In Parent m1");
	}
}

class Child extends Parent{
	
	Child(){
		System.out.println("In child");
		//System.out.println(this);
		super.m1();
	}
	void m1(){
		System.out.println("In child m1");
	}
	public static void main(String[] ar){
		
		Child o = new Child();
		
		System.out.println();
			super.m1();
		//Parent p = new Parent();
		//Parent p1 = new Parent(o);
		System.out.println();
	 //	Child o = new Child(p);
	}
}
