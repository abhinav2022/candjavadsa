
class t{
	
	int x = 10;
	static int y = 10;
	t(){
		System.out.println("In t()");
	}	
	t(int z){
		System.out.println("In t(int)");
	}

	t(t ob){
		System.out.println("In Demo(Dem)");
		ob.m1();
	}
	void m1(){
		System.out.println(this);
	}

	public static void main(String[] ar){
		t ob = new t();
		ob.m1();
		t ob1 = new t(4);
		ob1.m1();
		t ob2 = new t(ob);
		ob2.m1();
	}
}
