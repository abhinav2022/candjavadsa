
class Outer {

	int x = 10;

	class Inner1{
	
		int x = 100;

		class Inner2{
			int x = 1000;
		

		void m1(){
		
			System.out.println(this.x);
			System.out.println(Inner1.this.x);
			System.out.println(Outer.this.x);
		}
		}
	}
	public static void main(String[] ar){
		Outer ob1 = new Outer();
		Outer.Inner1 ob = ob1.new Inner1();

		Outer.Inner1.Inner2 obj3 = ob.new Inner2();
		obj3.m1();
	}
}
