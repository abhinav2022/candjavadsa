class Par{
	static{
		System.out.println("In static block par");
	}
	Par(){
		super();
		System.out.println("In Par");
	}
	void marry(){
		System.out.println("P Marry meth");
	}
	void m2(){
		System.out.println("M1");
	}
}
class Child extends Par{
	static{
		System.out.println("In static block child");
	}
	Child(){
		super();
		System.out.println("In child Construc");
	}
	void marry(){
		System.out.println("C marry meth");
	}
	void m2(){
		System.out.println("M2");
	}
	public static void main(String[] ar){
		 Par c = new Child();
		c.marry();
		c.m2();
	}
}
