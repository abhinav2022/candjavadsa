class Outer{
	class Inner1{
		class Inner2{
			void m3(){
				System.out.println(this);
				System.out.println("In m3");
			}
		}
	void m2(){
		System.out.println(this);
		Inner2 i2 = this.new Inner2();
		i2.m3();
		System.out.println("In m2");
	}
	}
	void m1(){
		System.out.println(this);
		Inner1 i1 = this.new Inner1();
		i1.m2();
		System.out.println("In m1");
	}
	public static void main(String[] ar){
		Outer O  =new Outer();
		O.m1();
	}
}
