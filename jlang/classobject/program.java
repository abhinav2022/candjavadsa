
class Outer {

	int x = 10;
	static int y = 20;

	class Inner extends Outer{
		Inner(){
		System.out.println(this);
	}
		int x = 10;
	
	}
	public static void main(String[] ae){
		
		Outer o = new Outer();
		Outer.Inner ob = o.new Inner();
		
	}
}

/*class Demo extends Outer{
	
	public static void main(String[] ae){
		System.out.println(this$0);
	}
}*/
