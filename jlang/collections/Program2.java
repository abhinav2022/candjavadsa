
import java.util.*;

class Employee implements Comparable<Employee>{

	String name =null;
	Double sal = 0.0;

	Employee(String n , double sal){
		this.name=n;
		this.sal=sal;

	}

	public int compareTo(Employee obj2){
		return sal.compareTo(obj2.sal);
	}

	public String toString(){
		return name + ":"+sal;
	}
}

class TreeSetDemo {

	public static void main(String[] ar){
		
		TreeSet obj = new TreeSet();

		obj.add(new Employee("Kanha",45.65));
		obj.add(new Employee("Rahul",55.65));
		obj.add(new Employee("Ashsih",65.65));

		System.out.println(obj);
	}
}
